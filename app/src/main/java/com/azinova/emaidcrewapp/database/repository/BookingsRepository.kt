package com.azinova.emaidcrewapp.database.repository

import android.annotation.SuppressLint
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.database.tables.BookingsTable
import com.azinova.emaidcrewapp.database.tables.MaidTable
import com.azinova.emaidcrewapp.model.response.bookings.ScheduleItem
import com.azinova.emaidcrewapp.model.response.finish.FinishDataItem
import com.azinova.emaidcrewapp.model.response.start.DataItem
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class BookingsRepository {

    companion object {
        var database: EmaidDatabase? = null

        var bookingTableModel: LiveData<List<BookingsTable>>? = null

        var bookingTimeingsFromDb: List<String>? = null

        var bookingdatesFromDb: List<String>? = null

        var maidsFromDb: List<String>? = null

        var schedulebytimeTableModel: List<BookingsTable>? = null

        var maidListFromDb: List<MaidTable>? = null

        var customerDetailsFromDb: LiveData<List<BookingsTable>>? = null

        var maidlistFromDb: LiveData<List<MaidTable>>? = null

        var newmaidlistFromDb: List<MaidTable>? = null

        var mainmaidlistFromDb: LiveData<List<MaidTable>?>? = null

        var newCustomerDetailsFromDb: List<BookingsTable>? = null


        fun initializeDB(context: Context): EmaidDatabase {
            return EmaidDatabase.getDataseClient(context)
        }

        suspend fun insertData(context: Context, schedule: List<ScheduleItem?>?) {
            var maidimage = ""
            var count = 0
            database = initializeDB(context)

            database!!.getDao().deleteBookings()
            database!!.getDao().deleteMaids()


            Log.e("Table Insert", "response" + schedule)

            try {

                if (schedule != null) {
                    if (schedule.isNotEmpty()) {


                        for (items in schedule) {
                            var bookingDetails = BookingsTable()


                            if (items?.schedule?.bookingId.isNullOrEmpty()) {
                                if (!(items?.scheduleDate.isNullOrEmpty())) {
                                    bookingDetails = BookingsTable(
                                        schedule_date = items?.scheduleDate
                                    )
                                    database!!.getDao().insertAllBookings(bookingDetails)
                                }
                            } else {
                                bookingDetails = BookingsTable(
                                    items?.id?.toInt(),
                                    items?.scheduleDate,
                                    items?.scheduleTimeing?.timeing,
                                    items?.scheduleTimeing?.noOfMaids,
                                    items?.schedule?.serviceStatus,
                                    items?.schedule?.shiftStart,
                                    items?.schedule?.shiftEnd,
                                    items?.schedule?.area,
                                    items?.schedule?.cleaningMaterial,
                                    items?.schedule?.customerId?.toInt(),
                                    items?.schedule?.customerName,
                                    items?.schedule?.customerAddress,
                                    items?.schedule?.customerMobile,
                                    items?.schedule?.customerType,
                                    items?.schedule?.customerCode,
                                    items?.schedule?.keyStatus,
                                    items?.schedule?.bookingNote,
                                    items?.schedule?.customernotes,
                                    items?.schedule?.serviceFee,
                                    items?.schedule?.zone,
                                    items?.schedule?.mop,
                                    items?.schedule?.extraService,
                                    items?.schedule?.tools,
                                    items?.schedule?.total,
                                    items?.schedule?.balance,
                                    items?.schedule?.outstandingBalance,
                                    items?.schedule?.servicetype,
                                    items?.schedule?.paymentStatus,
                                    items?.schedule?.paidAmount,
                                    items?.schedule?.customerLatitude,
                                    items?.schedule?.customerLongitude,
                                    items?.schedule?.starttime,
                                    items?.schedule?.endtime,
                                    items?.schedule?.istransfer,
                                    items?.schedule?.vaccum_cleaner,
                                    items?.schedule?.booking_type,
                                    items?.schedule?.username
                                )

                                Log.e("Insert", "Insert: $bookingDetails")

                                database!!.getDao().insertAllBookings(bookingDetails)

                                var maiddetails = MaidTable()
                                //MaidTable
                                if (items?.maidDetails?.size != 0) {

                                    for (maiditems in items?.maidDetails!!) {

                                        //maidImage save -------------------

                                        val maid_image: List<String> = database!!.getDao()
                                            .getMaidImage(maiditems?.maidId.toString())
                                        Log.e(
                                            "Image Exist Check",
                                            "insertData-check: " + maid_image
                                        )

                                        if (maid_image.size != 0) {

                                            maidimage = maid_image.get(0)

                                        } else {

                                            if (maiditems?.maid_image!!.isNotEmpty()) {

                                                val urlImage: URL = URL(maiditems.maid_image)

                                                val result: Deferred<Bitmap?> = GlobalScope.async {
                                                    urlImage.toBitmap()
                                                }

                                                val bitmap: Bitmap? = result.await()

                                                bitmap?.apply {
                                                    val savedUri: Uri? =
                                                        saveToInternalStorage(context)
                                                    Log.e(
                                                        "maidProfile",
                                                        "doLogin: " + savedUri.toString()
                                                    )
                                                    maidimage = savedUri.toString()
                                                }


                                            } else {
                                                maidimage = ""
                                            }
                                        }


                                        // ------------------------------
                                        count += 1
                                        maiddetails = MaidTable(
                                            maid_id = maiditems?.maidId,
                                            customer_id = maiditems?.customerId,
                                            booking_id = maiditems?.maid_booking_id?.toInt(),// items.id?.toInt(),
                                            maid_name = maiditems?.maidName,
                                            maid_attandence = maiditems?.maidAttandence,
                                            service_status = items?.schedule?.serviceStatus,
                                            maid_image = maidimage,
                                            maid_serviceFee = maiditems?.maid_serviceFee,
                                            maid_total = maiditems?.maid_total,
                                            maid_shift_start = items?.schedule?.shiftStart,
                                            maid_shift_end = items?.schedule?.shiftEnd,
                                            schedule_date = items?.scheduleDate,
                                            customerAddress = items?.schedule?.customerAddress
                                        )
                                        Log.e(".......", "insertData: " + maiddetails)
                                        database!!.getDao().insertAllMaidss(maiddetails)
                                    }
                                }


                            }


                        }


                    }
                }

            } catch (e: NumberFormatException) {
                Log.e("Exception", "Insertschedule&Maid: ${e.message}")
            }

        }


        // --------------------------Image Download -----------------
        fun URL.toBitmap(): Bitmap? {
            return try {
                BitmapFactory.decodeStream(openStream())
            } catch (e: IOException) {
                null
            }
        }

        fun Bitmap.saveToInternalStorage(context: Context): Uri? {
            val wrapper = ContextWrapper(context)

            var file = wrapper.getDir("maid_images", Context.MODE_PRIVATE)

            file = File(file, "${UUID.randomUUID()}.jpg")

            return try {
                val stream: OutputStream = FileOutputStream(file)

                compress(Bitmap.CompressFormat.JPEG, 100, stream)

                stream.flush()

                stream.close()

                Uri.parse(file.absolutePath)
            } catch (e: IOException) {
                e.printStackTrace()
                null
            }
        }
// -------------------------------------------------------------------------


        fun insertDate(currentdate: String) {
            if (currentdate.isNotEmpty()) {
                database?.getDao()?.insertDate(currentdate)
            }
        }


        fun getBookingDetails(
            context: Context,
            selecteddate: String
        ): LiveData<List<BookingsTable>>? {
            database = initializeDB(context)

            bookingTableModel = database!!.getDao().getAllBookings(selecteddate)
            return bookingTableModel
        }

        suspend fun getBookingTimeings(
            context: Context,
            selecteddate: String,
            FilterPosition: Int,
            filter: ArrayList<Int>
        ): List<String>? {
            database = initializeDB(context)

            if (filter.contains(-1)) {
                bookingTimeingsFromDb = database!!.getDao().getBookingTimeings(selecteddate)

            } else {
                bookingTimeingsFromDb = database!!.getDao()
                    .getBookingTimeingsByFilterPosition(selecteddate, filter)
            }
            return bookingTimeingsFromDb
        }

        suspend fun getBookingDates(context: Context): List<String>? {
            database = initializeDB(context)
            bookingdatesFromDb = database!!.getDao().getDates()
            return bookingdatesFromDb
        }

        suspend fun getBookingMaids(context: Context): List<String>? {
            database = initializeDB(context)
            maidsFromDb = database!!.getDao().getMaids()
            return maidsFromDb
        }

        suspend fun getscheduleDetailsbytime(
            context: Context,
            scheduleDate: String?,
            timeing: String?,
            FilterPosition: Int,
            filter: ArrayList<Int>
        ): List<BookingsTable>? {
            database = initializeDB(context)

            if (filter.contains(-1)) {
                schedulebytimeTableModel =
                    database!!.getDao().getSchedulesByTime(scheduleDate, timeing)
            } else {
                schedulebytimeTableModel =
                    database!!.getDao().getSchedulesByTimeByFilterPosition(
                        scheduleDate,
                        timeing,
                        filter
                    )
            }
            return schedulebytimeTableModel
        }

        suspend fun getMaidList(
            context: Context,
            customer_id: String?,
            shiftStart: String?,
            shiftEnd: String?,
            selecteddate: String,
            customerAddress: String?
        ): List<MaidTable>? {
            database = initializeDB(context)
            maidListFromDb = database!!.getDao().getMaidList(
                customer_id,
                shiftStart,
                shiftEnd,
                selecteddate,
                customerAddress
            )  //booking_id

            return maidListFromDb
        }

        fun getCustomerDetails(
            context: Context,
            booking_id: String
        ): LiveData<List<BookingsTable>>? {
            database = initializeDB(context)
            customerDetailsFromDb = database!!.getDao().getCustomerDetails(booking_id)

            return customerDetailsFromDb
        }

        fun getMailListFromDB(context: Context, booking_id: String): LiveData<List<MaidTable>>? {
            database = initializeDB(context)
            maidlistFromDb = database!!.getDao().getMailListFromDB(booking_id)

            return maidlistFromDb
        }

        suspend fun DeleteTransferbooking(context: Context, bookingId: String) {
            database = initializeDB(context)
            database!!.getDao().deleteTransferedBooking(bookingId)
            database!!.getDao().deleteTransferedBookingmaidtable(bookingId)
        }

        suspend fun deleteTransferedallBooking(context: Context, service_type: String) {
            database = initializeDB(context)
            database!!.getDao().deleteTransferedallBooking(service_type)
            database!!.getDao().deleteTransferedallBookingmaidtable(service_type)
        }


        suspend fun updatestartbookingStatus(
            context: Context,
            bookingId: String,
            serviceStatus: String,
            starttime: String?,
            data: List<DataItem?>?
        ) {
            database = initializeDB(context)
            database!!.getDao().updatestartbookingStatus(bookingId, serviceStatus, starttime)


            if (data != null) {
                for (maidstatus in data) {

                    if (maidstatus?.status.equals("success")) {

                        database!!.getDao().updateMaidbookingStatus(
                            maidstatus?.bookingId.toString(),
                            maidstatus?.serviceStatus.toString()
                        )
                    }
                }
            }

//update service status in maidtable using loop with data
        }

        @SuppressLint("LongLogTag")
        suspend fun updatefinishbookingStatus(
            context: Context,
            bookingId: String,
            serviceStatus: String,
            paid_amount: String?,
            outstandingAmount: String?,
            mop: String?,
            payment_status: Int,
            balance: Double?,
            endtime: String?,
            data: List<FinishDataItem?>?
        ) {
            database = initializeDB(context)
            database!!.getDao().updatefinishbookingStatus(
                bookingId,
                serviceStatus,
                paid_amount,
                outstandingAmount,
                mop,
                payment_status,
                balance, endtime
            )

            if (data != null) {
                for (maidstatus in data) {

                    if (maidstatus?.status.equals("success")) {

                        database!!.getDao().updateMaidFinishbookingStatus(
                            maidstatus?.bookingId.toString(),
                            maidstatus?.serviceStatus.toString()
                        )
                    }
                }
            }
        }

        suspend fun updatecancelbookingStatus(
            context: Context,
            bookingId: String,
            serviceStatus: String
        ) {
            database = initializeDB(context)
            database!!.getDao().updateCancelbookingStatus(bookingId, serviceStatus)

        }


        suspend fun updateMaidStatusChange(
            context: Context,
            maidId: String,
            maidAttandence: String
        ) {
            database = initializeDB(context)
            database!!.getDao().updateMaidStatusChange(maidId, maidAttandence)

        }


        suspend fun transferupdates(context: Context, timeing: String?, noOfMaids: Int?) {
            database = initializeDB(context)
            database!!.getDao().transferupdates(timeing.toString(), noOfMaids)

        }

        suspend fun NewMailListFromDB(
            context: Context,
            customer_id: String,
            shift_starttime: String,
            shift_endtime: String,
            booking_date: String,
            customeraddress: String
        ): List<MaidTable>? {
            database = initializeDB(context)
            newmaidlistFromDb = database!!.getDao().NewMailListFromDB(
                customer_id,
                shift_starttime,
                shift_endtime,
                booking_date,
                customeraddress
            ) //bookingId,
            return newmaidlistFromDb
        }

        fun MailListFromDB(
            context: Context,
            customer_id: String,
            shift_starttime: String,
            shift_endtime: String,
            booking_date: String,
            customeraddress: String
        ): LiveData<List<MaidTable>?>? {
            database = initializeDB(context)
            mainmaidlistFromDb = database!!.getDao().MailListFromDB(
                customer_id,
                shift_starttime,
                shift_endtime,
                booking_date,
                customeraddress
            ) //bookingId,
            return mainmaidlistFromDb
        }

        suspend fun NewCustomerDetailsFromDB(
            requireContext: Context,
            bookingId: String
        ): List<BookingsTable>? {
            database = initializeDB(requireContext)
            newCustomerDetailsFromDb = database!!.getDao().NewCustomerDetailsFromDB(bookingId)

            return newCustomerDetailsFromDb
        }

        suspend fun updateKeyStatusChange(context: Context, customerId: String, status: String) {
            database = initializeDB(context)
            database!!.getDao().updateKeyStatusChange(customerId, status)
        }
    }


}