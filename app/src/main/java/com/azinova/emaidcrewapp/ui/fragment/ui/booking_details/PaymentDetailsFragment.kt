package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.model.response.Invoice.DataDetails
import com.azinova.emaidcrewapp.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.DetailInvoiceFragment
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.payment_details.*
import kotlinx.android.synthetic.main.payment_details.btn_invoice
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class PaymentDetailsFragment : Fragment() {
    val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var paymentDetailsViewModel: PaymentDetailsViewModel

    private lateinit var parent: BookingDetailsActivity

    var paymentmode: Int = -1
    var paytype: Int = 0

    var booking_id_list: ArrayList<String>? = ArrayList()


    var booking_id: String = ""
    var customer_id: String = ""
    var serviceFee: String = ""
    var service_status: String = ""
    var total: String = ""
    var outstandingBalance: String = ""
    var paymentStatus: String = ""
    var mop: String = ""
    var paidAmount: String = ""
    lateinit var booking_date: String


    private var finishButtonClicked = false
    private var cancelButtonClicked = false

    lateinit var details: DataDetails

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.payment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        paymentDetailsViewModel = ViewModelProvider(this).get(PaymentDetailsViewModel::class.java)

        parent = activity as BookingDetailsActivity

        paymentDetailsViewModel.reset()

        val bundle = this.arguments
        if(bundle!=null) {
            booking_id =bundle.getString("Booking_id").toString()
            customer_id = bundle.getString("Customer_id").toString()
            serviceFee = bundle.getString("Service_amount").toString()
            service_status = bundle.getString("Service_status").toString()
            total = bundle.getString("Total").toString()
            outstandingBalance = bundle.getString("Outstanding_Balance").toString()
            paymentStatus =bundle.getString("Payment_status").toString()
            mop = bundle.getString("MOP").toString()
            paidAmount = bundle.getString("Paid_amount").toString()
            booking_date = bundle.getString("booking_date").toString()
            booking_id_list =
                bundle.getSerializable("Booking_id List") as ArrayList<String>?

        }

        initview(view)
        observeList()
        onClick()


    }

    private fun observeList() {
        paymentDetailsViewModel.finishServiceStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { finishServiceStatus ->
                txt_paid_value.text?.clear()
                txt_receiptnumbervalue.text?.clear()
                edit_notes.text?.clear()
                val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)

                if (finishServiceStatus?.status.equals("success")) {
                    Loader.hideLoader()

                    //Invoice
                    btn_finishbooking.visibility = View.INVISIBLE
                    btn_cancelservice.visibility = View.INVISIBLE


                    if (!finishServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(
                            requireContext(),
                            finishServiceStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        getActivity()?.onBackPressed()

                    }
                }

                else if (finishServiceStatus?.status.equals("token_error")) {
                    Loader.hideLoader()
                    if (!finishServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(requireContext(), finishServiceStatus?.message, Toast.LENGTH_SHORT)
                            .show()
                    }

                    clearLogin()
                     parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()


                }
                else {
                    Loader.hideLoader()
                    if (!finishServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(
                            requireContext(),
                            finishServiceStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                }
            }
        )

       /* paymentDetailsViewModel.cancelServiceStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { cancelServiceStatus ->
                if (cancelServiceStatus.status.equals("success")) {
                    Loader.hideLoader()
                    if (!cancelServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(
                            requireContext(),
                            cancelServiceStatus.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                    requireActivity().finish()
                }
                else if (cancelServiceStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    if (!cancelServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(requireContext(), cancelServiceStatus.message, Toast.LENGTH_SHORT)
                            .show()
                    }



                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                }

                else {
                    Loader.hideLoader()
                    if (!cancelServiceStatus?.message.isNullOrEmpty()){
                        Toast.makeText(
                            requireContext(),
                            cancelServiceStatus.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                }

            })
*/

        paymentDetailsViewModel.detailInvoice.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Loader.hideLoader()
            if (it != null) {
                if (it.status.equals("success")) {

                    if (it.details != null){
                        details = it.details
                        val bundle = Bundle()

                        bundle.putSerializable("details",details)
                        DetailInvoiceFragment.pdfUrl = ""
                        findNavController().navigate(R.id.action_navigation_paymentdetails_to_detailInvoiceFragment,bundle)
                    }

                    Log.e("Test", "InvoiceDetails: $details " )

                    if (!it.message.isNullOrEmpty()){
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                } else {
                    if (!it.message.isNullOrEmpty()){
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                }
            }
        })



    }


    private fun initview(view: View) {


        if (prefs.is_payment_hide.equals("yes")){
            ll_pay.visibility =View.GONE
            serviceamount_value.text = "AED " + "0.00"

            total_value.text = "AED " + "0.00"
            txt_outstanding_balance.text = "AED " + "0.00"



        }else{
            ll_pay.visibility =View.VISIBLE
            serviceamount_value.text = "AED " + serviceFee

            total_value.text = "AED " + total
            txt_outstanding_balance.text = "AED " + outstandingBalance


            if (SharedPref.company_name ==  "Urban Cleaning"){
                l_cheque.visibility = View.GONE
                l_online.visibility = View.VISIBLE
            }else{
                l_cheque.visibility = View.VISIBLE
                l_online.visibility = View.GONE
            }
        }





        //PAYMENT STATUS
        when (paymentStatus) {
            "0" -> img_paystatus_payment.setImageResource(R.drawable.new_notpaid) //= "Not Paid"
            "1" -> img_paystatus_payment.setImageResource(R.drawable.new_paid)// = "Paid"
            "2" -> img_paystatus_payment.setImageResource(R.drawable.new_online) // = "Online"
            else -> img_paystatus_payment.visibility = View.INVISIBLE
        }

//-----

        when (service_status) {
            "0" -> {

                finishButtonClicked = true
                btn_finishbooking.setBackgroundResource(R.drawable.bg_button_off)

                cancelButtonClicked = true
                btn_cancelservice.setBackgroundResource(R.drawable.bg_button_off)

            }
            "1" -> {

                finishButtonClicked = false
                btn_finishbooking.setBackgroundResource(R.drawable.bg_green)

                cancelButtonClicked = false
                btn_cancelservice.setBackgroundResource(R.drawable.bg_red)

            }

        }




        txt_paid_value.text = Editable.Factory.getInstance()
            .newEditable(total)
        txt_receiptnumbervalue.text = Editable.Factory.getInstance()
            .newEditable("")

        when (mop) {
            "Cash" -> {
                paymentmode = 0
                l_cash.setBackgroundResource(R.drawable.bg_purple_left_curve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_icon)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.white))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
            "Card" -> {
                paymentmode = 1

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_purple_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_white)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.white))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
            "Cheque" -> {
                paymentmode = 2

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_purple_right_curve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque_white)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.white))
            }
            else -> {
                paymentmode = -1

                l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
                l_card.setBackgroundResource(R.drawable.bg_grey_square)
                l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

                img_cash.setImageResource(R.drawable.ic_cash_grey)
                imgcard.setImageResource(R.drawable.ic_card_icon)
                imgcheque.setImageResource(R.drawable.ic_cheque)

                cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
                cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            }
        }




    }

    private fun onClick() {
        l_cash.setOnClickListener {
            paymentmode = 0
            l_cash.setBackgroundResource(R.drawable.bg_purple_left_curve)
            l_card.setBackgroundResource(R.drawable.bg_grey_square)
            l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

            img_cash.setImageResource(R.drawable.ic_cash_icon)
            imgcard.setImageResource(R.drawable.ic_card_icon)
            imgcheque.setImageResource(R.drawable.ic_cheque)

            cash_click.setTextColor(getResources().getColor(R.color.white))
            card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))

        }
        l_card.setOnClickListener {
            paymentmode = 1

            l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
            l_card.setBackgroundResource(R.drawable.bg_purple_square)
            l_cheque.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)
            l_online.setBackgroundResource(R.drawable.bg_greysqu_rightcurve)

            img_cash.setImageResource(R.drawable.ic_cash_grey)
            imgcard.setImageResource(R.drawable.ic_card_white)
            imgcheque.setImageResource(R.drawable.ic_cheque)
            imgonline.setImageResource(R.drawable.ic_online_grey)

            cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            card_click.setTextColor(getResources().getColor(R.color.white))
            cheque_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            online_click.setTextColor(getResources().getColor(R.color.color_grey_common))

        }
        l_cheque.setOnClickListener {
            paymentmode = 2

            l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
            l_card.setBackgroundResource(R.drawable.bg_grey_square)
            l_cheque.setBackgroundResource(R.drawable.bg_purple_right_curve)

            img_cash.setImageResource(R.drawable.ic_cash_grey)
            imgcard.setImageResource(R.drawable.ic_card_icon)
            imgcheque.setImageResource(R.drawable.ic_cheque_white)

            cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            cheque_click.setTextColor(getResources().getColor(R.color.white))

        }
        l_online.setOnClickListener {
            paymentmode = 3

            l_cash.setBackgroundResource(R.drawable.bg_greysqu_leftcurve)
            l_card.setBackgroundResource(R.drawable.bg_grey_square)
            l_online.setBackgroundResource(R.drawable.bg_purple_right_curve)

            img_cash.setImageResource(R.drawable.ic_cash_grey)
            imgcard.setImageResource(R.drawable.ic_card_icon)
            imgonline.setImageResource(R.drawable.ic_online_white)

            cash_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            card_click.setTextColor(getResources().getColor(R.color.color_grey_common))
            online_click.setTextColor(getResources().getColor(R.color.white))

        }


        btn_finishbooking.setOnClickListener {
            Log.d("btn_finishbooking", "onClick: "+finishButtonClicked)
            Log.d("MOP", "finishService: " + paymentmode)
            if (!finishButtonClicked) {


                if (requireContext().isConnectedToNetwork()) {

                    if (!TextUtils.isEmpty(txt_paid_value.text.toString())){

                            if (paymentmode == -1) {
                            Snackbar.make(
                                    outerpaymentdetails,
                                    "Please select payment mode ",
                                    Snackbar.LENGTH_SHORT
                            ).show()

                        }else{
                            Log.d("btn_finishbooking 1", "onClick with payment: ")

                            finishService("1")
                        }
                    }

                    else{
                        Log.d("btn_finishbooking  2", "onClick without payment: ")

                        finishService("0")
                    }



                } else {
                    Snackbar.make(
                        outerpaymentdetails,
                        "Please connect to internet",
                        Snackbar.LENGTH_LONG
                    ).show()
                }


            }
        }
        btn_cancelservice.setOnClickListener {
            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")

            val currentDate = sdf1.format(Date())
            if (!cancelButtonClicked) {
                val bundle = Bundle()
                bundle.putString("BookingId",booking_id)
                bundle.putString("notes",edit_notes.text.toString())
                bundle.putSerializable("Booking_id_list", booking_id_list)
                Log.d("TAG", "onClick: "+booking_id_list)
                findNavController().navigate(R.id.cancelOrderBottomSheet,bundle)



        }
        }

        btn_invoice.setOnClickListener {


            var spf = SimpleDateFormat("dd-MM-yyyy")
            val newDatee = spf.parse(booking_date)
            spf = SimpleDateFormat("yyyy-MM-dd")
            val date: String = spf.format(newDatee)

            GlobalScope.launch(Dispatchers.Main){
                Loader.showLoader(requireContext())
                paymentDetailsViewModel.getDetailInvoice(prefs.user_id.toString(),prefs.user_type.toString(),prefs.token,booking_id,date)
            }
        }
    }

    private fun cancelService() {
        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())


        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage("Do you want to finish this booking?")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirm"
        ) { dialog, which ->
            /*Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {

                paymentDetailsViewModel.getCancelService(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    booking_id.toInt(), "3", edit_notes.text.toString(),"",""
                )
            }*/
            dialog.dismiss()
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams


        /*val cancelbuilder = AlertDialog.Builder(requireContext())
        val cancelview: View =
            LayoutInflater.from(requireContext()).inflate(R.layout.dialog_cancel, null)
        cancelbuilder.setView(cancelview)
        val cancelalertDialog = cancelbuilder.create()

        val confirm = cancelview.findViewById<TextView>(R.id.confirm)
        val cancel = cancelview.findViewById<TextView>(R.id.cancel)

        confirm.setOnClickListener {
            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {

                paymentDetailsViewModel.getCancelService(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    booking_id.toInt(), "3"
                )
            }
            cancelalertDialog.dismiss()

        }
        cancel.setOnClickListener {
            cancelalertDialog.dismiss()
        }

        cancelalertDialog.show()*/


    }

    private fun finishService(paymenttype: String) {

        Log.d("btn_finishbooking", "onClick: "+"Confirmation alert")
        Log.d("payment", "onClick: "+paymenttype)




        val alertDialog = AlertDialog.Builder(requireContext()).create()
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage("Do you want to finish this booking?")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirm"
        ) { dialog, which ->

            finalFinish(paymenttype)

            dialog.dismiss()
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel"
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams


    }

    private fun finalFinish(paymenttype: String) {

        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf1.format(Date())

        Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                Log.d("MOP", "finishService: " + paymentmode)

                paymentDetailsViewModel.getFinishService(
                        requireContext(),
                        prefs.user_id,
                        prefs.user_type,
                        currentDate,
                        prefs.token,
                    booking_id_list,// booking_id.toInt(),
                        "2",
                        paymenttype,
                        txt_paid_value.text.toString(),
                        txt_receiptnumbervalue.text.toString(),
                        paymentmode,
                        outstandingBalance,edit_notes.text.toString(),
                    booking_id
                )
            }
    }
    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }


    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }


}