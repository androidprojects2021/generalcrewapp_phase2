package com.azinova.emaidcrewapp.database

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.azinova.emaidcrewapp.database.tables.BookingsTable
import com.azinova.emaidcrewapp.database.tables.MaidTable

@Dao
interface DatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllBookings(bookins: BookingsTable)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllMaidss(maiids: MaidTable)

    //new change
    @Query("INSERT INTO bookings_table(schedule_date) VALUES (:selecteddate)")
    fun insertDate(selecteddate: String)



    @Query("SELECT DISTINCT schedule_date from bookings_table ORDER BY schedule_date ASC")
    suspend fun getDates(): List<String>

    //AllDataByDate
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate ORDER BY timeing ASC")
    fun getAllBookings(selecteddate: String): LiveData<List<BookingsTable>>?

    //ALL Timeing
    @Query("SELECT DISTINCT timeing FROM bookings_table WHERE schedule_date =:selecteddate ORDER BY timeing ASC")
    suspend fun getBookingTimeings(selecteddate: String): List<String>?

    //Selected Timeing
    @Query("SELECT DISTINCT timeing FROM bookings_table WHERE schedule_date =:selecteddate AND service_status in(:filter) ORDER BY timeing ASC")           //  <--------
    suspend fun getBookingTimeingsByFilterPosition(
        selecteddate: String,
        filter: ArrayList<Int>,
    ): List<String>?

    //ALL Bookings
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate AND timeing =:timeing ORDER BY service_status ASC")
    suspend fun getSchedulesByTime(
        selecteddate: String?,
        timeing: String?

    ): List<BookingsTable>?

    //Selected Bookings
    @Query("SELECT * FROM bookings_table WHERE schedule_date =:selecteddate AND timeing =:timeing AND service_status in(:filter)")
    suspend fun getSchedulesByTimeByFilterPosition(
        selecteddate: String?,
        timeing: String?,
        filter: ArrayList<Int>
    ): List<BookingsTable>?

    @Query("SELECT * FROM bookings_table WHERE booking_id =:bookingId ")
    fun getCustomerDetails(bookingId: String):LiveData<List<BookingsTable>>?

    @Query("SELECT * FROM maid_table WHERE customer_id =:customer_id AND shift_start=:shiftStart AND shift_end=:shiftEnd AND schedule_date=:selecteddate AND customer_address=:customerAddress")  // booking_id =:booking_id , changed on 18 Feb 2022 , by Reema
    suspend fun getMaidList(
        customer_id: String?,
        shiftStart: String?,
        shiftEnd: String?,
        selecteddate: String,
        customerAddress: String?
    ): List<MaidTable>?

    @Query("SELECT * FROM maid_table WHERE booking_id =:booking_id ")
    fun getMailListFromDB(booking_id: String?):  LiveData<List<MaidTable>>?





//-----------
    @Query("SELECT * FROM maid_table WHERE customer_id =:customer_id AND shift_start=:shiftStart AND shift_end=:shift_endtime AND schedule_date=:booking_date AND customer_address=:customeraddress")  // booking_id =:booking_id
   suspend fun NewMailListFromDB(
    customer_id: String,
    shiftStart: String,
    shift_endtime: String,
    booking_date: String,
    customeraddress: String
): List<MaidTable>?

    @Query("SELECT * FROM maid_table WHERE customer_id =:customer_id AND shift_start=:shiftStart AND shift_end=:shift_endtime AND schedule_date=:booking_date AND customer_address=:customeraddress")  // booking_id =:booking_id
     fun MailListFromDB(
        customer_id: String,
        shiftStart: String,
        shift_endtime: String,
        booking_date: String,
        customeraddress: String
    ): LiveData<List<MaidTable>?>?

    @Query("SELECT * FROM bookings_table WHERE booking_id =:bookingId ")
    suspend fun NewCustomerDetailsFromDB(bookingId: String?):List<BookingsTable>?
//------------

    @Query("SELECT maid_image from maid_table WHERE maid_id =:maidid")
    suspend fun getMaidImage(maidid: String): List<String>


    @Query("UPDATE bookings_table SET service_status=:serviceStatus, starttime=:starttime WHERE booking_id = :bookingId ")
    suspend fun updatestartbookingStatus(bookingId: String, serviceStatus: String, starttime: String?)

    @Query("UPDATE maid_table SET service_status=:service_status WHERE booking_id = :booking_id ")
    suspend fun updateMaidbookingStatus(booking_id: String, service_status: String) {
        Log.d("UPDATE Maid table 3", "updatestartbookingStatus: "+booking_id+" , "+service_status)

    }  //added 0n 21Feb2022


    @Query("UPDATE bookings_table SET service_status=:serviceStatus, paid_amount=:paid_amount, outstanding_balance=:outstandingAmount, mop=:mop, payment_status=:payment_status, balance=:balance , endtime=:endtime WHERE booking_id = :bookingId ")
    suspend fun updatefinishbookingStatus(
            bookingId: String, serviceStatus: String, paid_amount: String?,
            outstandingAmount: String?, mop: String?, payment_status: Int, balance: Double?,endtime: String?
    )

    @SuppressLint("LongLogTag")
    @Query("UPDATE maid_table SET service_status=:service_status WHERE booking_id = :booking_id ")
    suspend fun updateMaidFinishbookingStatus(booking_id: String, service_status: String) {
        Log.d("UPDATE Maid table Finish3", "updatestartbookingStatus: "+booking_id+" , "+service_status)

    }  //added 0n 22Feb2022


    @Query("UPDATE bookings_table SET service_status=:serviceStatus WHERE booking_id = :bookingId ")
    suspend fun updateCancelbookingStatus(bookingId: String, serviceStatus: String)

    @Query("UPDATE maid_table SET maid_attandence=:maidAttandence WHERE maid_id = :maidId ")
    suspend fun updateMaidStatusChange(maidId: String, maidAttandence: String)

    @Query("UPDATE bookings_table SET key_status=:keyStatus WHERE customer_id = :customer_id ")
    suspend fun updateKeyStatusChange(customer_id: String, keyStatus: String)

    @Query("UPDATE bookings_table SET no_of_maids=:noOfMaids WHERE timeing = :timeing ")
    suspend fun transferupdates(timeing: String, noOfMaids: Int?)


    @Query("SELECT DISTINCT maid_id from maid_table")
    suspend fun getMaids(): List<String>


    @Query("DELETE FROM bookings_table")
    suspend fun deleteBookings()

    @Query("DELETE FROM maid_table")
    suspend fun deleteMaids()


    @Query("DELETE FROM bookings_table WHERE booking_id =:bookingId")
    suspend fun deleteTransferedBooking(bookingId: String)

    @Query("DELETE FROM maid_table WHERE booking_id =:bookingId")
    suspend fun deleteTransferedBookingmaidtable(bookingId: String)

    @Query("DELETE FROM bookings_table WHERE service_status =:service_type")
    suspend fun deleteTransferedallBooking(service_type: String)

    @Query("DELETE FROM maid_table WHERE service_status =:service_type")
    suspend fun deleteTransferedallBookingmaidtable(service_type: String)



}