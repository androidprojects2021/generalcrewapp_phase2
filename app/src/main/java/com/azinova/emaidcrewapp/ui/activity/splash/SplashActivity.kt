package com.azinova.emaidcrewapp.ui.activity.splash

import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.multidex.MultiDex
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.ui.activity.companycode.CompanyCodeActivity
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.utils.notification.BaseActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class SplashActivity : BaseActivity() {


    private lateinit var splash_viewModel: SplashActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()


        splash_viewModel = ViewModelProvider(this).get(SplashActivityViewModel::class.java)
        observeLoggedIn()

    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    private fun observeLoggedIn() {
        splash_viewModel.loginStatus.observe(this, Observer { loginStatus ->
            try {
                if (loginStatus) {

                    GlobalScope.launch {
                        delay(1000)
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK


                        if (getIntent().extras != null) {

                            Log.e(
                                "data", "generateNotification" + getIntent().getExtras()?.getString(
                                    "body"
                                ).toString()
                            )
                            Log.e(
                                "NewData",
                                "Maid" + getIntent().getExtras()?.getString("maid").toString()
                            )
                            Log.e(
                                "NewData",
                                "BookingTime" + getIntent().getExtras()?.getString("bookingTime")
                                    .toString()
                            )
                            Log.e(
                                "NewData",
                                "Customer" + getIntent().getExtras()?.getString("customer")
                                    .toString()
                            )
                            Log.e(
                                "NewData",
                                "Title" + getIntent().getExtras()?.getString("title").toString()
                            )
                            if (getIntent().extras!!.getString("body") != null
                                || getIntent().extras!!.getString("maid") != null
                                || getIntent().extras!!.getString("bookingTime") != null
                                || getIntent().extras!!.getString("customer") != null
                                || getIntent().extras!!.getString("title") != null
                            ) {
                                intent.putExtra("message", getIntent().extras!!.getString("body"))
                                intent.putExtra("maid", getIntent().extras!!.getString("maid"))
                                intent.putExtra(
                                    "bookingTime",
                                    getIntent().extras!!.getString("bookingTime")
                                )
                                intent.putExtra(
                                    "customer",
                                    getIntent().extras!!.getString("customer")
                                )
                                intent.putExtra("title", getIntent().extras!!.getString("title"))
                            }
                        }
                        startActivity(intent)
                    }
                } else {



                    GlobalScope.launch {
                        delay(1000)
                        val intent = Intent(applicationContext, CompanyCodeActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }


}