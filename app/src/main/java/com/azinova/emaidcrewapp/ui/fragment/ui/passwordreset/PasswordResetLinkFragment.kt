package com.azinova.emaidcrewapp.ui.fragment.ui.passwordreset

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.azinova.emaidcrewapp.R


class PasswordResetLinkFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_password_reset_link, container, false)
    }


}