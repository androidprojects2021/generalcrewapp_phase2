package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.database.tables.BookingsTable
import com.azinova.emaidcrewapp.database.tables.MaidTable
import com.azinova.emaidcrewapp.database.tables.TimeCountdownTable
import com.azinova.emaidcrewapp.model.response.Invoice.ResponseInvoice
import com.azinova.emaidcrewapp.model.response.bookings.ResponseBookings
import com.azinova.emaidcrewapp.model.response.bookings.ScheduleItem
import com.azinova.emaidcrewapp.model.response.key.ResponseAddRemoveKey
import com.azinova.emaidcrewapp.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaidcrewapp.model.response.maiddetails.ResponseMaidDetailsList
import com.azinova.emaidcrewapp.model.response.maidstatus.ResponseMaidStatusChange
import com.azinova.emaidcrewapp.model.response.start.DataItem
import com.azinova.emaidcrewapp.model.response.start.ResponseStartService
import com.azinova.emaidcrewapp.model.response.transfer.ResponseBookingTransfer
import com.azinova.emaidcrewapp.model.response.transfer.ResponseTransferlist
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import java.text.SimpleDateFormat
import java.util.*

class BookingDetailsViewModel : ViewModel() {
    private val prefs = SharedPref

    var customerDetailsFromDb: LiveData<List<BookingsTable>>? = null

    var maidlistFromDb: LiveData<List<MaidTable>>? = null

    var countdatalist: List<TimeCountdownTable>? = null

    var newmaidlistFromDb: List<MaidTable>? = null


    var mainmaidlistFromDb: LiveData<List<MaidTable>?>? = null

    var newCustomerDetailsFromDb: List<BookingsTable>? = null


    private val _maidlistStatus = MutableLiveData<ResponseMaidDetailsList?>()
    val maidlistStatus: LiveData<ResponseMaidDetailsList?>
        get() = _maidlistStatus

    private val _TransferlistStatus = MutableLiveData<ResponseTransferlist?>()
    val TransferlistStatus: LiveData<ResponseTransferlist?>
        get() = _TransferlistStatus

    private val _transferStatus = MutableLiveData<ResponseBookingTransfer?>()
    val transferStatus: LiveData<ResponseBookingTransfer?>
        get() = _transferStatus

    private val _startServiceStatus = MutableLiveData<ResponseStartService?>()
    val startServiceStatus: LiveData<ResponseStartService?>
        get() = _startServiceStatus

    private val _maidBookingsStatus = MutableLiveData<ResponseMaidBookingDetailsList?>()
    val maidBookingsStatus: LiveData<ResponseMaidBookingDetailsList?>
        get() = _maidBookingsStatus

    private val _maidStatusChange = MutableLiveData<ResponseMaidStatusChange?>()
    val maidStatusChange: LiveData<ResponseMaidStatusChange?>
        get() = _maidStatusChange

    //driverTransferList
    private val _DriverTransferlistStatus = MutableLiveData<ResponseTransferlist?>()
    val DriverTransferlistStatus: LiveData<ResponseTransferlist?>
        get() = _DriverTransferlistStatus

    private val _keyStatusChange = MutableLiveData<ResponseAddRemoveKey?>()
    val keyStatusChange: LiveData<ResponseAddRemoveKey?>
        get() = _keyStatusChange

    private val _bookingsDetailsfetch = MutableLiveData<ResponseBookings>()  //ResponseBookings
    val bookingsDetailsfetch: LiveData<ResponseBookings>
        get() = _bookingsDetailsfetch

    fun getCustomerDetails(context: Context, booking_id: String): LiveData<List<BookingsTable>>? {
        customerDetailsFromDb = BookingsRepository.getCustomerDetails(context, booking_id)
        return customerDetailsFromDb
    }

    fun getMailListFromDB(context: Context, booking_id: String): LiveData<List<MaidTable>>? {
        maidlistFromDb = BookingsRepository.getMailListFromDB(context, booking_id)
        return maidlistFromDb
    }


    fun reset() {
        _TransferlistStatus.value = null
        _maidBookingsStatus.value = null
        _maidStatusChange.value = null
        _startServiceStatus.value = null
        _transferStatus.value = null
        _maidlistStatus.value = null
        _detailInvoice.value = null
        _DriverTransferlistStatus.value = null
        _keyStatusChange.value = null
    }

    suspend fun getMaiddetailslist(
        userid: String,
        usertype: String,
        date: String,
        token: String?,
        bookingId: String
    ) {

        try {
            val response = ApiClient.getRetrofit()
                .getMaidDetailslist(userid, usertype, date, token!!, bookingId)

            if (response.status.equals("success")) {
                _maidlistStatus.value = response
            } else {
                _maidlistStatus.value = response
            }


        } catch (e: Exception) {
            _maidlistStatus.value =
                ResponseMaidDetailsList(message = "Something went wrong", status = "error")

        }

    }

    suspend fun getTransferlist(
        userid: String,
        usertype: String,
        date: String,
        token: String?,
        bookingId: String
    ) {
        try {
            val response =
                ApiClient.getRetrofit().getTransferlist(userid, usertype, date, token!!, bookingId)

            if (response.status.equals("success")) {

                _TransferlistStatus.value = response
            } else {
                _TransferlistStatus.value = response
            }


        } catch (e: Exception) {
            Log.e("Exception", "getTransferlist: ")
            _TransferlistStatus.value =
                ResponseTransferlist(message = "Something went wrong", status = "error")

        }


    }

    suspend fun getDriverTransferList(
        user_id: String,
        user_type: String,
        currentDate: String,
        token: String?,
        booking_id: String
    ) {
        try {
            val response = ApiClient.getRetrofit()
                .getDriverList(user_id, user_type, currentDate, token!!, booking_id)

            if (response.status.equals("success")) {
                _DriverTransferlistStatus.value = response
            } else {
                _DriverTransferlistStatus.value = response
            }
        } catch (e: Exception) {
            Log.e("Exception", "getDriverTransferList: ${e.message}")
            _DriverTransferlistStatus.value =
                ResponseTransferlist(message = "Something Went Wrong", status = "Error")
        }
    }

    suspend fun getBookingsTransfer(
        context: Context,
        userid: String,
        usertype: String,
        date: String,
        token: String?,
        bookingId: String,
        selected_zone: String,
        transfer_type: String,
        is_driver_based: String,
        maidId: String?,
        booking_list: ArrayList<String>?
    ) {
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())
        try {
            val response = ApiClient.getRetrofit().getBookingsTransfer(
                userid,
                usertype,
                date,
                token!!,
                booking_list!!,
                selected_zone,
                transfer_type,
                is_driver_based,
                maidId.toString()
            )

            if (response.status.equals("success")) {

                when (response.transferAll) {
                    "0" -> DeleteTransferbooking(context, bookingId)
                   /* "1" -> {
                        fetDatFromServer(context, currentDate)
*//*
                        if (response.transferDetails != null && response.transferDetails.isNotEmpty()) {
                            Log.d("Delet List", "getBookingsTransfer: " + response.transferDetails)

                            for (bookingItem in response.transferDetails) {
                                DeleteTransferbooking(context, bookingItem.bookingId.toString())
                            }
                        }
*//*
                    }*/

                }


              /*  if (response.transferDetails != null && response.transferDetails.isNotEmpty()) {

                    for (bookingItem in response.transferDetails) {
                        transferupdates(context, bookingItem.timeing, bookingItem.noOfMaids)
                    }
                }*/


                _transferStatus.value = response

            } else {
                _transferStatus.value = response
            }


        } catch (e: Exception) {
            _transferStatus.value =
                ResponseBookingTransfer(message = "Something went wrong", status = "error")
        }

    }

    suspend fun fetDatFromServer(context: Context, currentDate: String) {


        try {
            val fetchresponse = ApiClient.getRetrofit().getBookings(prefs.user_id.toString(), prefs.user_type.toString(), currentDate,prefs.token.toString())

            Log.d("Refresh", "getBookings: " + fetchresponse)

            if (fetchresponse.status.equals("success")) {
                insertToDb(context, fetchresponse.schedule)
                _bookingsDetailsfetch.value =fetchresponse
            }else{
                _bookingsDetailsfetch.value =fetchresponse
            }
           // _transferStatus.value = ResponseBookingTransfer(message = "Booking transferred successfully", status = "success")

        } catch (e: Exception) {
            Log.e("throw", "fetDatFromServer: "+e.message)
            _bookingsDetailsfetch.value =ResponseBookings(message = "Something went wrong", status = "error")

            // _transferStatus.value = ResponseBookingTransfer(message = "Booking transferred successfully", status = "success")

        }

    }
    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }
    suspend fun transferupdates(context: Context, timeing: String?, noOfMaids: Int?) {
        BookingsRepository.transferupdates(context, timeing, noOfMaids)

    }

    suspend fun getStartService(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        bookingId: ArrayList<String>,
        status: String,
        starttime: String,
        booking_id: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getStartService(
                userId.toString(),
                userType.toString(),
                currentDate,
                token.toString(),
                bookingId, // String
                status, "", "", "", "", "", ""
            )

            if (response.status.equals("success")) {

                val sdf = SimpleDateFormat("HH:mm aaa")  //HH:mm aaa
                val starttime = sdf.format(Date())


                _startServiceStatus.value = response
                updateStartServiceStatus(
                    requireContext, booking_id,
                    response.serviceStatus.toString(), response.starttime,response.data
                )


            } else {
                _startServiceStatus.value = response
            }


        } catch (e: Exception) {
            _startServiceStatus.value =
                ResponseStartService(message = "Something went wrong", status = "error")
        }
    }




    private suspend fun DeleteTransferbooking(context: Context, bookingId: String) {
        BookingsRepository.DeleteTransferbooking(context, bookingId)
    }

    private suspend fun deleteTransferedall(context: Context) {
        BookingsRepository.deleteTransferedallBooking(context, "0")
    }

    private suspend fun updateStartServiceStatus(
        context: Context,
        bookingId: String,
        serviceStatus: String,
        starttime: String?,
        data: List<DataItem?>?
    ) {
        BookingsRepository.updatestartbookingStatus(context, bookingId, serviceStatus, starttime,data)
    }

    suspend fun getMaidbookingsDetailslist(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        maidId: String?,
        bookingId: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getMaidbookingsDetailslist(
                userId.toString(),
                userType.toString(),
                currentDate,
                token.toString(),
                maidId.toString(),
                bookingId

            )
            if (response.status.equals("success")) {
                _maidBookingsStatus.value = response


            } else {
                _maidBookingsStatus.value = response
            }

        } catch (e: Exception) {
            _maidBookingsStatus.value =
                ResponseMaidBookingDetailsList(message = "Something went wrong", status = "error")
        }
    }

    suspend fun getMaidStatusChange(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        status: Int,
        maidId: String?,
        si_no: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getMaidStatusChange(
                userId.toString(),
                userType.toString(),
                currentDate,
                token.toString(),
                status.toString(), maidId.toString(), si_no

            )
            if (response.status.equals("success")) {
                _maidStatusChange.value = response

                updateMaidStatusChange(
                    requireContext, response.maidId.toString(),
                    response.maidDetails?.maidAttandence.toString()
                )

            } else {
                _maidStatusChange.value = response
            }

        } catch (e: Exception) {
            _maidStatusChange.value =
                ResponseMaidStatusChange(message = "Something went wrong", status = "error")
        }


    }

    private suspend fun updateMaidStatusChange(
        context: Context,
        maidId: String,
        maidAttandence: String
    ) {
        BookingsRepository.updateMaidStatusChange(context, maidId, maidAttandence)
    }




    suspend fun NewMailListFromDB(
        requireContext: Context,
        customer_id: String,
        shift_starttime: String,
        shift_endtime: String,
        booking_date: String,
        customeraddress: String
    ): List<MaidTable>? {
        newmaidlistFromDb = BookingsRepository.NewMailListFromDB(requireContext, customer_id,shift_starttime,shift_endtime,booking_date,customeraddress) //bookingId,
        return newmaidlistFromDb
    }

     fun MailListFromDB(
        requireContext: Context,
        customer_id: String,
        shift_starttime: String,
        shift_endtime: String,
        booking_date: String,
        customeraddress: String
    ): LiveData<List<MaidTable>?>? {
        mainmaidlistFromDb = BookingsRepository.MailListFromDB(requireContext, customer_id,shift_starttime,shift_endtime,booking_date,customeraddress) //bookingId,
        return mainmaidlistFromDb
    }

    suspend fun NewCustomerDetailsFromDB(
        requireContext: Context,
        bookingId: String
    ): List<BookingsTable>? {
        newCustomerDetailsFromDb =
            BookingsRepository.NewCustomerDetailsFromDB(requireContext, bookingId)
        return newCustomerDetailsFromDb
    }


    //Invoice Api
    private val _detailInvoice = MutableLiveData<ResponseInvoice?>()
    val detailInvoice: LiveData<ResponseInvoice?>
        get() = _detailInvoice

    suspend fun getDetailInvoice(
        user_id: String,
        user_type: String,
        token: String?,
        booking_id: String,
        date: String
    ) {

        try {
            val response =
                ApiClient.getRetrofit().getInvoice(user_id, token!!, date, user_type, booking_id)

            if (response.status.equals("success")) {
                _detailInvoice.value = response
            } else {
                _detailInvoice.value = response
            }
        } catch (e: Exception) {
            _detailInvoice.value =
                ResponseInvoice(message = "Something Went Wrong", status = "Error")
        }
    }

    suspend fun getKeyStatusChange(
        requireContext: Context,
        userId: Int,
        userType: String?,
        token: String?,
        status: String,
        customerId: String?
    ) {
        try {
            var response = ApiClient.getRetrofit().getAddRemoveKey(
                userId.toString(),
                userType.toString(),
                token.toString(),
                status,
                customerId.toString()
            )

            if (response.status.equals("success")){
                response= ResponseAddRemoveKey(message = response.message,status=response.status,key_statuss = status)
                Log.d("keystatus", "getKeyStatusChange: "+response)
                _keyStatusChange.value = response
                updatekeyStatusChange(requireContext,customerId.toString(),status)
            }else{
                _keyStatusChange.value = response
            }
        } catch (e: java.lang.Exception) {
            _keyStatusChange.value =
                ResponseAddRemoveKey(message = "Something went wrong", status = "error")
        }
    }


     private suspend fun updatekeyStatusChange(
         context: Context,
         customerId: String,
         status: String
     ) {
         BookingsRepository.updateKeyStatusChange(context, customerId, status)
     }

}