package com.azinova.emaidcrewapp

import android.app.Application
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.util.Log
import androidx.multidex.MultiDex
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.gms.common.util.DeviceProperties.isTablet
import net.khirr.library.foreground.Foreground


class App: Application() ,Foreground.Listener{
    private val prefs = SharedPref

    override fun onCreate() {
        super.onCreate()

        //  Initialize
        Foreground.Companion.init(this)
        //  Add listener
        Foreground.Companion.addListener(this)
        val foregroundListener = object: Foreground.Listener {
            override fun background() {
                Log.e("Listener Foreground", "Go to background")
            }

            override fun foreground() {
                Log.e("Listener Foreground", "Go to foreground")
            }
        }

        Foreground.addListener(foregroundListener)
    }

    override fun background() {
        Log.e("Foreground", "Go to background");
    }

    override fun foreground() {
        Log.e("Foreground", "Go to foreground");
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


}