package com.azinova.emaidcrewapp.ui.fragment.ui.payment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_payment.*
import java.text.SimpleDateFormat
import java.util.*


class PaymentFragment : Fragment() {
    private lateinit var paymentViewModel: PaymentFragmentViewModel

    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: MainActivity

    private lateinit var rv_paymentlist: RecyclerView
    lateinit var paymentListAdapter: PaymentListAdapter


    var newdate: String = " "

    lateinit var datePickerDialog: DatePickerDialog
    var year = 0
    var month = 0
    var dayOfMonth = 0
    lateinit var calendar: Calendar


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        paymentViewModel = ViewModelProvider(this).get(PaymentFragmentViewModel::class.java)

        parent = activity as MainActivity

        rv_paymentlist = view.findViewById(R.id.rv_paymentlist)

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        newdate = sdf.format(Date())

        if (prefs.is_payment_hide.equals("yes")){
            txt_paymenttotal.text = "AED " + "0.00"
            rv_paymentlist.visibility = View.GONE
            l_noPaymentsList.visibility = View.VISIBLE
        }

        setUpPaymentlist(newdate)
        observedIn()
        onClick()

    }

    private fun onClick() {
        requireActivity().img_calender
                .setOnClickListener {
                    calendar = Calendar.getInstance()
                    year = calendar.get(Calendar.YEAR)
                    month = calendar.get(Calendar.MONTH)
                    dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

                    datePickerDialog = DatePickerDialog(
                        requireContext(),
                        { datePicker, year, month, day ->
                            var formatmonth: String = month.toString()
                            var formatDayOfMonth: String = "" + day

                            if (month < 10) {

                                formatmonth = "0" + (month+1);
                            }
                            if (day < 10) {

                                formatDayOfMonth = "0" + day;
                            }
                            Log.d("TAG", "onClick: a"+ formatDayOfMonth + "/" + formatmonth + "/" + year)


                            newdate = formatDayOfMonth + "/" + formatmonth + "/" + year
                            requireActivity().txt_title.text = newdate
                            setUpPaymentlist(newdate)

                        }, year, month, dayOfMonth
                    )

                    datePickerDialog.setButton(
                            DialogInterface.BUTTON_NEGATIVE,
                            "Cancel",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (which == DialogInterface.BUTTON_NEGATIVE) {

                                    datePickerDialog.dismiss()

                                }
                            })
                    datePickerDialog.show()
                }
    }

    private fun observedIn() {
        paymentViewModel.paymentlistStatus.observe(
                viewLifecycleOwner,
                Observer { paymentlistStatus ->
                    Log.d("TAG", "getPaymentlist: " + paymentlistStatus)

                    if (paymentlistStatus.status.equals("success")) {
                        Loader.hideLoader()
                        if (prefs.is_payment_hide.equals("yes")){
                            txt_paymenttotal.text = "AED " + "0.00"
                            rv_paymentlist.visibility = View.GONE
                            l_noPaymentsList.visibility = View.VISIBLE
                        }else {
                            txt_paymenttotal.text = "AED " + paymentlistStatus.total


                            if (paymentlistStatus.paymentList?.size != 0) {
                                rv_paymentlist.visibility = View.VISIBLE
                                l_noPaymentsList.visibility = View.GONE

                                paymentListAdapter =
                                    PaymentListAdapter(
                                        requireContext(),
                                        paymentlistStatus.paymentList
                                    )
                                val lmList = LinearLayoutManager(context)
                                lmList.orientation = LinearLayoutManager.VERTICAL
                                rv_paymentlist.setLayoutManager(lmList)
                                rv_paymentlist.setAdapter(paymentListAdapter)
                            } else {
                                rv_paymentlist.visibility = View.GONE
                                l_noPaymentsList.visibility = View.VISIBLE
                            }
                        }
                    } else if (paymentlistStatus.status.equals("token_error")){
                        Loader.hideLoader()

                        clearLogin()
                        parent.stopServiceTracker()
                        database?.clearAllTables()
                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        activity?.finish()
                        Toast.makeText(requireContext(), paymentlistStatus.message, Toast.LENGTH_SHORT)
                            .show()


                    } else {
                        Loader.hideLoader()
                        Toast.makeText(requireContext(), paymentlistStatus.message, Toast.LENGTH_SHORT)
                                .show()

                    }
                })
    }
    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }
    @SuppressLint("SimpleDateFormat")
    private fun setUpPaymentlist(newdate: String) {


        var spf = SimpleDateFormat("dd/MM/yyyy")
        val newDatee = spf.parse(newdate)
        spf = SimpleDateFormat("yyyy-MM-dd")
        val date:String = spf.format(newDatee)





        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {

            getPaymentlist(prefs.user_id.toString(), date, prefs.user_type.toString(), prefs.token)
        } else {
            Loader.hideLoader()
            Snackbar.make(outerpayment, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun getPaymentlist(id: String, date: String, userstatus: String, token: String?) {
        paymentViewModel.getPaymentlist(id, date, userstatus, token)
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

}