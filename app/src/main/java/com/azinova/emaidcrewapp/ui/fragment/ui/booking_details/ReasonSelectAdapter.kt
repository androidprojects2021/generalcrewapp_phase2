package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.cancel_reasons.Reasons

class ReasonSelectAdapter(val requireContext: Context,val reasons: List<Reasons?>?,val onItemClicked: (Reasons?, Int ) -> Unit) :  RecyclerView.Adapter<ReasonSelectAdapter.DeviceHolder>() {

    private var selectedPosition = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReasonSelectAdapter.DeviceHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_reason_list, parent, false)
        return DeviceHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: DeviceHolder, position: Int) {
        Log.d("Adapter check", "onBindViewHolder: "+reasons?.get(position)?.reason_name)
        holder.txt_reason.text = reasons?.get(position)?.reason_name

        holder.checkb_reason.tag = position

        if (position == selectedPosition) {
            holder.checkb_reason.isChecked = true
        } else {
            holder.checkb_reason.isChecked = false
        }
        holder.checkb_reason.setOnClickListener {
            onStateChangedListener(holder.checkb_reason, position) }
    }

    private fun onStateChangedListener(selected: CheckBox, position: Int) {
        if (selected.isChecked()) {
            selectedPosition = position
            onItemClicked.invoke(reasons?.get(position),position)
        } else {
            selectedPosition = -1
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return reasons!!.size
    }

    class DeviceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var checkb_reason: CheckBox
        lateinit var txt_reason: TextView

        init {
            checkb_reason = itemView.findViewById(R.id.checkb_reason) as CheckBox
            txt_reason = itemView.findViewById(R.id.txt_reason) as TextView
        }
    }
}