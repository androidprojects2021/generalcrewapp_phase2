package com.azinova.emaidcrewapp.ui.fragment.ui.payment

import androidx.lifecycle.*
import com.azinova.emaidcrewapp.model.response.payment.ResponsePayment
import com.azinova.emaidcrewapp.network.ApiClient
import kotlinx.coroutines.launch

class PaymentFragmentViewModel : ViewModel() {

    private val _paymentlistStatus = MutableLiveData<ResponsePayment>()
    val paymentlistStatus: LiveData<ResponsePayment>
        get() = _paymentlistStatus



    fun getPaymentlist(id: String, date: String, userstatus: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getPaymentlist(id,userstatus,date,token!!)
                if (response.status.equals("success")) {
                    _paymentlistStatus.value = response


                } else {
                    _paymentlistStatus.value = response
                }
            } catch (e: Exception) {
                _paymentlistStatus.value =  ResponsePayment(message = "Something went wrong", status = "Error")

            }
        }
    }


}