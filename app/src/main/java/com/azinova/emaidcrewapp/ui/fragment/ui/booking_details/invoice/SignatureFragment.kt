package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice

import android.Manifest
import android.Manifest.permission
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.FileUtils.copy
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.utils.SharedPref
import com.github.gcacace.signaturepad.views.SignaturePad
import com.google.android.gms.common.util.IOUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.*
import java.util.Collections.copy
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE

import android.Manifest.permission.READ_EXTERNAL_STORAGE

import android.os.Build

import androidx.annotation.RequiresApi
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE

import androidx.core.content.ContextCompat

import android.Manifest.permission.READ_EXTERNAL_STORAGE

import android.app.Activity
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.DownloadManager


class SignatureFragment : Fragment() {

    private val REQUEST_EXTERNAL_STORAGE = 1
    private val PERMISSIONS_STORAGE = arrayOf(
        READ_EXTERNAL_STORAGE,
        WRITE_EXTERNAL_STORAGE
    )
    private lateinit var mSignaturePad: SignaturePad
    private lateinit var mClearButton: Button
    private lateinit var mSaveButton: Button

    var file: File? = null

    var multipartBodyImage: MultipartBody.Part? = null

    lateinit var signatureViewModel: SignatureViewModel

    lateinit var invoiceId: String

    private val prefs = SharedPref

    var lastWord = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signature, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signatureViewModel = ViewModelProvider(this)[SignatureViewModel::class.java]
        verifyStoragePermissions(requireActivity())


        invoiceId = requireArguments().getString("invoiceId").toString()

        mSignaturePad = view.findViewById(R.id.signature_pad)

        onObserve()

        mSignaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {}
            override fun onSigned() {
                mSaveButton!!.isEnabled = true
                mClearButton!!.isEnabled = true
            }

            override fun onClear() {
                mSaveButton!!.isEnabled = false
                mClearButton!!.isEnabled = false
            }
        })

        mClearButton = view.findViewById<View>(R.id.clear_button) as Button
        mSaveButton = view.findViewById<View>(R.id.save_button) as Button

        mClearButton.setOnClickListener(View.OnClickListener { mSignaturePad.clear() })

        mSaveButton.setOnClickListener(View.OnClickListener {
            val signatureBitmap = mSignaturePad.getSignatureBitmap()
            if (signatureBitmap != null) {
                try {
                    val imageUri = signatureBitmap?.let {
                        context?.let { it1 ->
                            getImageUri(it1, it)
                        }
                    }
                    val imagePath = imageUri?.let { getPath(it) }
                    if (imagePath != null) {
                        file = File(imagePath)

                    } else {
                        Log.e("ImageFile", "Null signature Found")
                    }
                    Log.e("Capture", "captureImagePath$signatureBitmap")
                    Log.e("CapturePath", "captureImagePath$imagePath")

                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(requireContext(), "Add Sign", Toast.LENGTH_SHORT).show()
            }

            multipartBodyImage = pathToMultiPart()

            if (multipartBodyImage != null) {
                GlobalScope.launch(Dispatchers.Main) {
                    Loader.showLoader(requireContext())

                    try {
                        signatureViewModel.uploadSign(
                            multipartBodyImage,
                            RequestBody.create(
                                "multipart/form-data".toMediaTypeOrNull(),
                                prefs.user_id.toString()
                            ),
                            RequestBody.create(
                                "multipart/form-data".toMediaTypeOrNull(),
                                prefs.token!!
                            ),
                            RequestBody.create(
                                "multipart/form-data".toMediaTypeOrNull(),
                                prefs.user_type.toString()
                            ),
                            RequestBody.create(
                                "multipart/form-data".toMediaTypeOrNull(),
                                invoiceId
                            )
                        )
                    } catch (e: NullPointerException) {
                        Toast.makeText(requireContext(), e.message, Toast.LENGTH_SHORT).show()
                        Log.e("SignUpload", "Exception ${e.message}")
                    }

                }
            } else {
                Toast.makeText(requireContext(), "Add Sign", Toast.LENGTH_SHORT).show()
            }

        })
    }



    private fun onObserve() {
        signatureViewModel.uploadSignature.observe(viewLifecycleOwner, Observer {

            if (it?.status.equals("success")) {
                prefs.customer_sign = true
                prefs.invoice_id = invoiceId
                DetailInvoiceFragment.pdfUrl = ""

                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT).show()
                activity?.onBackPressed()
            } else {
                prefs.customer_sign = false
                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT).show()
                activity?.onBackPressed()
            }
        })


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {

            REQUEST_EXTERNAL_STORAGE -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size <= 0
                    || grantResults[0] != PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(
                        requireContext(),
                        "Cannot write images to external storage",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun addSvgSignatureToGallery(signatureSvg: String?): Boolean {
        var result = false
        try {
            val svgFile = File(
                getAlbumStorageDir("General Crew"),
                String.format("Signature_%d.svg", System.currentTimeMillis())
            )
            val stream: OutputStream = FileOutputStream(svgFile)
            val writer = OutputStreamWriter(stream)
            writer.write(signatureSvg)
            writer.close()
            stream.flush()
            stream.close()
            scanMediaFile(svgFile)
            result = true
            Log.d("AStro", "SVG file = $svgFile")

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun addJpgSignatureToGallery(signatureBitmap: Bitmap?): Boolean {
        var result = false
        try {
            val photo = File(
                getAlbumStorageDir("General Crew"),
                String.format("Signature_%d.jpg", System.currentTimeMillis())
            )
            if (signatureBitmap != null) {
                saveBitmapToJPG(signatureBitmap, photo)
            }
            scanMediaFile(photo)
            result = true
            Log.d("AStro", "signature photo = $photo")

        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        requireActivity().sendBroadcast(mediaScanIntent)
    }

    private fun verifyStoragePermissions(activity: FragmentActivity) {
        // Check if we have write permission
        val permissionWrite = ActivityCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val permissionRead =
            ContextCompat.checkSelfPermission(activity, READ_EXTERNAL_STORAGE)

        val externalStoragePermissionGranted =
            permissionWrite === PackageManager.PERMISSION_GRANTED &&
                    permissionRead === PackageManager.PERMISSION_GRANTED

        if (!externalStoragePermissionGranted) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE
            )
        }
    }

    fun getAlbumStorageDir(albumName: String?): File? {
        // Get the directory for the user's public pictures directory.
        val file = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ), albumName
        )
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created")
        }
        return file
    }

    @Throws(IOException::class)
    fun saveBitmapToJPG(bitmap: Bitmap, photo: File?) {
        val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()
    }

    private fun getPath(uri: Uri): String? {

        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor =
            context?.getContentResolver()?.query(uri, projection, null, null, null)!!
        val column_index: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s: String = cursor.getString(column_index)
        cursor.close()
        return s
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {

        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
            inContext.contentResolver,
            inImage,
            "Title",
            null
        )
        return Uri.parse(path)

    }

    private fun pathToMultiPart(): MultipartBody.Part? {

        var requestFile = file?.let {
            it.asRequestBody("multipart/form-data".toMediaTypeOrNull())
            RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                it
            )
        }

        var pic =
            requestFile?.let { MultipartBody.Part.createFormData("signature", file!!.name, it) }
        return pic
    }

    private fun downloadPdf(url: String, titlepdf: String) {
        val request = DownloadManager.Request(Uri.parse(url))
        val title = "EmaidCrew_$titlepdf"
        request.setTitle(title)
        if (readAndWriteExternalStorage(context)) {

            try {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner()
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                }
                request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    "$title.pdf"
                )
                val downloadManager =
                    requireActivity().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
                request.setMimeType("application/pdf")
                request.allowScanningByMediaScanner()
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
                downloadManager?.enqueue(request)
            }catch (e:Exception){}
        }
//        sharePdf(pdfUrl, lastWord)
    }
    fun readAndWriteExternalStorage(context: Context?): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                requireContext(),
                READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireContext(), WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                (context as Activity?)!!,
                arrayOf(
                    READ_EXTERNAL_STORAGE,
                    WRITE_EXTERNAL_STORAGE
                ),
                1
            )
            false
        } else {
            true
        }
    }

}