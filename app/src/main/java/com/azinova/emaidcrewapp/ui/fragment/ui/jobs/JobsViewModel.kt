package com.azinova.emaidcrewapp.ui.fragment.ui.jobs

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.database.tables.BookingsTable
import com.azinova.emaidcrewapp.database.tables.MaidTable
import com.azinova.emaidcrewapp.model.response.bookings.ResponseBookings
import com.azinova.emaidcrewapp.model.response.bookings.ScheduleItem
import com.azinova.emaidcrewapp.model.response.schedules.MaidDetails
import com.azinova.emaidcrewapp.model.response.schedules.SceduleTimeingResponse
import com.azinova.emaidcrewapp.model.response.schedules.ScheduleResponse
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class JobsViewModel : ViewModel() {
    private val prefs = SharedPref

    var bookingsFromDb: LiveData<List<BookingsTable>>? = null


    var bookingdatesFromDb: List<String>? = null

    var bookingTimeingsFromDb: List<String>? = null

    var schedulesFromDb: List<BookingsTable>? = null

    var maidListFromDb: List<MaidTable>? = null

    private val _bookingsDetailsRefresh = MutableLiveData<ResponseBookings>()  //ResponseBookings
    val bookingsDetailsRefresh: LiveData<ResponseBookings>
        get() = _bookingsDetailsRefresh


    fun getBookingDetails(context: Context, selecteddate: String): LiveData<List<BookingsTable>>? {
        bookingsFromDb = BookingsRepository.getBookingDetails(context, selecteddate)
        return bookingsFromDb
    }

    suspend fun getBookingTimeings(
        context: Context,
        selecteddate: String,
        FilterPosition: Int,
        filter: ArrayList<Int>
    ): ArrayList<SceduleTimeingResponse>? {

        //Multi Selection Using filter array
        bookingTimeingsFromDb =
            BookingsRepository.getBookingTimeings(context, selecteddate,FilterPosition,filter)
        Log.d("Status Based ......Time", "callBookingDate: "+bookingTimeingsFromDb)


        val bookingList = ArrayList<SceduleTimeingResponse>()
        bookingTimeingsFromDb?.let {

            it.forEach {
                //Multi Selection Using filter array
                val scheduleListFromDB = getschedulesbytime(context, selecteddate, it,FilterPosition,filter)
                val bookinglist = SceduleTimeingResponse()

                if (scheduleListFromDB != null && scheduleListFromDB.isNotEmpty()) {
                    val customerDetailArray = ArrayList<ScheduleResponse>()

                    Log.d("Booking 1 - ", "getBookingTimeings 1: "+scheduleListFromDB)

                    for (bookingListItem in scheduleListFromDB) {
                        Log.d("Booking 2 - ", "getBookingTimeings 2: "+bookingListItem)

                        val maidDetailsList = ArrayList<MaidDetails>()
                        val scheduleResponsenew = ScheduleResponse()

                        //getMaidList condition changeing from booking_id to customer_id & start time
                        //bookingListItem.booking_id.toString()
                        val maidListFromDb = BookingsRepository.getMaidList(context, bookingListItem.customer_id.toString(),bookingListItem.shift_start,bookingListItem.shift_end,selecteddate,bookingListItem.customerAddress)
                        Log.d("TAG", "getBookingTimeings: "+maidListFromDb)

                        bookinglist.noOfMaids = bookingListItem.no_of_maids
                        bookinglist.scheduleDate = bookingListItem.schedule_date
                        bookinglist.scheduleTimeing = bookingListItem.timeing

                        scheduleResponsenew.schedule_date = bookingListItem.schedule_date
                        scheduleResponsenew.customer_id = bookingListItem.customer_id
                        scheduleResponsenew.booking_id = bookingListItem.booking_id
                        scheduleResponsenew.customer_name = bookingListItem.customer_name
                        scheduleResponsenew.service_status = bookingListItem.service_status
                        scheduleResponsenew.shift_start = bookingListItem.shift_start
                        scheduleResponsenew.shift_end = bookingListItem.shift_end
                        scheduleResponsenew.area = bookingListItem.area
                        scheduleResponsenew.cleaning_material = bookingListItem.cleaning_material
                        scheduleResponsenew.customerAddress = bookingListItem.customerAddress
                        scheduleResponsenew.customerMobile = bookingListItem.customerMobile
                        scheduleResponsenew.customerType = bookingListItem.customerType
                        scheduleResponsenew.customerCode = bookingListItem.customerCode
                        scheduleResponsenew.keyStatus = bookingListItem.keyStatus
                        scheduleResponsenew.bookingNote = bookingListItem.bookingNote
                        scheduleResponsenew.customernotes = bookingListItem.customernotes
                        scheduleResponsenew.serviceFee = bookingListItem.serviceFee
                        scheduleResponsenew.zone = bookingListItem.zone
                        scheduleResponsenew.mop = bookingListItem.mop
                        scheduleResponsenew.extraService = bookingListItem.extraService
                        scheduleResponsenew.tools = bookingListItem.tools
                        scheduleResponsenew.total = bookingListItem.total
                        scheduleResponsenew.balance = bookingListItem.balance
                        scheduleResponsenew.outstandingBalance = bookingListItem.outstandingBalance
                        scheduleResponsenew.servicetype = bookingListItem.servicetype
                        scheduleResponsenew.paymentStatus = bookingListItem.paymentStatus
                        scheduleResponsenew.paidAmount = bookingListItem.paidAmount
                        scheduleResponsenew.customerLatitude = bookingListItem.customerLatitude
                        scheduleResponsenew.customerLongitude = bookingListItem.customerLongitude
                        scheduleResponsenew.starttime = bookingListItem.starttime
                        scheduleResponsenew.endtime = bookingListItem.endtime
                        scheduleResponsenew.istransfer = bookingListItem.istransfer
                        scheduleResponsenew.vaccum_cleaner = bookingListItem.vaccum_cleaner
                        scheduleResponsenew.booking_type = bookingListItem.booking_type
                        scheduleResponsenew.username = bookingListItem.username

                        maidListFromDb?.let {
                            for (maidList in maidListFromDb) {
                                val maidDetails = MaidDetails()
                                maidDetails.customer_id = maidList.customer_id
                                maidDetails.booking_id = maidList.booking_id
                                maidDetails.maid_id = maidList.maid_id
                                maidDetails.maid_name = maidList.maid_name
                                maidDetails.maid_attandence = maidList.maid_attandence
                                maidDetails.service_status = maidList.service_status
                                maidDetails.maid_image = maidList.maid_image
                                maidDetails.maid_serviceFee = maidList.maid_serviceFee
                                maidDetails.maid_total = maidList.maid_total
                                maidDetails.maid_shift_start = maidList.maid_shift_start
                                maidDetails.maid_shift_end = maidList.maid_shift_end
                                maidDetails.maid_schedule_date = maidList.schedule_date
                                maidDetailsList.add(maidDetails)
                                scheduleResponsenew.maidlist = maidDetailsList

                            }
                        }
                        customerDetailArray.add(scheduleResponsenew)
                        bookinglist.schedulebookings = customerDetailArray

                    }


                }
                bookingList.add(bookinglist)
            }
        }
        return bookingList

    }

    suspend fun getBookingDates(context: Context): List<String>? {
        bookingdatesFromDb = BookingsRepository.getBookingDates(context)
        return bookingdatesFromDb
    }

    suspend fun getschedulesbytime(
        context: Context,
        scheduleDate: String?,
        timeing: String?,
        FilterPosition: Int,
        filter: ArrayList<Int>
    ): List<BookingsTable>? {
        schedulesFromDb = BookingsRepository.getscheduleDetailsbytime(context, scheduleDate, timeing,FilterPosition,filter)
        return schedulesFromDb
    }

    suspend fun fetDatFromServer(context: Context, currentDate: String) {
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val dates = sdf.format(Date())

        try {
            val response = ApiClient.getRetrofit().getBookings(prefs.user_id.toString(), prefs.user_type.toString(), currentDate,prefs.token.toString())

            Log.d("Refresh", "getBookings: " + response)

            if (response.status.equals("success")) {
                insertToDb(context, response.schedule)

                _bookingsDetailsRefresh.value = response
            } else {
                _bookingsDetailsRefresh.value = response
            }
        } catch (e: Exception) {
            Log.e("throw", "fetDatFromServer: "+e.message)
            _bookingsDetailsRefresh.value =  ResponseBookings(message = "Something went wrong", status = "error")

        }

    }



    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }

    fun insertDate(currentdate: String) {

        BookingsRepository.insertDate(currentdate)

    }


}