package com.azinova.emaidcrewapp.commons

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.azinova.emaidcrewapp.R

object LogoutConfirmDialog {

    private lateinit var builder: AlertDialog.Builder
    private lateinit var customLayout: View
    private lateinit var dialog: AlertDialog

    fun showDialog(context: Context,logoutClick: LogoutClick){
        try {
            builder = AlertDialog.Builder( context )
            customLayout = LayoutInflater.from( context ).inflate(R.layout.logout_layout,null)
            builder.setView(customLayout)
            dialog = builder.create()
            dialog.setCancelable(false)

            val confirm = customLayout.findViewById<TextView>(R.id.confirm)
            val cancel = customLayout.findViewById<TextView>(R.id.cancel)

            confirm.setOnClickListener {
                logoutClick.proceedLogout()
                hideDialog() }

            cancel.setOnClickListener { hideDialog() }


            dialog.show()
        } catch (e: Exception) {
        }
    }

    fun hideDialog(){
        try {
            dialog.dismiss()
        } catch (e: Exception) {
        }
    }



    interface LogoutClick{
        fun proceedLogout()
    }

}