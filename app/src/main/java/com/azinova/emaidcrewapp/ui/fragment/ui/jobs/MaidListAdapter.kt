package com.azinova.emaidcrewapp.ui.fragment.ui.jobs

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.schedules.MaidDetails
import com.bumptech.glide.Glide
import java.io.File

class MaidListAdapter(val context: Context,val maidlist: List<MaidDetails>?) : RecyclerView.Adapter<MaidListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaidListAdapter.ViewHolder {
      return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_main_maidlist, parent, false))
    }

    override fun onBindViewHolder(holder: MaidListAdapter.ViewHolder, position: Int) {
        if (maidlist?.get(position)?.maid_image?.isNotEmpty()!!) {



            val savedUri: Uri? =
                maidlist?.get(position)?.maid_image?.toUri()
            Glide.with(context)
                .load( File(savedUri?.getPath()))
                .placeholder(R.drawable.nomaidimage)
                .error(R.drawable.nomaidimage)
                .into(holder.img_maid)


        }
        holder.tv_maidname.text = maidlist?.get(position)?.maid_name

    }

    override fun getItemCount(): Int {
        return maidlist?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_maidname = itemView.findViewById<TextView>(R.id.tv_maidname)
        var img_maid = itemView.findViewById<ImageView>(R.id.img_maid)
    }
}