package com.azinova.emaidcrewapp.utils.notification

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.MultiDex
import com.google.android.gms.common.util.DeviceProperties

open class BaseActivity : AppCompatActivity() {
    var myReceiver = MyReceiver()

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    override fun onCreate(arg0: Bundle?) {
        super.onCreate(arg0)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)


        appInFront = true
        Log.d("TAG", "onCreate: " + "appInFront  ->")
    }

    private fun isTablets(baseActivity: BaseActivity): Boolean {
        return ((baseActivity.resources.configuration.screenLayout
                and Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE)
    }

    @SuppressLint("MissingSuperCall")
    override fun onResume() {
        super.onStart()
        ServiceStart()
        appInFront = true
    }

    override fun onPause() {
        super.onPause()
        appInFront = false

        Log.d("TAG", "onCreate: " + "appInFront   <-")
        GlobalManager.getdata().setCurrentActivity("")


        unregisterReceiver(myReceiver)

    }

    fun ServiceStart() {

        Log.e("TAG", "ServiceStart: ")
        val intentFilter = IntentFilter()
        intentFilter.addAction(Myservices.MY_ACTION)


        registerReceiver(myReceiver, intentFilter)

    }

    class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, arg1: Intent) {
            try {
                Log.d("TAG", "onReceive: " + arg1.extras!!.getString("isNotification"))
                if (arg1.extras!!.getString("isNotification") != null) {

                    Log.e("Broadcast", "Maid : ${arg1.extras!!.getString("maid")} ")
                    Log.e("Broadcast", "Customer : ${arg1.extras!!.getString("customer")} ")
                    Log.e("Broadcast", "bookingTime : ${arg1.extras!!.getString("bookingTime")} ")

                    NotificationDioalogue.NotificationNew(
                        context,
                        arg1.extras!!.getString("heading").toString(),
                        arg1.extras!!.getString("message").toString(),
                        arg1.extras!!.getString("isNotification"),
                        arg1.extras!!.getString("title").toString(),
                        arg1.extras!!.getString("maid").toString(),
                        arg1.extras!!.getString("customer").toString(),
                        arg1.extras!!.getString("bookingTime").toString()

                    )

                    return
                }
                NotificationDioalogue.Notification(
                    context,
                    arg1.extras!!.getString("heading").toString(),
                    arg1.extras!!.getString("message").toString(),
                    arg1.extras!!.getString("isNotification"),
                    arg1.extras!!.getString("title").toString(),
                    arg1.extras!!.getString("maid").toString(),
                    arg1.extras!!.getString("customer").toString(),
                    arg1.extras!!.getString("bookingTime").toString()
                )
            } catch (e: Exception) {
            }

        }
    }


    companion object {
        var appInFront = false
        var dialog1: Dialog? = null
    }

    override fun onDestroy() {
        super.onDestroy()
        val builder = AlertDialog.Builder(this)

    }
}