package com.azinova.emaidcrewapp.network

import com.azinova.emaidcrewapp.utils.SharedPref
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    var prefs = SharedPref

    private lateinit var BASE_URL: String //live new General

    fun baseUrl(companyCode: String){
        BASE_URL = companyCode
    }

    fun getRetrofit(): ApiInterface {

         var interceptor = HttpLoggingInterceptor()
         interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

         var client = OkHttpClient.Builder()
             .addInterceptor(interceptor)
             .connectTimeout(60, TimeUnit.SECONDS)
             .readTimeout(60, TimeUnit.SECONDS)
             .writeTimeout(60, TimeUnit.SECONDS)
             .addNetworkInterceptor { chain: Interceptor.Chain ->
                 val request = chain.request().newBuilder()
                     .build()
                 chain.proceed(request)
             }.build()


        return Retrofit.Builder()
            .baseUrl(prefs.baseurl)  //.baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }


}