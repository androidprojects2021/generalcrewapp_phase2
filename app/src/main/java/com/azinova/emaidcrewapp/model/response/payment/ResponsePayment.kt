package com.azinova.emaidcrewapp.model.response.payment

import com.google.gson.annotations.SerializedName

data class ResponsePayment(

	@field:SerializedName("total")
	val total: Double? = null,

	@field:SerializedName("schedule")
	val paymentList: List<PaymentListItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class MaidDetailsItem(

	@field:SerializedName("maid_attendance")
	val maidAttendance: String? = null,

	@field:SerializedName("maid_id")
	val maidId: String? = null,

	@field:SerializedName("maid_country")
	val maidCountry: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("maid_name")
	val maidName: String? = null,

	@field:SerializedName("maid_attandence")
	val maidAttandence: String? = null,

	@field:SerializedName("maid_image")
	val maidimage: String? = null
)

data class PaymentListItem(

	@field:SerializedName("customer_type")
	val customerType: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("customer_mobile")
	val customerMobile: Any? = null,

	@field:SerializedName("cleaning_material")
	val cleaningMaterial: String? = null,

	@field:SerializedName("service_fee")
	val serviceFee: String? = null,

	@field:SerializedName("collected")
	val collected: String? = null,

	@field:SerializedName("schedule_date")
	val scheduleDate: String? = null,

	@field:SerializedName("key_status")
	val keyStatus: String? = null,

	@field:SerializedName("booking_note")
	val bookingNote: String? = null,

	@field:SerializedName("mop")
	val mop: String? = null,

	@field:SerializedName("billing")
	val billing: String? = null,

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("shift_start")
	val shiftStart: String? = null,

	@field:SerializedName("new_ref")
	val newRef: String? = null,

	@field:SerializedName("zone")
	val zone: String? = null,

	@field:SerializedName("service_status")
	val serviceStatus: String? = null,

	@field:SerializedName("paymentstatus")
	val paymentstatus: Int? = null,

	@field:SerializedName("shift_end")
	val shiftEnd: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("maid_details")
	val maidDetails: List<MaidDetailsItem?>? = null
)
