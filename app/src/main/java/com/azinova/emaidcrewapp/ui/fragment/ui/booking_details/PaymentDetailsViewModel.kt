package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.model.response.Invoice.ResponseInvoice
import com.azinova.emaidcrewapp.model.response.finish.FinishDataItem
import com.azinova.emaidcrewapp.model.response.finish.ResponseFinishService
import com.azinova.emaidcrewapp.network.ApiClient
import java.util.*

class PaymentDetailsViewModel : ViewModel() {

    private val _finishServiceStatus = MutableLiveData<ResponseFinishService?>()
    val finishServiceStatus: LiveData<ResponseFinishService?>
        get() = _finishServiceStatus


    fun reset(){
        _detailInvoice.value = null
        _finishServiceStatus.value = null
    }

    suspend fun getFinishService(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        bookingId: ArrayList<String>?,
        status: String,
        paytype: String,
        paid_value: String,
        receiptnumber: String,
        paymentmode: Int,
        outstandingBalance: String?,
        notes: String,
        booking_id: String
    ) {
        try {
            val response = ApiClient.getRetrofit().getFinishService(
                    userId.toString(),
                    userType.toString(),
                    currentDate,
                    token.toString(),
                    bookingId!!,//bookingId.toString(),
                    status, paytype,paid_value,receiptnumber,paymentmode.toString(),outstandingBalance.toString(),notes)

            Log.d("TAG", "getFinishService: "+response.copy())
            if (response.status.equals("success")) {

                updateFinishServiceStatus(
                        requireContext,booking_id,
                    response.serviceStatus.toString(),
                    response.amount,response.outstandingAmount.toString(),response.method,response.payment!!.toInt(),response.balance,response.endtime,response.data
                )



                _finishServiceStatus.value = response

            } else {
                _finishServiceStatus.value = response
            }
        } catch (e: Exception) {
            _finishServiceStatus.value = ResponseFinishService(message = "Something went wrong", status = "error")
        }
    }

    suspend fun updateFinishServiceStatus(
        context: Context,
        bookingId: String,
        serviceStatus: String,
        paid_amount: String?,
        outstandingAmount: String?,
        mop: String?,
        payment_status: Int,
        balance: Double?,
        endtime: String?,
        data: List<FinishDataItem?>?
    ) {
        var MOP = ""
        when(mop){
            "0" -> MOP = "Cash"
            "1" -> MOP = "Card"
            "2" -> MOP = "Cheque"
            "3" -> MOP = "Online"
            else -> MOP = ""
        }
        BookingsRepository.updatefinishbookingStatus(context, bookingId, serviceStatus,paid_amount,outstandingAmount,MOP,payment_status,balance,endtime,data)

    }




    //Invoice Api
    private val _detailInvoice = MutableLiveData<ResponseInvoice?>()
    val detailInvoice: LiveData<ResponseInvoice?>
        get() = _detailInvoice

    suspend fun getDetailInvoice(
            user_id: String,
            user_type: String,
            token: String?,
            booking_id: String,
            date: String
    ) {

        try {
            val response = ApiClient.getRetrofit().getInvoice(user_id, token!!, date, user_type, booking_id)

            if (response.status.equals("success")) {
                _detailInvoice.value = response
            } else {
                _detailInvoice.value = response
            }
        } catch (e: Exception) {
            _detailInvoice.value = ResponseInvoice(message = "Something Went Wrong", status = "Error")
        }
    }




}