package com.azinova.emaidcrewapp.ui.activity.detail

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.service.LocationService
import com.azinova.emaidcrewapp.utils.PermissionUtils
import com.azinova.emaidcrewapp.utils.SharedPref
import com.azinova.emaidcrewapp.utils.notification.BaseActivity
import kotlinx.android.synthetic.main.activity_booking_details.*

class BookingDetailsActivity : BaseActivity() {

    private lateinit var navController: NavController
    var actionbar: ActionBar? = null

    lateinit var customer_id: String
    lateinit var booking_id: String
    lateinit var booking_date: String
    lateinit var service_status: String
    lateinit var shift_starttime: String
    lateinit var shift_endtime: String
    lateinit var address: String
    private val prefs = SharedPref

    companion object {
        private var REQUEST_LOCATION_CODE = 101
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)
        setSupportActionBar(findViewById(R.id.toolbar))


        actionbar = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setDisplayShowHomeEnabled(true)

        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            customer_id = bundle.getString("Customer_Id").toString()
            booking_id = bundle.getString("Booking_id").toString()
            booking_date = bundle.getString("Schedule_date").toString()
            service_status = bundle.getString("Service_status").toString()
            shift_starttime = bundle.getString("Shift_start_time").toString()
            shift_endtime = bundle.getString("Shift_end_time").toString()
            address = bundle.getString("Customer_address").toString()

        }




        onLocation()

        navController = findNavController(R.id.fragment_details)
        Log.d("Schedule_date", "onCreate: " + booking_date)

        val args = bundleOf(
            "Booking_id" to booking_id,
            "Schedule_date" to booking_date,
            "Service_status" to service_status,
            "Customer_Id" to customer_id,
            "Shift_start_time" to shift_starttime,
            "Shift_end_time" to shift_endtime,
            "Customer_address" to address
        )


        navController.setGraph(R.navigation.nav_graph, args)
        onDestination()


    }

    fun stopServiceTracker() {
        stopService(Intent(this, LocationService::class.java))
    }


    private fun onDestination() {
        findNavController(R.id.fragment_details).addOnDestinationChangedListener { controller, destination, arguments ->

            when (destination.id) {
                R.id.navigation_bookingdetails -> {
                    actionbar?.show()
                    actionbar?.title = "Booking Details"
                    img_call.visibility = View.VISIBLE
                    layout_direction.visibility = View.VISIBLE
                    showPdf.visibility = View.GONE
                    layout_downloadPdf.visibility = View.GONE
                    layout_sharePdf.visibility = View.GONE

                }
                R.id.navigation_paymentdetails -> {
                    actionbar?.show()
//                    actionbar?.title = "Payment Details"
                    actionbar?.title = "Booking ID: $booking_id"
                    img_call.visibility = View.GONE
                    layout_direction.visibility = View.GONE
                    showPdf.visibility = View.GONE
                    layout_downloadPdf.visibility = View.GONE
                    layout_sharePdf.visibility = View.GONE

                }
                R.id.detailInvoiceFragment -> {
                    actionbar?.show()
                    actionbar?.title = "Detailed Invoice"
                    if (prefs.is_hide.equals("yes")) {
                        img_call.visibility = View.GONE
                        layout_direction.visibility = View.GONE
                        showPdf.visibility = View.GONE
                        layout_downloadPdf.visibility = View.GONE
                        layout_sharePdf.visibility = View.GONE
                    } else {
                        img_call.visibility = View.GONE
                        layout_direction.visibility = View.GONE
                        showPdf.visibility = View.VISIBLE
                        layout_downloadPdf.visibility = View.VISIBLE
                        layout_sharePdf.visibility = View.VISIBLE
                    }

                }
                R.id.signatureFragment -> {
                    actionbar?.hide()
                    img_call.visibility = View.GONE
                    layout_direction.visibility = View.GONE
                    showPdf.visibility = View.GONE
                    layout_downloadPdf.visibility = View.GONE
                    layout_sharePdf.visibility = View.GONE

                }
                else -> {
                    actionbar?.show()
                    actionbar?.title = " "
                    img_call.visibility = View.GONE
                    layout_direction.visibility = View.GONE
                    showPdf.visibility = View.GONE
                    layout_downloadPdf.visibility = View.GONE
                    layout_sharePdf.visibility = View.GONE

                }
            }
        }

    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun onLocation() {
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {

                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    BookingDetailsActivity.REQUEST_LOCATION_CODE
                )


            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            BookingDetailsActivity.REQUEST_LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                        }
                        else -> {

                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Location Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        onLocation()

    }


}