package com.azinova.emaidcrewapp.model.response.PdfUrl

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponsePdfUrl(
        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("pdfurl")
        val pdfurl: String? = null
): Serializable