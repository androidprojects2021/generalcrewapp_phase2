package com.azinova.emaidcrewapp.utils.notification;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

public class Myservices extends Service {

    public final static String MY_ACTION = "MY_ACTION";

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.d("Emaid crew 3", "generateNotification: ");

        notificationreceive(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    public void notificationreceive(Intent intent2) {


        Intent intent = new Intent();
        intent.setAction(MY_ACTION);
        Bundle b = new Bundle();
        Log.d("Emaid crew 4", "generateNotification: ");

        b.putString("message", intent2.getExtras().getString("message"));
        b.putString("title", intent2.getExtras().getString("title"));
        b.putString("maid", intent2.getExtras().getString("maid"));
        b.putString("customer", intent2.getExtras().getString("customer"));
        b.putString("bookingTime", intent2.getExtras().getString("bookingTime"));
        b.putString("heading", intent2.getExtras().getString("heading"));
        if (intent2.getExtras().getString("isNotification") != null) {
            b.putString("isNotification", "");

            Log.e("Called", "Yes");
        }
        intent.putExtras(b);
        sendBroadcast(intent);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    public static class MyLoadView extends View {

        private final Paint mPaint;

        public MyLoadView(Context context) {
            super(context);
            mPaint = new Paint();
            mPaint.setTextSize(50);
            mPaint.setARGB(200, 200, 200, 200);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawText("test test test", 0, 100, mPaint);
        }

        @Override
        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
        }

        @Override
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}
