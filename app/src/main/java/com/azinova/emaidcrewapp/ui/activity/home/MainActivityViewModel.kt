package com.azinova.emaidcrewapp.ui.activity.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.model.response.bookings.ResponseBookings
import com.azinova.emaidcrewapp.model.response.bookings.ScheduleItem
import com.azinova.emaidcrewapp.model.response.notification.ResponseNotification
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import kotlinx.coroutines.launch

class MainActivityViewModel :ViewModel(){

    private val prefs = SharedPref

    private val _bookingsDetailsSyncStatus = MutableLiveData<ResponseBookings>()
    val bookingsDetailsSyncStatus: LiveData<ResponseBookings>
        get() = _bookingsDetailsSyncStatus

    suspend fun fetDatFromServer(context: Context, currentDate: String) {
        try {
            val response = ApiClient.getRetrofit().getBookings(
                prefs.user_id.toString(),
                prefs.user_type.toString(),
                currentDate,
                prefs.token.toString()
            )

            Log.d("Refresh", "getBookings: " + response)

            if (response.status.equals("success")) {
                insertToDb(context, response.schedule)
                _bookingsDetailsSyncStatus.value = response
            } else {
                _bookingsDetailsSyncStatus.value = response
            }
        } catch (e: Exception) {
            Log.e("throw", "fetDatFromServer: ")
            ResponseBookings(message = "Something went wrong", status = "error")
            _bookingsDetailsSyncStatus.value = ResponseBookings(message = "Something went wrong", status = "error")

        }

    }

    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }

    //fetch Notification...
    private val _notificationlistStatus = MutableLiveData<ResponseNotification>()
    val notificationlistStatus: LiveData<ResponseNotification>
        get() = _notificationlistStatus

    fun getNotificationList(userId: String, currentdate: String, userType: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getnotificationList(userId,userType,currentdate,token!!)
                if (response.status.equals("success")) {
                    _notificationlistStatus.value = response

                } else {
                    _notificationlistStatus.value = response
                }
            } catch (e: Exception) {
                _notificationlistStatus.value = ResponseNotification(message = "Something went wrong", status = "Error")

            }
        }
    }

}