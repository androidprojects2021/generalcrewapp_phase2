package com.azinova.emaidcrewapp.model.response.Signature

import com.google.gson.annotations.SerializedName

data class ResponseSignature(
        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("message")
        val message: String? = null
)