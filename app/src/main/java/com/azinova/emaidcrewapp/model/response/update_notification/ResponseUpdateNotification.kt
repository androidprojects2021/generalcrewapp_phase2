package com.azinova.emaidcrewapp.model.response.update_notification

import com.google.gson.annotations.SerializedName

data class ResponseUpdateNotification(

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)
