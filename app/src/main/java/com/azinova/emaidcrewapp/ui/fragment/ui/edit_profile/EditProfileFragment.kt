package com.azinova.emaidcrewapp.ui.fragment.ui.edit_profile

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import java.text.SimpleDateFormat
import java.util.*


class EditProfileFragment : Fragment() {
    private val prefs = SharedPref
    private lateinit var editProfileViewModel: EditProfileViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editProfileViewModel = ViewModelProvider(this).get(EditProfileViewModel::class.java)

        initview(view)
        observeIn()
        onClick()
    }

    private fun observeIn() {
        editProfileViewModel.editprofileStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer {editprofileStatus ->
            if (editprofileStatus.equals("success")) {
                Loader.hideLoader()
                Toast.makeText(requireContext(), editprofileStatus.message, Toast.LENGTH_SHORT).show()

            } else {
                Loader.hideLoader()
                Toast.makeText(requireContext(), editprofileStatus.message, Toast.LENGTH_SHORT).show()

            }
        })
    }

    private fun initview(view: View) {
        txt_editfullname.text = Editable.Factory.getInstance()
            .newEditable(prefs.user_name)
        txt_editphone.text = Editable.Factory.getInstance()
            .newEditable(prefs.phone)
        txt_editmail.text = Editable.Factory.getInstance()
            .newEditable(prefs.email)

    }

    private fun onClick() {

        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        btn_save.setOnClickListener {
/*
            if (txt_editfullname.text.toString().isEmpty()){
                Snackbar.make(outerprofile, "Please enter username", Snackbar.LENGTH_SHORT).show()
            }else if (txt_editphone.text.toString().isEmpty()){
                Snackbar.make(outerprofile, "Please enter phone number", Snackbar.LENGTH_SHORT).show()
            }else if (txt_editmail.text.toString().isEmpty()){
                Snackbar.make(outerprofile, "Please enter email", Snackbar.LENGTH_SHORT).show()
            }else{

            }*/

            Loader.showLoader(requireContext())
            if (requireContext().isConnectedToNetwork()) {
                setProfileEdit(txt_editfullname.text.toString(), txt_editphone.text.toString(), txt_editmail.text.toString(),currentDate)
            } else {
                Loader.hideLoader()
                Snackbar.make(outerprofile, "Please connect to internet", Snackbar.LENGTH_LONG).show()
            }

        }
    }

    private fun setProfileEdit(name: String, phone: String, email: String, currentDate: String) {
        editProfileViewModel.setProfileEdit(prefs.user_id,prefs.user_type,currentDate,prefs.token,name,phone,email)
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}