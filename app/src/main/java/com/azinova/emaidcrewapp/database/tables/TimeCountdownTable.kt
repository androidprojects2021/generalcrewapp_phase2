package com.azinova.emaidcrewapp.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "timecountdown_table")
data class TimeCountdownTable(

        @PrimaryKey(autoGenerate = true)
        var count_id: Int? = null,

        @ColumnInfo(name = "booking_id")
        var booking_id: String? = null,

        @ColumnInfo(name = "service_status")
        var service_status: String? = null,

        @ColumnInfo(name = "service_start_hour")
        var service_start_hour: String? = null,

        @ColumnInfo(name = "service_start_minute")
        var service_start_minute: String? = null,

        @ColumnInfo(name = "service_start_second")
        var service_start_second: String? = null

        )
