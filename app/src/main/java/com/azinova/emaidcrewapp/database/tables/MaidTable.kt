package com.azinova.emaidcrewapp.database.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "maid_table")
data class MaidTable(

        @PrimaryKey(autoGenerate = true)
        var m_id: Int? = null,

        @ColumnInfo(name = "maid_id")
        var maid_id: String? = null,

        @ColumnInfo(name = "customer_id")
        var customer_id: String? = null,

        @ColumnInfo(name = "booking_id")
        var booking_id: Int? = null,

        @ColumnInfo(name = "maid_name")
        var maid_name: String? = null,

        @ColumnInfo(name = "maid_attandence")
        var maid_attandence: String? = null,

        @ColumnInfo(name = "service_status")
        var service_status: Int? = null,

        @ColumnInfo(name = "maid_image")
        val maid_image: String? = null,

        @ColumnInfo(name = "service_fee")
        val maid_serviceFee: String? = null,

        @ColumnInfo(name = "total")
        val maid_total: String? = null,

        @ColumnInfo(name = "shift_start")
        var maid_shift_start: String? = null,

        @ColumnInfo(name = "shift_end")
        var maid_shift_end: String? = null,

        @ColumnInfo(name = "schedule_date")
        var schedule_date: String? = null,

        @ColumnInfo(name = "customer_address")
        val customerAddress: String? = null

)
