package com.azinova.emaidcrewapp.model.response.cancel

import com.google.gson.annotations.SerializedName

data class ResponseCancelService(

	@field:SerializedName("service_status")
	val serviceStatus: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null



)
