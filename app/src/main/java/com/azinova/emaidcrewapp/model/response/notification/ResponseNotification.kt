package com.azinova.emaidcrewapp.model.response.notification

import com.google.gson.annotations.SerializedName

data class ResponseNotification(

	@field:SerializedName("notification_details")
	val notificationDetails: List<NotificationDetailsItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class NotificationDetailsItem(

	@field:SerializedName("notification_list")
	val notificationList: List<NotificationListItem?>? = null,

	@field:SerializedName("Date")
	val date: String? = null
)

data class NotificationListItem(

	@field:SerializedName("date")
	val n_date: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("read_status")
	val read_status: String? = null,

	@field:SerializedName("pushid")
	val pushid: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("customer")
	val customer: String? = null,

	@field:SerializedName("maid")
	val maid: String? = null,

	@field:SerializedName("booking_time")
	val booking_time: String? = null,

	@field:SerializedName("maid_read_status")
	val maid_read_status: String? = null
)


