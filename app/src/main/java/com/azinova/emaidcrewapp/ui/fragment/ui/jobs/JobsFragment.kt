package com.azinova.emaidcrewapp.ui.fragment.ui.jobs

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.model.response.SampleBookingResponse
import com.azinova.emaidcrewapp.model.response.SampleMaidDetails
import com.azinova.emaidcrewapp.model.response.SampleSchduleResponse
import com.azinova.emaidcrewapp.model.response.schedules.SceduleTimeingResponse
import com.azinova.emaidcrewapp.model.response.schedules.ScheduleResponse
import com.azinova.emaidcrewapp.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_jobs.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.layout_cancel_reasons.*
import android.provider.ContactsContract
import android.widget.*
import com.azinova.emaidcrewapp.R


class JobsFragment : Fragment() {

    private lateinit var jobsViewModel: JobsViewModel

    private lateinit var parent: MainActivity
    var database: EmaidDatabase? = null

    private val prefs = SharedPref
    private var requireContext: Context? = null

    var newbookinglist: MutableList<SceduleTimeingResponse> = ArrayList()

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var l_nobookingsList: LinearLayout

    private lateinit var booking_lm: LinearLayoutManager


    private lateinit var rv_bookingdates: RecyclerView
    private lateinit var rv_bookingtimeing: RecyclerView
    lateinit var dateBookingsAdapter: DateBookingsAdapter
    var timeBookingsAdapter: TimeBookingsAdapter? = null

    var FilterPosition: Int = -1
    val filter: ArrayList<Int> = ArrayList()

    var CheckPosition = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        jobsViewModel = ViewModelProvider(this).get(JobsViewModel::class.java)

        //turnOnStrictMode()

        parent = activity as MainActivity
        Log.d("txt_zone", "onViewCreated: " + prefs.user_type)
        if (!(prefs.user_zone.isNullOrEmpty())) {
            when (prefs.user_type) {
                "0" -> parent.txt_zone.text = ""
                "1" -> parent.txt_zone.text = prefs.user_zone
                else -> parent.txt_zone.text = ""
            }

        }

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        l_nobookingsList = view.findViewById(R.id.l_nobookingsList)

        rv_bookingdates = view.findViewById(R.id.rv_bookingdates)
        rv_bookingtimeing = view.findViewById(R.id.rv_bookingtimeing)

        booking_lm = LinearLayoutManager(requireContext())
        booking_lm.orientation = LinearLayoutManager.VERTICAL
        rv_bookingtimeing.setLayoutManager(booking_lm)
        rv_bookingdates.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)


        filter.clear()
        filter.add(-1)
        CheckPosition = 0


        //Multi Selection Using filter array
        callDateData(FilterPosition, filter)
        observe()
        clicks()


    }


    override fun onResume() {
        super.onResume()
        try {


            CheckPosition = 0

            //Multi Selection Using filter array
            callDateData(FilterPosition, filter)
            Log.d("TAG", "Position: 2 - " + CheckPosition)
        } catch (e: java.lang.Exception) {
        }
    }

    private fun callDateData(FilterPosition: Int, filter: ArrayList<Int>) {


        Log.d("Zone", "callDateData: " + prefs.user_zone)
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        //val sdf = "25-06-2021"
        var currentdate = sdf.format(Date())

        // DatesFromDb
        GlobalScope.launch(Dispatchers.Main) {
            try {


                val dateArray = jobsViewModel.getBookingDates(requireContext())
                if (dateArray != null) {
                    if (dateArray.isEmpty()) {

                        rv_bookingtimeing.visibility = View.GONE
                        l_nobookingsList.visibility = View.VISIBLE

                    } else {


                        Log.d("TAG", "LIST 2" + dateArray)

                        dateArray?.let { date ->

                            //current date added to 0 index.
                            val datesArrays: ArrayList<String> = ArrayList()
                            if (date.contains(currentdate)) {
                                datesArrays.addAll(date)
                                datesArrays.remove(currentdate)
                                datesArrays.add(0, currentdate)
                            }
                            Log.d("TAG", "LIST 3" + datesArrays)

                            when (prefs.show_other_dates) {
                                "yes" -> {
                                    dateBookingsAdapter =
                                        DateBookingsAdapter(requireContext(), datesArrays) {
                                            if (it.isNotEmpty()) {
                                                //Multi Selection Using filter array
                                                callBookingDate(it, FilterPosition, filter)
                                            }
                                        }

                                }
                                "no" -> {
                                    val currentDate = date.filter { it.equals(currentdate) }
                                    dateBookingsAdapter =
                                        DateBookingsAdapter(requireContext(), currentDate) {
                                            if (it.isNotEmpty()) {
                                                //Multi Selection Using filter array
                                                callBookingDate(it, FilterPosition, filter)
                                            }
                                        }
                                }
                            }

                            rv_bookingdates.adapter = dateBookingsAdapter


                            when (CheckPosition) {
                                0 -> {
                                    val datelist: List<String> = date
                                    val selected = datelist.filter { it.equals(currentdate) }
                                    Log.e("TAG", "callDateData: " + selected)

                                    if (selected.isEmpty()) {

                                        //newChange
                                        insertCurrentDate(currentdate)


                                    }
                                }
                            }
                        }

                    }
                }

            } catch (e: Exception) {
                Log.e("callDateData", "callDateData: ${e.message}")
                if (isAdded) {
                    errorMessage()
                }
            }

        }
    }


    //NEW CHANGE
    private fun errorMessage() {
        clearLogin()
        parent.stopServiceTracker()
        GlobalScope.launch {
            BookingsRepository.database?.clearAllTables() //New Change
        }
        val loginIntent = Intent(requireActivity(), LoginActivity::class.java)
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(loginIntent)
        activity?.finish()
    }

    private fun insertCurrentDate(currentDate: String) {
        GlobalScope.launch {
            jobsViewModel.insertDate(currentDate)
        }
    }

    private fun callBookingDate(date: String, filterPosition: Int, filter: ArrayList<Int>) {

        GlobalScope.launch(Dispatchers.Main) {

            //Multi Selection Using filter array
            val bookingTimeList =
                jobsViewModel.getBookingTimeings(
                    requireActivity(),
                    date,
                    filterPosition,
                    filter
                )//NEW CHANGE

            Log.e("BOOKINGDATE", "callBookingDate: Working")


            bookingTimeList?.let {
                newbookinglist = ArrayList()
                newbookinglist.clear()
                newbookinglist = it

                when {
                    newbookinglist.size == 0 -> {

                        rv_bookingtimeing.visibility = View.GONE
                        l_nobookingsList.visibility = View.VISIBLE
                        Snackbar.make(outerjobs, "No bookings Found", Snackbar.LENGTH_SHORT)
                            .show()


                    }
                    newbookinglist.get(0).schedulebookings?.get(0)?.booking_id == null -> {
                        rv_bookingtimeing.visibility = View.GONE
                        l_nobookingsList.visibility = View.VISIBLE
                        Snackbar.make(outerjobs, "No bookings Found", Snackbar.LENGTH_SHORT)
                            .show()

                    }
                    else -> {
                        if (isAdded) { //New Change
                            Log.d("Status Based  .......", "callBookingDate: " + newbookinglist)

                            l_nobookingsList.visibility = View.GONE
                            rv_bookingtimeing.visibility = View.VISIBLE

                            timeBookingsAdapter = TimeBookingsAdapter(
                                requireActivity(),
                                newbookinglist
                            ) { scheduleResponse: ScheduleResponse?, i: Int, list: List<ScheduleResponse?>? ->
                                Log.d("Data", "callBookingDate: " + scheduleResponse)
                                val intent =
                                    Intent(requireContext(), BookingDetailsActivity::class.java)
                                intent.putExtra(
                                    "Customer_Id",
                                    scheduleResponse?.customer_id.toString()
                                )
                                intent.putExtra(
                                    "Booking_id",
                                    scheduleResponse?.booking_id.toString()
                                )
                                intent.putExtra(
                                    "Schedule_date",
                                    scheduleResponse?.schedule_date.toString()
                                )
                                intent.putExtra(
                                    "Service_status",
                                    scheduleResponse?.service_status.toString()
                                )
                                intent.putExtra(
                                    "Shift_start_time",
                                    scheduleResponse?.shift_start
                                )
                                intent.putExtra(
                                    "Shift_end_time",
                                    scheduleResponse?.shift_end
                                )
                                intent.putExtra(
                                    "Customer_address",
                                    scheduleResponse?.customerAddress
                                )

                                startActivity(intent)


                            }


                            rv_bookingtimeing.adapter = timeBookingsAdapter

                        }
                    }
                }
            }
        }
    }


    private fun clicks() {

        swipeRefreshLayout.setOnRefreshListener {
            if (requireContext().isConnectedToNetwork()) {

                fetDatFromServer(requireContext())
            } else {

                Snackbar.make(outerjobs, "Please connect to internet", Snackbar.LENGTH_LONG).show()
            }

        }

        parent.img_filter.setOnClickListener {

            val builder = AlertDialog.Builder(requireContext())
            val view: View =
                LayoutInflater.from(requireContext())
                    .inflate(R.layout.dialog_filter, null)
            builder.setView(view)
            val alertDialog = builder.create()

            val checkbox_all = view.findViewById<CheckBox>(R.id.checkbox_condition_all)
            val checkbox_pending = view.findViewById<CheckBox>(R.id.checkbox_conditions_pending)
            val checkbox_Ongoing = view.findViewById<CheckBox>(R.id.checkbox_conditions_Ongoing)
            val checkbox_Finished = view.findViewById<CheckBox>(R.id.checkbox_conditions_Finished)
            val checkbox_Cancelled = view.findViewById<CheckBox>(R.id.checkbox_conditions_Cancelled)

            CheckPosition = 1

            when {
                filter.contains(0) && filter.contains(1) && filter.contains(2) && filter.contains(3) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Ongoing.isChecked = true
                    checkbox_Finished.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(1) && filter.contains(2) && filter.contains(3) -> {
                    checkbox_Ongoing.isChecked = true
                    checkbox_Finished.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(0) && filter.contains(1) && filter.contains(2) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Ongoing.isChecked = true
                    checkbox_Finished.isChecked = true
                }
                filter.contains(0) && filter.contains(2) && filter.contains(3) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Finished.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(0) && filter.contains(1) && filter.contains(3) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Ongoing.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(0) && filter.contains(1) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Ongoing.isChecked = true
                }
                filter.contains(2) && filter.contains(3) -> {
                    checkbox_Finished.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(0) && filter.contains(2) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Finished.isChecked = true
                }
                filter.contains(1) && filter.contains(3) -> {
                    checkbox_Ongoing.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(0) && filter.contains(3) -> {
                    checkbox_pending.isChecked = true
                    checkbox_Cancelled.isChecked = true
                }
                filter.contains(1) && filter.contains(2) -> {
                    checkbox_Ongoing.isChecked = true
                    checkbox_Finished.isChecked = true
                }
                filter.contains(-1) -> {
                    checkbox_all.isChecked = true
                }
                filter.contains(0) -> {
                    checkbox_pending.isChecked = true
                }
                filter.contains(1) -> {
                    checkbox_Ongoing.isChecked = true
                }
                filter.contains(2) -> {
                    checkbox_Finished.isChecked = true
                }
                filter.contains(3) -> {
                    checkbox_Cancelled.isChecked = true
                }
            }

            checkbox_all.setOnClickListener {
                filter.clear()
                filter.add(-1)
                //FilterPosition = -1
                checkbox_all.isChecked = true
                checkbox_pending.isChecked = false
                checkbox_Ongoing.isChecked = false
                checkbox_Finished.isChecked = false
                checkbox_Cancelled.isChecked = false
                //Multi Selection Using filter array
                callDateData(FilterPosition, filter)

                Log.e("FILTER", "all: $filter")
            }
            checkbox_pending.setOnClickListener {
                if (filter.contains(-1)) {
                    filter.remove(-1)
                }
                checkbox_all.isChecked = false
                checkbox_pending.isChecked = true
                if (filter.contains(0)) {
                    filter.remove(0)
                    checkbox_pending.isChecked = false
                } else {
                    filter.add(0)
                }
                Log.e("FILTER", "pending: $filter")
                //Multi Selection Using filter array
                callDateData(FilterPosition, filter)

            }
            checkbox_Ongoing.setOnClickListener {
                if (filter.contains(-1)) {
                    filter.remove(-1)
                }
                checkbox_all.isChecked = false
                checkbox_Ongoing.isChecked = true
                if (filter.contains(1)) {
                    filter.remove(1)
                    checkbox_Ongoing.isChecked = false
                } else {
                    filter.add(1)
                }
                Log.e("FILTER", "ongoing: $filter")
                //Multi Selection Using filter array
                callDateData(FilterPosition, filter)

            }
            checkbox_Finished.setOnClickListener {
                if (filter.contains(-1)) {
                    filter.remove(-1)
                }
                checkbox_all.isChecked = false
                checkbox_Finished.isChecked = true
                if (filter.contains(2)) {
                    filter.remove(2)
                    checkbox_Finished.isChecked = false
                } else {
                    filter.add(2)
                }
                Log.e("FILTER", "finished: $filter")
                //Multi Selection Using filter array
                callDateData(FilterPosition, filter)


            }
            checkbox_Cancelled.setOnClickListener {
                if (filter.contains(-1)) {
                    filter.remove(-1)
                }
                checkbox_all.isChecked = false
                checkbox_Cancelled.isChecked = true
                if (filter.contains(3)) {
                    filter.remove(3)
                    checkbox_Cancelled.isChecked = false
                } else {
                    filter.add(3)
                }
                Log.e("FILTER", "cancelled: $filter")
                //Multi Selection Using filter array
                callDateData(FilterPosition, filter)

            }

            alertDialog.show()
        }
    }

    private fun fetDatFromServer(context: Context) {

        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        GlobalScope.launch(Dispatchers.Main) {
            jobsViewModel.fetDatFromServer(context, currentDate)

            CheckPosition = 1
            //Multi Selection Using filter array
            callDateData(FilterPosition, filter)
        }
    }

    private fun observe() {
        jobsViewModel.bookingsDetailsRefresh.observe(
            viewLifecycleOwner,
            Observer { bookingsDetailsRefresh ->

                if (bookingsDetailsRefresh.status.equals("success")) {
                    swipeRefreshLayout.isRefreshing = false
                    CheckPosition = 1

                    //Multi Selection Using filter array
                    callDateData(FilterPosition, filter)
                    Toast.makeText(
                        requireContext(),
                        "Refreshed",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (bookingsDetailsRefresh.status.equals("token_error")) {
                    swipeRefreshLayout.isRefreshing = false
                    Toast.makeText(
                        requireContext(),
                        bookingsDetailsRefresh.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    swipeRefreshLayout.isRefreshing = false
                    Toast.makeText(
                        requireContext(),
                        bookingsDetailsRefresh.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            })
    }

    private fun clearLogin() {
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""
        // prefs.company_name = ""
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireContext = context
    }

    override fun onDetach() {
        super.onDetach()
        requireContext = null
    }

    override fun onDestroy() {
        super.onDestroy()
    }


}