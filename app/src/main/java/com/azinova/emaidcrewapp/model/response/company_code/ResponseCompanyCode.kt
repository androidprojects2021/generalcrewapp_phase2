package com.azinova.emaidcrewapp.model.response.company_code

import com.google.gson.annotations.SerializedName

data class ResponseCompanyCode(
        @field:SerializedName("message")
        val message: String? = null,

        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("url")
        val url: String? = null,

        @field:SerializedName("is_testing")
        val is_testing: String? = null,

        @field:SerializedName("is_driver_based")
        val is_driver_based: String? = null,

        @field:SerializedName("is_hide")
        val is_hide: String? = null,

        @field:SerializedName("show_other_dates")
        val show_other_dates: String? = null,

        @field:SerializedName("is_maid_hide")
        val is_maid_hide: String? = null,

        @field:SerializedName("is_payment_hide")
        val is_payment_hide: String? = null,

        @field:SerializedName( "company_name")
        val company_name: String? = null
)