package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.database.tables.BookingsTable
import com.azinova.emaidcrewapp.database.tables.MaidTable
import com.azinova.emaidcrewapp.model.response.Invoice.DataDetails
import com.azinova.emaidcrewapp.model.response.maiddetails.MaidDetails
import com.azinova.emaidcrewapp.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaidcrewapp.model.response.transfer.TransferListItem
import com.azinova.emaidcrewapp.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.adapter.MaidOnBoardAdapter
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.adapter.TimeScheduleAdapter
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.adapter.TransferAdapter
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.DetailInvoiceFragment
import com.azinova.emaidcrewapp.ui.fragment.ui.jobs.JobsViewModel
import com.azinova.emaidcrewapp.utils.SharedPref
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_booking_details.*
import kotlinx.android.synthetic.main.fragment_booking_details.*
import kotlinx.android.synthetic.main.fragment_jobs.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class BookingDetailsFragment : Fragment() {
    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    var count: Int? = 0

    private lateinit var parent: BookingDetailsActivity

    private lateinit var bookingssViewModel: BookingDetailsViewModel
    private lateinit var jobsViewModel: JobsViewModel //new change
    lateinit var maidOnBoardAdapter: MaidOnBoardAdapter
    lateinit var timeScheduleAdapter: TimeScheduleAdapter
    lateinit var transferAdapter: TransferAdapter

    private lateinit var rv_maidlistdetails: RecyclerView
    private lateinit var rvTimeSchedule: RecyclerView
    private lateinit var rv_transferSchedule: RecyclerView
    private lateinit var txt_title: TextView


    lateinit var booking_id: String
    var customer_id: String? = null
    lateinit var shift_starttime: String
    lateinit var shift_endtime: String
    lateinit var booking_date: String
    lateinit var customer_address: String
    var service_status: String? = "-1"

    var selected_driver: String = ""
    var selected_zone: String = ""

    val REQ_CODE_CALL = 100

    var schedulesFromDb: ArrayList<BookingsTable>? = ArrayList()

    var transferlist: List<TransferListItem?>? = listOf()

    var maidarraylist: ArrayList<MaidTable>? = null

    var customerDetailsFromDb: List<BookingsTable>? = listOf()

    private var startButtonClicked = false
    private var stopButtonClicked = false
    private var transferButtonClicked = false

    var starttime: String = " "

    var maidActive = 0

    lateinit var details: DataDetails

    private var key_status = false

    var booking_list: ArrayList<String>? = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_booking_details, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bookingssViewModel = ViewModelProvider(this).get(BookingDetailsViewModel::class.java)
        jobsViewModel = ViewModelProvider(this).get(JobsViewModel::class.java)//new change
        parent = activity as BookingDetailsActivity


        bookingssViewModel.reset()


        booking_date = arguments?.getString("Schedule_date").toString()

        booking_id = arguments?.getString("Booking_id").toString()
        txt_bookingidvalue.text = booking_id

        service_status = arguments?.getString("Service_status")

        customer_id = arguments?.getString("Customer_Id").toString()

        shift_starttime = arguments?.getString("Shift_start_time").toString()

        shift_endtime = arguments?.getString("Shift_end_time").toString()

        customer_address = arguments?.getString("Customer_address").toString()




        rv_maidlistdetails = view.findViewById(R.id.rv_maidlistdetails)

        when (prefs.user_type) {
            "0" -> btn_transfer.visibility = View.GONE
            "1" -> btn_transfer.visibility = View.VISIBLE
        }
        if (prefs.is_maid_hide.equals("yes") && prefs.user_type.equals("0")) {
            btn_start.visibility = View.INVISIBLE
            btn_stop.visibility = View.INVISIBLE

            btn_invoice.visibility = View.GONE
        } else if (prefs.is_hide.equals("yes") && prefs.user_type.equals("1")) {
            btn_start.visibility = View.INVISIBLE
            btn_stop.visibility = View.INVISIBLE

            btn_invoice.visibility = View.GONE
        } else if (prefs.is_hide.equals("no") && prefs.user_type.equals("1")) {
            btn_start.visibility = View.VISIBLE
            btn_stop.visibility = View.VISIBLE

        } else {
            btn_start.visibility = View.VISIBLE
            btn_stop.visibility = View.VISIBLE

        }



        onCustomerViewset()
        onCustomerDetails()

        observeList()
        onClick()

    }


    private fun onCustomerViewset() {

        try {


            //MAID
            GlobalScope.launch(Dispatchers.Main) {
                if (!customer_id.isNullOrEmpty()) {

                    try {
                        bookingssViewModel.MailListFromDB(
                            requireContext(), customer_id!!,
                            shift_starttime,
                            shift_endtime,
                            booking_date,
                            customer_address
                        )
                            ?.observe(viewLifecycleOwner, Observer { maidlist ->

                                if (maidlist != null) {
                                    if (maidlist?.size != 0) {
                                        maidActive = 0
                                        for (item in maidlist) {
                                            when (item.maid_attandence) {
                                                "1" -> maidActive = 1
                                            }

                                        }

                                        maidarraylist?.clear()
                                        maidarraylist = maidlist as ArrayList<MaidTable>

                                        if (maidarraylist!!.isNotEmpty()) {
                                            Loader.hideLoader()
                                            maidViewSet(maidarraylist!!)
                                            onCustomerDetails()
                                        } else {
                                            Loader.hideLoader()
                                        }


                                    }

                                }


                            })


                    } catch (e: Exception) {
                        Log.d(
                            "Exception",
                            "onCustomerViewset: " + e.localizedMessage + " , " + e.message
                        )
                    }
                }
            }


        } catch (e: Exception) {
            Log.d(
                "Main Exception",
                "onCustomerViewset: " + e.localizedMessage + " , " + e.message
            )
        }
    }

    private fun onCustomerDetails() {
        // CUSTOMER
        GlobalScope.launch(Dispatchers.Main) {
            try {

                val bookingdetailsArray =
                    bookingssViewModel.NewCustomerDetailsFromDB(requireContext(), booking_id)

                if (bookingdetailsArray != null) {
                    if (bookingdetailsArray.size != 0) {
                        bookingdetailsArray.let { detailslist ->

                            customerDetailsFromDb = ArrayList()
                            (customerDetailsFromDb as ArrayList<BookingsTable>).clear()
                            customerDetailsFromDb = detailslist

                            if (customerDetailsFromDb!!.isNotEmpty()) {
                                Loader.hideLoader()
                                detailsViewSet(customerDetailsFromDb!!)
                            } else {
                                Loader.hideLoader()
                            }


                        }
                    }
                }

                Loader.hideLoader()


            } catch (e: Exception) {
            }
        }
    }

    private fun detailsViewSet(customerdetails: List<BookingsTable>) {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val currentdate = sdf.format(Date())


        customer_id = customerdetails[0].customer_id.toString()

        when (customerdetails[0].service_status) {
            0 -> {
                service_status = "0"

                if (maidActive == 1) {
                    startButtonClicked = false
                    btn_start.setBackgroundResource(R.drawable.bg_green)
                } else if (maidActive == 0) {
                    startButtonClicked = true
                    btn_start.setBackgroundResource(R.drawable.bg_button_off)
                }

                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = false
                btn_transfer.setBackgroundResource(R.drawable.bg_purple)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
                btn_invoice.visibility = View.GONE
                parent.img_option.visibility = View.VISIBLE
            }
            1 -> {
                service_status = "1"
                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = false
                btn_stop.setBackgroundResource(R.drawable.bg_red)
                transferButtonClicked = false
                btn_transfer.setBackgroundResource(R.drawable.bg_purple)

                var spf = SimpleDateFormat("hh:mm:ss a")
                val new = spf.parse(customerdetails[0].starttime)
                spf = SimpleDateFormat("hh:mm a")
                val starttime: String = spf.format(new)

                if (prefs.is_maid_hide.equals("yes") && prefs.user_type.equals("0")) {
                    txt_stoptime.visibility = View.GONE
                    txt_starttime.visibility = View.GONE
                } else if (prefs.is_hide.equals("yes") && prefs.user_type.equals("1")) {
                    txt_stoptime.visibility = View.GONE
                    txt_starttime.visibility = View.GONE
                } else if (prefs.is_hide.equals("no") && prefs.user_type.equals("1")) {

                    tv_start_time.visibility = View.VISIBLE
                    tv_start_time.text = starttime

                } else {

                    tv_start_time.visibility = View.VISIBLE
                    tv_start_time.text = starttime
                }

                parent.img_option.visibility = View.GONE
                btn_invoice.visibility = View.GONE
            }
            2 -> {
                service_status = "2"

                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                var sspf = SimpleDateFormat("hh:mm:ss a")
                val snew = sspf.parse(customerdetails[0].starttime)
                sspf = SimpleDateFormat("hh:mm a")
                val starttime: String = sspf.format(snew)

                var spf = SimpleDateFormat("hh:mm:ss a")
                val new = spf.parse(customerdetails[0].endtime)
                spf = SimpleDateFormat("hh:mm a")
                val endtime: String = spf.format(new)

                if (prefs.is_maid_hide.equals("yes") && prefs.user_type.equals("0")) {
                    txt_starttime.visibility = View.GONE
                    txt_stoptime.visibility = View.GONE
                    btn_invoice.visibility = View.GONE
                } else if (prefs.is_hide.equals("yes") && prefs.user_type.equals("1")) {
                    txt_starttime.visibility = View.GONE
                    txt_stoptime.visibility = View.GONE
                    btn_invoice.visibility = View.GONE
                } else if (prefs.is_hide.equals("no") && prefs.user_type.equals("1")) {

                    tv_stop_time.visibility = View.VISIBLE
                    tv_stop_time.text = endtime
                    tv_start_time.visibility = View.VISIBLE
                    tv_start_time.text = starttime

                } else {

                    tv_stop_time.visibility = View.VISIBLE
                    tv_stop_time.text = endtime
                    tv_start_time.visibility = View.VISIBLE
                    tv_start_time.text = starttime

                }

                parent.img_option.visibility = View.VISIBLE

            }
            3 -> {
                service_status = "3"

                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
                btn_invoice.visibility = View.GONE
                parent.img_option.visibility = View.GONE
            }
            else -> {
                startButtonClicked = true
                btn_start.setBackgroundResource(R.drawable.bg_button_off)
                stopButtonClicked = true
                btn_stop.setBackgroundResource(R.drawable.bg_button_off)
                transferButtonClicked = true
                btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

                txt_starttime.visibility = View.GONE
                txt_stoptime.visibility = View.GONE
                btn_invoice.visibility = View.GONE
                parent.img_option.visibility = View.GONE
            }
        }

        //---TRANSFER STATUS
        when (customerdetails[0].istransfer) {
            0 -> img_transfer_detail.visibility = View.INVISIBLE//= "Not "
            1 -> img_transfer_detail.visibility = View.VISIBLE// = "yes"
            else -> img_transfer_detail.visibility = View.INVISIBLE
        }

        if (SharedPref.company_name == "Urban Cleaning") {
            txt_username.visibility = View.VISIBLE
        } else {
            txt_username.visibility = View.GONE
        }

        txt_customername.text = customerdetails[0].customer_name
        txt_username.text = customerdetails[0].username
        txt_customertype.text = customerdetails[0].customerType
        txt_area.text = customerdetails[0].area
        txt_zone.text = customerdetails[0].zone
        txt_customeraddress.text = customerdetails[0].customerAddress
        txt_notes.text = customerdetails[0].customernotes
        txt_extranotes.text = customerdetails[0].bookingNote


        val _24HourTimeshiftStart = customerdetails[0].shift_start
        val _24HourSDFS = SimpleDateFormat("HH:mm")
        val _12HourSDFS = SimpleDateFormat("hh:mm a")
        val shiftStart = _24HourSDFS.parse(_24HourTimeshiftStart)

        val _24HourTimeshiftEnd = customerdetails[0].shift_end
        val _24HourSDF = SimpleDateFormat("HH:mm")
        val _12HourSDF = SimpleDateFormat("hh:mm a")
        val shiftEnd = _24HourSDF.parse(_24HourTimeshiftEnd)

        txt_servicetime.text =
            _12HourSDFS.format(shiftStart) + " - " + _12HourSDF.format(shiftEnd)
        txt_housecleaning.text = customerdetails[0].servicetype


        //cleaning material
        if (customerdetails[0].cleaning_material.toString() == "no") {
            img_materialtool.visibility = View.GONE //= "No"
        } else if (customerdetails[0].cleaning_material.toString() == "yes") {
            img_materialtool.visibility = View.VISIBLE  // = "Yes"
        }

        //vaccum material : vaccum_cleaner : 0 means disabled,  1 means enabled
        if (customerdetails[0].vaccum_cleaner.toString() == "0") {
            img_vaccumtool.visibility = View.GONE  // = "No"
        } else if (customerdetails[0].vaccum_cleaner.toString() == "1") {
            img_vaccumtool.visibility = View.VISIBLE  //= "Yes"
        }
        //key
        if (customerdetails[0].keyStatus.toString() == "0") {
            key_status = false  // = "No"
            img_key.visibility = View.GONE
        } else if (customerdetails[0].keyStatus?.toString() == "1") {
            key_status = true    // = "Yes"
            img_key.visibility = View.VISIBLE
        }


        txt_extra.text = customerdetails[0].extraService
        //booking_type : OD - one day booking, WE - weekly booking, BI - bi weekly booking
        when (customerdetails[0].booking_type) {
            "OD" -> txt_bookingtype.text = "One Day Booking"
            "WE" -> txt_bookingtype.text = "Weekly Booking"
            "BI" -> txt_bookingtype.text = "Biweekly Booking"
            else -> txt_bookingtype.text = ""
        }


        //---PAYMENT STATUS
        when (customerdetails[0].paymentStatus) {
            0 -> img_paystatus_detail.setImageResource(R.drawable.new_notpaid) //= "Not Paid"
            1 -> img_paystatus_detail.setImageResource(R.drawable.new_paid)// = "Paid"
            2 -> img_paystatus_detail.setImageResource(R.drawable.new_online) // = "Online"
            else -> img_paystatus_detail.visibility = View.INVISIBLE
        }



        if (prefs.is_payment_hide.equals("yes")) {
            txt_serviceamount_value.text = "AED " + "0.00"
            txt_paidamount_value.text = "AED " + "0.00"
            txt_balance_value.text = "AED " + "0.00"
        } else {
            txt_serviceamount_value.text = "AED " + customerdetails[0].serviceFee
            txt_paidamount_value.text =
                "AED " + customerdetails[0].paidAmount.toString()
            txt_balance_value.text = "AED " + customerdetails[0].balance.toString()
        }



        schedulesFromDb = customerdetails as ArrayList<BookingsTable>?

        if (currentdate.toString() != customerDetailsFromDb?.get(0)?.schedule_date) {

            Log.e("Booking date", "onViewSet: date " + "True")

            startButtonClicked = true
            btn_start.setBackgroundResource(R.drawable.bg_button_off)
            stopButtonClicked = true
            btn_stop.setBackgroundResource(R.drawable.bg_button_off)
            transferButtonClicked = true
            btn_transfer.setBackgroundResource(R.drawable.bg_button_off)

        }

    }

    private fun maidViewSet(maidlist: List<MaidTable>) {
        // maidActive = 0
        /* for (item in maidlist) {
             when (item.maid_attandence) {
                 "1" -> maidActive = 1
             }

         }*/


        maidOnBoardAdapter = MaidOnBoardAdapter(
            requireContext(),
            maidlist
        ) { maidtable: MaidTable?, i: Int, list: List<MaidTable?>? ->
            MaidClick(maidtable)

        }
        val lm = LinearLayoutManager(requireContext())
        lm.orientation = LinearLayoutManager.HORIZONTAL
        rv_maidlistdetails.setLayoutManager(lm)
        rv_maidlistdetails.setAdapter(maidOnBoardAdapter)
    }


    private fun observeList() {


        bookingssViewModel.TransferlistStatus.observe(
            viewLifecycleOwner,
            Observer { TransferlistStatus ->
                if (TransferlistStatus != null) {
                    if (TransferlistStatus?.status.equals("success")) {
                        Loader.hideLoader()

                        if (TransferlistStatus?.transferList?.size != 0) {
                            transferlist = TransferlistStatus?.transferList
                            transferdailog()
                        } else {
                            Snackbar.make(
                                outerdetails,
                                "Transfer List is empty",
                                Snackbar.LENGTH_LONG
                            )
                                .show()
                        }
                    } else if (TransferlistStatus.status.equals("token_error")) {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            TransferlistStatus.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()


                        clearLogin()
                        database?.clearAllTables()

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        activity?.finish()

                    } else {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            TransferlistStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })

        bookingssViewModel.DriverTransferlistStatus.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if (it != null) {
                if (it.status.equals("success")) {
                    if (it.transferList?.size != 0) {
                        transferlist = it.transferList
                        driverTransferDialog()
                    } else {
                        Snackbar.make(
                            outerdetails,
                            "Transfer List is empty",
                            Snackbar.LENGTH_LONG
                        )
                            .show()
                    }

                } else if (it.status.equals("token_error")) {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    clearLogin()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()
                } else {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })


        bookingssViewModel.transferStatus.observe(viewLifecycleOwner, Observer { transferStatus ->
            if (transferStatus != null) {

                if (transferStatus?.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus?.message, Toast.LENGTH_SHORT)
                        .show()
                    when (transferStatus.transferAll) {
                        "0" -> {
                            callCurrentDate()
                            activity?.onBackPressed()
                        }
                        "1" -> {
                            Loader.showLoader(requireContext())
                            fetDatFromServer(requireContext())
                        }
                    }

                } else if (transferStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus.message, Toast.LENGTH_SHORT)
                        .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), transferStatus?.message, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })

        bookingssViewModel.startServiceStatus.observe(
            viewLifecycleOwner,
            Observer { startServiceStatus ->
                if (startServiceStatus != null) {

                    if (startServiceStatus?.status.equals("success")) {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            startServiceStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()

                        onResume()
                    } else if (startServiceStatus.status.equals("token_error")) {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            startServiceStatus.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()


                        clearLogin()
                        parent.stopServiceTracker()
                        database?.clearAllTables()

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        activity?.finish()

                    } else {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            startServiceStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })


        bookingssViewModel.maidBookingsStatus.observe(
            viewLifecycleOwner,
            Observer { maidBookingsStatus ->
                if (maidBookingsStatus != null) {

                    if (maidBookingsStatus?.status.equals("success")) {
                        Loader.hideLoader()

                        if (maidBookingsStatus != null) {

                            maidDetailsDialog(maidBookingsStatus)
                        }

                    } else if (maidBookingsStatus.status.equals("token_error")) {
                        Loader.hideLoader()


                        clearLogin()
                        parent.stopServiceTracker()
                        database?.clearAllTables()

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)

                        Toast.makeText(
                            requireContext(),
                            maidBookingsStatus.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()

                    } else {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            maidBookingsStatus?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })

        bookingssViewModel.maidStatusChange.observe(
            viewLifecycleOwner,
            Observer { maidStatusChange ->
                if (maidStatusChange != null) {

                    if (maidStatusChange?.status.equals("success")) {
                        Loader.hideLoader()
                        when (maidStatusChange.maidDetails?.maidAttandence) {
                            "1" -> maidActive = 1
                            "0" -> maidActive = 0
                            "2" -> maidActive = 0
                            else -> maidActive = 0
                        }

                        onCustomerViewset()


                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            maidStatusChange?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()

                    } else if (maidStatusChange.status.equals("token_error")) {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            maidStatusChange.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()


                        clearLogin()
                        parent.stopServiceTracker()
                        database?.clearAllTables()

                        val intent = Intent(requireContext(), LoginActivity::class.java)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        activity?.finish()

                    } else {
                        Loader.hideLoader()
                        Toast.makeText(
                            requireContext(),
                            maidStatusChange?.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            })


        bookingssViewModel.detailInvoice.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if (it != null) {
                if (it.status.equals("success")) {

                    if (it.details != null) {
                        details = it.details
                        val bundle = Bundle()

                        DetailInvoiceFragment.pdfUrl = ""
                        bundle.putSerializable("details", details)
                        findNavController().navigate(R.id.detailInvoiceFragment, bundle)
                    }


                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                } else if (it.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                        .show()


                    clearLogin()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })



        bookingssViewModel.keyStatusChange.observe(viewLifecycleOwner, Observer { it ->
            Loader.hideLoader()
            if (it?.status.equals("success")) {

                when (it?.key_statuss) {
                    "0" -> key_status = false
                    "1" -> key_status = true
                }
                onResume()

                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT).show()
            } else if (it?.status.equals("token_error")) {
                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT)
                    .show()

                clearLogin()
                database?.clearAllTables()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()
            } else if (it?.status.equals("error")) {
                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT)
                    .show()
            }

        })


        bookingssViewModel.bookingsDetailsfetch.observe(
            viewLifecycleOwner,
            Observer { bookingsDetailsRefresh ->
                Loader.hideLoader()
                if (bookingsDetailsRefresh.status.equals("success")) {
                    Loader.hideLoader()
                    callCurrentDate()
                    activity?.onBackPressed()
                } else if (bookingsDetailsRefresh.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        bookingsDetailsRefresh.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()
                    activity?.onBackPressed()
                }

            })


    }

    private fun fetDatFromServer(requireContext: Context) {
        Toast.makeText(
            requireContext(),
            "Refreshing ,Please wait....",
            Toast.LENGTH_SHORT
        )
            .show()
        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())
        if (requireContext().isConnectedToNetwork()) {
            GlobalScope.launch(Dispatchers.Main) {
                bookingssViewModel.fetDatFromServer(requireContext, currentDate)
            }
        } else {
            Loader.hideLoader()
            Snackbar.make(outerjobs, "Please connect to internet & refresh ", Snackbar.LENGTH_LONG)
                .show()
            activity?.onBackPressed()
        }

    }

    //    newChange
    private fun callCurrentDate() {
        val sdf = SimpleDateFormat("dd-MM-yyyy")
        var currentdate = sdf.format(Date())

        // DatesFromDb
        GlobalScope.launch(Dispatchers.Main) {
            try {

                val dateArray = jobsViewModel.getBookingDates(requireContext())

                val datelist: List<String>? = dateArray
                val selected = datelist?.filter { it.equals(currentdate) }

                if (selected?.isEmpty()!!) {

                    insertCurrentDate(currentdate)

                } else {
                    activity?.onBackPressed()
                }
            } catch (e: Exception) {
                Log.e("callDateData", "Exception: ${e.message}")
                activity?.onBackPressed()
            }
        }
    }

    //newChange
    private fun insertCurrentDate(currentDate: String) {
        GlobalScope.launch {
            jobsViewModel.insertDate(currentDate)
        }
    }

    private fun driverTransferDialog() {
        selected_driver = ""
        selected_zone = ""

        if (transferlist?.size != 0) {

            val transferbuilder = AlertDialog.Builder(requireContext())
            val transferview: View =
                LayoutInflater.from(requireContext()).inflate(R.layout.dialog_transfer, null)
            transferbuilder.setView(transferview)
            val transferalertDialog: AlertDialog = transferbuilder.create()

            rv_transferSchedule = transferview.findViewById(R.id.rv_transferSchedule)
            txt_title = transferview.findViewById<TextView>(R.id.transfer_title)

            txt_title.text = "Transfer Driver"


            transferAdapter = TransferAdapter(requireContext(), transferlist) {
                selected_driver = it?.tabletId.toString()
            }
            val layoutManager = GridLayoutManager(requireContext(), 2)
            rv_transferSchedule.layoutManager = layoutManager
            rv_transferSchedule.adapter = transferAdapter

            val img_closetransfer = transferview.findViewById(R.id.img_closetransfer) as ImageView
            img_closetransfer.setOnClickListener {
                onCustomerViewset()
                transferalertDialog.dismiss()
            }

            val transferButton = transferview.findViewById(R.id.transferButton) as Button

            transferButton.setOnClickListener {
                Log.d("TAG", "transferdailog: " + selected_driver + " , " + selected_zone)
                booking_list?.clear()
                if (maidarraylist != null) {
                    for (b_id in maidarraylist!!) {
                        booking_list?.add(b_id.booking_id.toString())
                    }
                }
                if (selected_driver.isNullOrEmpty()) {
                    Toast.makeText(requireContext(), "   Please select zone   ", Toast.LENGTH_LONG)
                        .show()
                } else {


                    @SuppressLint("SimpleDateFormat")
                    val sdf1 = SimpleDateFormat("yyyy/MM/dd")
                    val currentDate = sdf1.format(Date())

                    if (requireContext().isConnectedToNetwork()) {
                        Loader.showLoader(requireContext())
                        GlobalScope.launch(Dispatchers.Main) {
                            bookingssViewModel.getBookingsTransfer(
                                requireContext(),
                                prefs.user_id.toString(),
                                prefs.user_type.toString(),
                                currentDate,
                                prefs.token,
                                booking_id,
                                selected_driver,
                                "1",
                                prefs.is_driver_based.toString(),
                                maidarraylist?.get(0)?.maid_id,
                                booking_list
                            )
                        }
                        transferalertDialog.dismiss()
                    } else {
                        Snackbar.make(
                            outerdetails,
                            "Please connect to internet",
                            Snackbar.LENGTH_LONG
                        )
                            .show()
                    }
                }
            }
            transferalertDialog.show()

        } else {

            Snackbar.make(outerdetails, "Transfer List is empty", Snackbar.LENGTH_LONG).show()

        }
    }

    private fun maidDetailsDialog(
        maidBookingsStatus: ResponseMaidBookingDetailsList?
    ) {

        val sdf = SimpleDateFormat("dd-MM-yyyy")
        val currentdate = sdf.format(Date())

        val prefs = SharedPref

        var maidstatus = 0

        val builder = AlertDialog.Builder(requireContext())
        val dialogview: View = LayoutInflater.from(requireContext()).inflate(
            R.layout.dialog_maiddetails,
            null
        )
        builder.setView(dialogview)

        rvTimeSchedule = dialogview.findViewById(R.id.rv_timeSchedule)
        val l_noSchedule = dialogview.findViewById<LinearLayout>(R.id.l_noSchedule)
        val txt_maidname = dialogview.findViewById<TextView>(R.id.txt_maidname)
        val btn_maidstatus_changer = dialogview.findViewById<TextView>(R.id.btn_maidstatus_changer)
        val txt_remainingtime_value =
            dialogview.findViewById<TextView>(R.id.txt_remainingtime_value)
        val img_maid = dialogview.findViewById<ImageView>(R.id.img_maid)

        val alertDialog: AlertDialog = builder.create()



        when (prefs.user_type) {
            "0" -> btn_maidstatus_changer.visibility = View.GONE
            "1" -> {

                btn_maidstatus_changer.visibility = View.VISIBLE

                if (booking_date != null) {
                    if (currentdate.toString() != booking_date) {
                        btn_maidstatus_changer.visibility = View.GONE
                    } else {
                        btn_maidstatus_changer.visibility = View.VISIBLE

                    }
                }
            }
        }







        if (maidBookingsStatus?.maidDetails?.maidImage!!.isNotEmpty()) {

            Glide.with(requireContext())
                .load(maidBookingsStatus.maidDetails.maidImage)
                .placeholder(R.drawable.nomaidimage)
                .error(R.drawable.nomaidimage)
                .into(img_maid)

        } else {
            img_maid.setImageResource(R.drawable.nomaidimage)
        }


        txt_maidname.text = maidBookingsStatus?.maidDetails?.maidName




        Log.d("TAG", "maidDetailsDialog 1: " + maidBookingsStatus?.maidDetails?.maidAttendance)
        when (maidBookingsStatus?.maidDetails?.maidAttendance) {
            "0" -> {
                maidstatus = 1
                btn_maidstatus_changer.text = "Maid In  "
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_green)
            }
            "1" -> {
                maidstatus = 2
                btn_maidstatus_changer.text = "Maid Out"
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_red)
            }
            "2" -> {
                maidstatus = 1
                btn_maidstatus_changer.text = "Maid In  "
                btn_maidstatus_changer.setBackgroundResource(R.drawable.bg_green)
            }
        }

        Log.d("TAG", "maidDetailsDialog 2: " + maidstatus)



        if (maidBookingsStatus?.maidDetails?.nextSchedules?.size != 0) {
            rvTimeSchedule.visibility = View.VISIBLE
            l_noSchedule.visibility = View.GONE

            timeScheduleAdapter =
                TimeScheduleAdapter(maidBookingsStatus?.maidDetails?.nextSchedules)
            val layoutManager = LinearLayoutManager(requireContext())
            rvTimeSchedule.layoutManager = layoutManager
            rvTimeSchedule.adapter = timeScheduleAdapter
        } else {
            rvTimeSchedule.visibility = View.GONE
            l_noSchedule.visibility = View.VISIBLE
        }


        val img_close = dialogview.findViewById(R.id.img_close) as ImageView
        img_close.setOnClickListener {
            alertDialog.dismiss()
        }

        btn_maidstatus_changer.setOnClickListener {
            maidStatuschange(maidBookingsStatus?.maidDetails, maidstatus)
            alertDialog.dismiss()
        }

        alertDialog.show()
    }


    private fun maidStatuschange(maidDetails: MaidDetails?, status: Int) {

        if (requireContext().isConnectedToNetwork()) {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                bookingssViewModel.getMaidStatusChange(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    currentDate,
                    prefs.token,
                    status,
                    maidDetails?.maidId,
                    ""
                )
            }


        } else {
            Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                .show()
        }


    }


    override fun onResume() {
        super.onResume()
        Loader.showLoader(requireContext())
        Log.d("TAG", "onResume: ")
        onCustomerViewset()

    }


    private fun transferListFetch() {

        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy-MM-dd") //newChange(2021-11-14)
        val currentDate = sdf1.format(Date())

        GlobalScope.launch(Dispatchers.Main) {
            bookingssViewModel.getTransferlist(
                prefs.user_id.toString(),
                prefs.user_type.toString(),
                currentDate,
                prefs.token,
                booking_id
            )
        }
    }

    private fun onClick() {

        requireActivity().img_direction.setOnClickListener {


            if (customerDetailsFromDb?.get(0)?.customerLatitude?.isEmpty() == true || customerDetailsFromDb?.get(
                    0
                )?.customerLongitude?.isEmpty() == true
            ) {
                Toast.makeText(
                    requireContext(),
                    "Location information not available",
                    Toast.LENGTH_SHORT
                ).show()


            } else {
                val gmmIntentUri: Uri
                gmmIntentUri =
                    if (customerDetailsFromDb?.get(0)?.customerLatitude?.isEmpty() == true) {
                        Uri.parse(
                            "google.navigation:q=" + customerDetailsFromDb?.get(0)?.area.toString() + ", "
                                    + customerDetailsFromDb?.get(0)?.customerAddress
                        )
                    } else {
                        Uri.parse(
                            "google.navigation:q=" + customerDetailsFromDb?.get(0)?.customerLatitude
                                .toString() + "," + customerDetailsFromDb?.get(0)?.customerLongitude
                        )
                    }
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                if (mapIntent.resolveActivity(requireContext().getPackageManager()) != null) {
                    startActivity(mapIntent)
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Google Maps not available, Please install from play store.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }


        }
        requireActivity().img_call.setOnClickListener {
            makePhoneCall()
        }

        parent.img_option.setOnClickListener {
            val popupMenu: PopupMenu = PopupMenu(requireContext(), parent.img_option)
            popupMenu.menuInflater.inflate(R.menu.menu_details, popupMenu.menu)


            when (service_status) {
                "0" -> {
                    //key
                    if (key_status) { //= "yes"


                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(true);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    } else { // = "No"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(true);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    }
                }
                "1" -> {
                    //key
                    if (key_status) { // = "yes"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(true);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    } else { // = "No"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(true);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    }
                }
                "2" -> {
                    //key
                    if (key_status) { // = "yes"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(true);
                    } else { // = "No"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(true);
                    }
                }
                else -> {
                    //key
                    if (key_status) { // = "yes"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    } else { // = "No"
                        popupMenu.getMenu().findItem(R.id.navigation_addkey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_removekey).setVisible(false);
                        popupMenu.getMenu().findItem(R.id.navigation_invoice).setVisible(false);
                    }
                }
            }

            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.navigation_addkey -> {

                        val alertDialog = AlertDialog.Builder(requireContext()).create()
                        alertDialog.setTitle("Confirmation")
                        alertDialog.setMessage("Do you want to add key?")

                        alertDialog.setButton(
                            AlertDialog.BUTTON_POSITIVE, "Confirm"
                        ) { dialog, which ->
                            if (requireContext().isConnectedToNetwork()) {
                                Loader.showLoader(requireContext())
                                GlobalScope.launch(Dispatchers.Main) {
                                    bookingssViewModel.getKeyStatusChange(
                                        requireContext(),
                                        prefs.user_id,
                                        prefs.user_type,
                                        prefs.token,
                                        "1",
                                        customer_id
                                    )
                                }
                            } else {

                                Snackbar.make(
                                    outerdetails,
                                    "Please connect to internet",
                                    Snackbar.LENGTH_LONG
                                )
                                    .show()

                            }
                            dialog.dismiss()
                        }

                        alertDialog.setButton(
                            AlertDialog.BUTTON_NEGATIVE, "Cancel"
                        ) { dialog, which -> dialog.dismiss() }
                        alertDialog.show()

                        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

                        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                        layoutParams.weight = 10f
                        btnPositive.layoutParams = layoutParams
                        btnNegative.layoutParams = layoutParams


                    }
                    R.id.navigation_removekey -> {


                        val alertDialog = AlertDialog.Builder(requireContext()).create()
                        alertDialog.setTitle("Confirmation")
                        alertDialog.setMessage("Do you want to remove key?")

                        alertDialog.setButton(
                            AlertDialog.BUTTON_POSITIVE, "Confirm"
                        ) { dialog, which ->
                            if (requireContext().isConnectedToNetwork()) {
                                Loader.showLoader(requireContext())
                                GlobalScope.launch(Dispatchers.Main) {
                                    bookingssViewModel.getKeyStatusChange(
                                        requireContext(),
                                        prefs.user_id,
                                        prefs.user_type,
                                        prefs.token,
                                        "0",
                                        customer_id
                                    )
                                }
                            } else {

                                Snackbar.make(
                                    outerdetails,
                                    "Please connect to internet",
                                    Snackbar.LENGTH_LONG
                                )
                                    .show()

                            }
                            dialog.dismiss()
                        }

                        alertDialog.setButton(
                            AlertDialog.BUTTON_NEGATIVE, "Cancel"
                        ) { dialog, which -> dialog.dismiss() }
                        alertDialog.show()

                        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

                        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                        layoutParams.weight = 10f
                        btnPositive.layoutParams = layoutParams
                        btnNegative.layoutParams = layoutParams


                    }
                    R.id.navigation_invoice -> {
                        var spf = SimpleDateFormat("dd-MM-yyyy")
                        val newDatee = spf.parse(booking_date)
                        spf = SimpleDateFormat("yyyy-MM-dd")
                        val date: String = spf.format(newDatee)
                        GlobalScope.launch(Dispatchers.Main) {
                            Loader.showLoader(requireContext())
                            bookingssViewModel.getDetailInvoice(
                                prefs.user_id.toString(),
                                prefs.user_type.toString(),
                                prefs.token,
                                booking_id,
                                date
                            )
                        }
                    }

                }
                true
            })
            popupMenu.show()
        }

        btn_start.setOnClickListener {


            if (!startButtonClicked) {
                val alertDialog = AlertDialog.Builder(requireContext()).create()
                alertDialog.setTitle("Confirmation")
                alertDialog.setMessage("Do you want to start this booking?")

                alertDialog.setButton(
                    AlertDialog.BUTTON_POSITIVE, "Confirm"
                ) { dialog, which ->
                    if (requireContext().isConnectedToNetwork()) {
                        startService(maidarraylist)
                    } else {
                        Snackbar.make(
                            outerdetails,
                            "Please connect to internet",
                            Snackbar.LENGTH_LONG
                        )
                            .show()
                    }
                    dialog.dismiss()
                }

                alertDialog.setButton(
                    AlertDialog.BUTTON_NEGATIVE, "Cancel"
                ) { dialog, which -> dialog.dismiss() }
                alertDialog.show()

                val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

                val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
                layoutParams.weight = 10f
                btnPositive.layoutParams = layoutParams
                btnNegative.layoutParams = layoutParams


            }
        }
        btn_stop.setOnClickListener {

            if (!stopButtonClicked) {
                booking_list?.clear()
                if (maidarraylist != null) {
                    for (b_id in maidarraylist!!) {
                        booking_list?.add(b_id.booking_id.toString())
                    }
                }


                val bundle = Bundle()
                bundle.putString("Booking_id", booking_id)

                bundle.putSerializable("Booking_id List", booking_list)

                bundle.putString(
                    "Customer_id",
                    customerDetailsFromDb?.get(0)?.customer_id.toString()
                )
                bundle.putString(
                    "Service_amount",
                    customerDetailsFromDb?.get(0)?.serviceFee.toString()
                )
                bundle.putString(
                    "Service_status",
                    customerDetailsFromDb?.get(0)?.service_status.toString()
                )
                bundle.putString("Total", customerDetailsFromDb?.get(0)?.total.toString())
                bundle.putString(
                    "Outstanding_Balance",
                    customerDetailsFromDb?.get(0)?.outstandingBalance.toString()
                )
                bundle.putString(
                    "Payment_status",
                    customerDetailsFromDb?.get(0)?.paymentStatus.toString()
                )
                bundle.putString("MOP", customerDetailsFromDb?.get(0)?.mop.toString())
                bundle.putString(
                    "Paid_amount",
                    customerDetailsFromDb?.get(0)?.paidAmount.toString()
                )
                bundle.putString("booking_date", booking_date)


                findNavController().navigate(
                    R.id.action_bookingdetails_to_paymentdetails, bundle
                )
            }
        }

        btn_transfer.setOnClickListener {
            if (!transferButtonClicked) {

                Loader.showLoader(requireContext())
                if (requireContext().isConnectedToNetwork()) {
                    if (prefs.is_driver_based.equals("yes")) {
                        driverTransferListFetch() //Driver transfer
                    } else {
                        transferListFetch() //Zone transfer
                    }
                } else {
                    Loader.hideLoader()
                    Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                        .show()

                }

            }
        }

        btn_invoice.setOnClickListener {
        }
    }

    private fun driverTransferListFetch() {
        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy-MM-dd") //newChange(2021-11-14)
        val currentDate = sdf1.format(Date())

        GlobalScope.launch(Dispatchers.Main) {
            bookingssViewModel.getDriverTransferList(
                prefs.user_id.toString(),
                prefs.user_type.toString(),
                currentDate,
                prefs.token,
                booking_id
            )
        }
    }

    private fun startService(maidarraylist: List<MaidTable>?) {

        val sdf = SimpleDateFormat("hh:mm:ss")
        starttime = sdf.format(Date())

        booking_list?.clear()
        if (maidarraylist != null) {
            for (b_id in maidarraylist) {
                booking_list?.add(b_id.booking_id.toString())
            }
        }
        Log.d("Start Button", "startService: " + booking_list)
        @SuppressLint("SimpleDateFormat")
        val sdf1 = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf1.format(Date())

        Loader.showLoader(requireContext())
        GlobalScope.launch(Dispatchers.Main) {

            bookingssViewModel.getStartService(
                requireContext(),
                prefs.user_id,
                prefs.user_type,
                currentDate,
                prefs.token,
                booking_list!!, // booking_id
                "1", starttime,
                booking_id
            )
        }
    }

    private fun transferdailog() {

        selected_driver = ""
        selected_zone = ""

        if (transferlist?.size != 0) {

            val transferbuilder = AlertDialog.Builder(requireContext())
            val transferview: View =
                LayoutInflater.from(requireContext()).inflate(R.layout.dialog_transfer, null)
            transferbuilder.setView(transferview)
            val transferalertDialog: AlertDialog = transferbuilder.create()

            rv_transferSchedule = transferview.findViewById(R.id.rv_transferSchedule)

            txt_title = transferview.findViewById<TextView>(R.id.transfer_title)

            txt_title.text = "Transfer Zone"

            transferAdapter = TransferAdapter(requireContext(), transferlist) {
                selected_driver = it?.tabletId.toString()
                selected_zone = it?.zoneId.toString()
            }
            val layoutManager = GridLayoutManager(requireContext(), 2)
            rv_transferSchedule.layoutManager = layoutManager
            rv_transferSchedule.adapter = transferAdapter

            val img_closetransfer = transferview.findViewById(R.id.img_closetransfer) as ImageView
            img_closetransfer.setOnClickListener {
                onCustomerViewset()
                transferalertDialog.dismiss()
            }

            val transferButton = transferview.findViewById(R.id.transferButton) as Button

            transferButton.setOnClickListener {
                Log.d("TAG", "transferdailog: " + selected_driver + " , " + selected_zone)

                if (selected_driver.isNullOrEmpty() || selected_zone.isNullOrEmpty()) {
                    Toast.makeText(requireContext(), "   Please select zone   ", Toast.LENGTH_LONG)
                        .show()
                } else {
                    transferConfirmationDialog(selected_driver, selected_zone)
                    transferalertDialog.dismiss()
                }
            }
            transferalertDialog.show()

        } else {

            Snackbar.make(outerdetails, "Transfer List is empty", Snackbar.LENGTH_LONG).show()

        }
    }

    private fun transferConfirmationDialog(selected_driver: String, selected_zone: String) {
        Log.d("TAG", "transferConfirmationDialog: " + selected_driver + selected_zone)

        val transferconfirmbuilder = AlertDialog.Builder(requireContext())
        val confirmview: View =
            LayoutInflater.from(requireContext())
                .inflate(R.layout.dialog_transfer_confirmation, null)
        transferconfirmbuilder.setView(confirmview)
        val transferalertDialog: AlertDialog = transferconfirmbuilder.create()

        val btn_transferall: TextView = confirmview.findViewById(R.id.btn_transferall)
        val btn_selectedonly: TextView = confirmview.findViewById(R.id.btn_selectedonly)
        val img_closeselection: ImageView = confirmview.findViewById(R.id.img_closeselection)

        img_closeselection.setOnClickListener {
            onCustomerViewset()
            transferalertDialog.dismiss()
        }

        btn_transferall.setOnClickListener {
            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            booking_list?.clear()
            if (maidarraylist != null) {
                for (b_id in maidarraylist!!) {
                    booking_list?.add(b_id.booking_id.toString())
                }
            }

            if (requireContext().isConnectedToNetwork()) {

                Loader.showLoader(requireContext())
                GlobalScope.launch(Dispatchers.Main) {
                    bookingssViewModel.getBookingsTransfer(
                        requireContext(),
                        prefs.user_id.toString(),
                        prefs.user_type.toString(),
                        currentDate,
                        prefs.token,
                        booking_id,
                        selected_zone,
                        "1",
                        prefs.is_driver_based.toString(),
                        maidarraylist?.get(0)?.maid_id, booking_list
                    )
                }
                transferalertDialog.dismiss()

            } else {
                Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                    .show()
            }

        }
        btn_selectedonly.setOnClickListener {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())


            booking_list?.clear()
            if (maidarraylist != null) {
                for (b_id in maidarraylist!!) {
                    booking_list?.add(b_id.booking_id.toString())
                }
            }

            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())
                GlobalScope.launch(Dispatchers.Main) {
                    bookingssViewModel.getBookingsTransfer(
                        requireContext(),
                        prefs.user_id.toString(),
                        prefs.user_type.toString(),
                        currentDate,
                        prefs.token,
                        booking_id,
                        selected_zone,
                        "0",
                        prefs.is_driver_based.toString(),
                        maidarraylist?.get(0)?.maid_id, booking_list
                    )
                }
                transferalertDialog.dismiss()
            } else {
                Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG)
                    .show()
            }
        }

        transferalertDialog.show()
    }


    private fun makePhoneCall() {
        if (customerDetailsFromDb?.get(0)?.customerMobile.isNullOrEmpty()) {
            Toast.makeText(
                requireContext(),
                "Contact information not available",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + customerDetailsFromDb?.get(0)?.customerMobile)
            startActivity(intent)
        }


    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQ_CODE_CALL -> if (grantResults.size > 0) {
                val CallPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                if (CallPermission) {
                    makePhoneCall()

                }
            }
        }
    }


    private fun MaidClick(maidtable: MaidTable?) {
        if (requireContext().isConnectedToNetwork()) {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())  //booking_date

            var spf = SimpleDateFormat("dd-MM-yyyy")
            val newDatee = spf.parse(booking_date)
            spf = SimpleDateFormat("yyyy-MM-dd")
            val date: String = spf.format(newDatee)


            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                bookingssViewModel.getMaidbookingsDetailslist(
                    requireContext(),
                    prefs.user_id,
                    prefs.user_type,
                    date,
                    prefs.token,
                    maidtable?.maid_id,
                    booking_id
                )
            }


        } else {
            Snackbar.make(outerdetails, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }


    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}