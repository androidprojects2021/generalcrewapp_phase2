package com.azinova.emaidcrewapp.ui.fragment.ui.maid_attendence

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.maidattendance.MaidDetailsItem
import com.bumptech.glide.Glide

class MaidAttendenceAdapter(val requireContext: Context, var maidDetails: List<MaidDetailsItem?>?
                            , val onItemClicked: (MaidDetailsItem?, Int,Int, List<MaidDetailsItem?>?) -> Unit
) : RecyclerView.Adapter<MaidAttendenceAdapter.ViewHolder>()  {
    var maidstatus = 0
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MaidAttendenceAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_maid_attendence, parent, false))

    }

    override fun onBindViewHolder(holder: MaidAttendenceAdapter.ViewHolder, position: Int) {
        holder.txt_username.text = maidDetails?.get(position)?.maidName
        holder.txt_si_no.text = maidDetails?.get(position)?.sl_no
        Log.d("MAID ", "onBindViewHolder: "+maidDetails?.get(position)?.sl_no)

        if (maidDetails?.get(position)?.maid_image!!.isNotEmpty()) {

            Glide.with(requireContext)
                    .load(maidDetails?.get(position)?.maid_image)
                    .placeholder(R.drawable.nomaidimage)
                    .error(R.drawable.nomaidimage)
                    .into(holder.img_maid_profile)

        } else {
            holder.img_maid_profile.setImageResource(R.drawable.nomaidimage)
        }

        when(maidDetails?.get(position)?.maidAttandence){
            "0" -> { maidin(holder) }
            "1" -> { maidout(holder) }
            "2" -> { maidin(holder) }
        }

        holder.btn_maidstart.setOnClickListener {
            onItemClicked.invoke(maidDetails?.get(position),maidstatus,position,maidDetails) }

    }

    private fun maidout(holder: MaidAttendenceAdapter.ViewHolder) {
        maidstatus = 2
        holder.btn_maidstart.text = "Time Out"
        holder.btn_maidstart.setBackgroundResource(R.drawable.bg_red)
        holder.btn_maidstart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_time_out, 0, 0, 0)
    }

    private fun maidin(holder: ViewHolder) {
        maidstatus = 1
        holder.btn_maidstart.text = "Time In   "
        holder.btn_maidstart.setBackgroundResource(R.drawable.bg_green)
        holder.btn_maidstart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_time_in, 0, 0, 0)
    }


    override fun getItemCount(): Int {
        return maidDetails?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_username = itemView.findViewById<TextView>(R.id.txt_username)
        var img_maid_profile = itemView.findViewById<ImageView>(R.id.img_maid_profile)
        var view = itemView.findViewById<View>(R.id.view)
        var btn_maidstart = itemView.findViewById<TextView>(R.id.btn_maidstart)
        var txt_si_no = itemView.findViewById<TextView>(R.id.txt_si_no)

    }

    fun refreshData(items: List<MaidDetailsItem?>?) {
        this.maidDetails = items
        notifyDataSetChanged()
    }
}