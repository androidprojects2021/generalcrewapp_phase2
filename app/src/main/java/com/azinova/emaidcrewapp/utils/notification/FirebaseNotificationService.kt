package com.azinova.emaidcrewapp.utils.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.fcm_payload.ResponseFcmPayload
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class FirebaseNotificationService : FirebaseMessagingService() {
    private val prefs = SharedPref
    var json: JSONObject? = null
    override fun onNewToken(token: String) {
        super.onNewToken(token)

        Log.d(TAG, "Token : " + token)
        Log.d("Firebase Token", "clickListen - token: " + token)

        prefs.firebasetoken = token
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.d(TAG, "Remote: " + remoteMessage)
        Log.d(TAG, "From: " + remoteMessage.from)

        val push_id = remoteMessage.data["pushid"]
        Log.d(TAG, "PushId: $push_id")

        Log.e(TAG, "Message data payload: " + remoteMessage.data)


        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.e(TAG, "Message data payload: " + remoteMessage.data)


            Log.e(TAG, "onMessageReceived: ")


            val data = remoteMessage.data["payload"]
            val firebasePayloadData = Gson().fromJson(data, ResponseFcmPayload::class.java)


            if (firebasePayloadData.pushid != null) {
                val pushId = firebasePayloadData.pushid
                Log.d(TAG, "PushId: $pushId")
                prefs.push_id = pushId
            }




        }



        if (remoteMessage.notification != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.notification!!.body)
            Log.e(TAG, "Message Notification Maid: " + remoteMessage.data["maid"])
            Log.e(TAG, "Message Notification Customer: " + remoteMessage.data["customer"])
            Log.e(TAG, "Message Notification BookingTime: " + remoteMessage.data["bookingTime"])
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    generateNotification(
                        this, remoteMessage.notification!!.body!!,
                        remoteMessage.notification!!.title!!,
                        remoteMessage.data["maid"],
                        remoteMessage.data["customer"],
                        remoteMessage.data["bookingTime"]
                    )
                }
            } catch (e: Exception) {
            }

        }

        Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification()?.getTitle())


    }


    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onMessageSent(s: String) {
        super.onMessageSent(s)
    }

    override fun onSendError(s: String, e: Exception) {
        super.onSendError(s, e)
    }

    companion object {
        private const val TAG = "Firebase"

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        private fun generateNotification(
            context: Context,
            message: String,
            title: String,
            maid: String?,
            customer: String?,
            bookingTime: String?
        ) {
            try {
                if (BaseActivity.appInFront) {
                    Log.d("Elitemaid 1", "generateNotification: ")

                    try {
                        playsound(context)
                    } catch (e: IllegalArgumentException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    val myIntent = Intent(context, Myservices::class.java)
                    myIntent.putExtra("message", message)
                    myIntent.putExtra("title", title)
                    myIntent.putExtra("maid", maid)
                    myIntent.putExtra("customer", customer)
                    myIntent.putExtra("bookingTime", bookingTime)
                    myIntent.putExtra("heading", "Notification")
                    myIntent.putExtra("isNotification7", "isNotification")
                    context.startService(myIntent)

                } else {
                    Log.d("Elitemaid 2", "generateNotification: ")
                    Log.d("appInBack ", "generateNotification: " + message)

                    var notification_id = 0
                    val c = Calendar.getInstance()
                    val df = SimpleDateFormat("kkmmss")
                    val strTodayDate = df.format(c.time)

                    notification_id = try {
                        strTodayDate.toInt()
                    } catch (e: Exception) {
                        0
                    }


                    val mBuilder: NotificationCompat.Builder =
                        NotificationCompat.Builder(context).setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(message).setContentText("")
                    mBuilder.setAutoCancel(true)
                    val resultIntent = Intent(context, MainActivity::class.java)
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    resultIntent.putExtra("page", "notification")

                    // Add the bundle to the intent
                    val stackBuilder = TaskStackBuilder.create(context)
                    stackBuilder.addNextIntent(resultIntent)
                    val resultPendingIntent: PendingIntent =
                        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                    mBuilder.setContentIntent(resultPendingIntent)
                    val alarmSound: Uri =
                        RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                    mBuilder.setSound(alarmSound)
                    val mNotificationManager: NotificationManager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    mNotificationManager.notify(notification_id, mBuilder.build())


                }
            } catch (e: Exception) {
                var notification_id = 0
                val c = Calendar.getInstance()
                val df = SimpleDateFormat("kkmmss")
                val strTodayDate = df.format(c.time)
                notification_id = try {
                    strTodayDate.toInt()
                } catch (es: Exception) {
                    0
                }
                val mBuilder: NotificationCompat.Builder =
                    NotificationCompat.Builder(context).setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(message).setContentText("")
                mBuilder.setAutoCancel(true)
                val resultIntent = Intent(context, MainActivity::class.java)
                resultIntent.putExtra("page", "notification")

                // Add the bundle to the intent
                val stackBuilder = TaskStackBuilder.create(context)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    stackBuilder.addNextIntent(resultIntent)
                }
                val resultPendingIntent: PendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                val alarmSound: Uri =
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                mBuilder.setSound(alarmSound)
                val mNotificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (mNotificationManager != null) {
                    mNotificationManager.notify(notification_id, mBuilder.build())
                }
            }
        }

        fun playsound(context: Context?) {
            val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            RingtoneManager.getRingtone(context, alarmSound).play()
        }
    }
}