package com.azinova.emaidcrewapp.model.response.location

import com.google.gson.annotations.SerializedName

data class ResponseLocationUpdate(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
