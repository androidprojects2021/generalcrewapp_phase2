package com.azinova.emaidcrewapp.ui.fragment.ui.jobs

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.SampleBookingResponse
import com.azinova.emaidcrewapp.model.response.schedules.ScheduleResponse
import com.pluscubed.recyclerfastscroll.RecyclerFastScroller
import java.text.SimpleDateFormat





class BookingsAdapter(
    val context: Context, val schedulelist: List<ScheduleResponse?>?,
    val onItemClicked: (ScheduleResponse?, Int, List<ScheduleResponse?>?) -> Unit
) : RecyclerView.Adapter<BookingsAdapter.ViewHolder>() {  //

    lateinit var maidListAdapter: MaidListAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingsAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.new_card_bookings, parent, false)
        )  // old xml - card_bookings

    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: BookingsAdapter.ViewHolder, position: Int) {

        if (schedulelist?.get(position)?.service_status == 0) {
            holder.l_maid_list.setBackgroundResource(R.color.color_pending_light)
            holder.layout_schedule.setBackgroundResource(R.drawable.bg_pending)
            holder.txt_status.text = "   Pending   "
            holder.txt_status.setBackgroundResource(R.drawable.pending_label)

            holder.material_vacuum.setImageResource(R.drawable.pending_vaccum)
            holder.material_chemical.setImageResource(R.drawable.pending_cleaner)
            holder.key.setImageResource(R.drawable.pending_key)

            holder.fastScroller.handleNormalColor =
                ContextCompat.getColor(context, R.color.color_pending_dark)
            holder.fastScroller.handlePressedColor =
                ContextCompat.getColor(context, R.color.color_pending_dark)


        } else if (schedulelist?.get(position)?.service_status == 1) {
            holder.l_maid_list.setBackgroundResource(R.color.color_ongoing_light)
            holder.layout_schedule.setBackgroundResource(R.drawable.bg_ongoing)
            holder.txt_status.text = "   Ongoing   "
            holder.txt_status.setBackgroundResource(R.drawable.ongoing_label)

            holder.material_vacuum.setImageResource(R.drawable.ongoing_vaccum)
            holder.material_chemical.setImageResource(R.drawable.ongoing_cleaner)
            holder.key.setImageResource(R.drawable.ongoing_key)

            holder.fastScroller.handleNormalColor =
                ContextCompat.getColor(context, R.color.color_ongoing_dark)
            holder.fastScroller.handlePressedColor =
                ContextCompat.getColor(context, R.color.color_ongoing_dark)


        } else if (schedulelist?.get(position)?.service_status == 2) {
            holder.l_maid_list.setBackgroundResource(R.color.color_finished_light)
            holder.layout_schedule.setBackgroundResource(R.drawable.bg_finished)
            holder.txt_status.text = "   Finished   "
            holder.txt_status.setBackgroundResource(R.drawable.finish_lable)

            holder.material_vacuum.setImageResource(R.drawable.finished_vaccum)
            holder.material_chemical.setImageResource(R.drawable.finished_cleaner)
            holder.key.setImageResource(R.drawable.finished_key)

            holder.fastScroller.handleNormalColor =
                ContextCompat.getColor(context, R.color.color_finished_dark)
            holder.fastScroller.handlePressedColor =
                ContextCompat.getColor(context, R.color.color_finished_dark)

        } else if (schedulelist?.get(position)?.service_status == 3) {
            holder.l_maid_list.setBackgroundResource(R.color.color_cancelled_light)
            holder.layout_schedule.setBackgroundResource(R.drawable.bg_cancel)
            holder.txt_status.text = "   Cancelled  "
            holder.txt_status.setBackgroundResource(R.drawable.cancel_label)


            holder.material_vacuum.setImageResource(R.drawable.cancel_vaccum)
            holder.material_chemical.setImageResource(R.drawable.cancel_cleaner)
            holder.key.setImageResource(R.drawable.cancel_key)

            holder.fastScroller.handleNormalColor =
                ContextCompat.getColor(context, R.color.color_cancelled_dark)
            holder.fastScroller.handlePressedColor =
                ContextCompat.getColor(context, R.color.color_cancelled_dark)

        }


        //---PAYMENT STATUS
        if (schedulelist?.get(position)?.paymentStatus.toString() == "0") {

            holder.img_paystatus.setImageResource(R.drawable.new_notpaid) //= "Not Paid"
        } else if (schedulelist?.get(position)?.paymentStatus.toString() == "1") {

            holder.img_paystatus.setImageResource(R.drawable.new_paid)// = "Paid"
        } else if (schedulelist?.get(position)?.paymentStatus.toString() == "2") {
            // = "Paid"
            holder.img_paystatus.setImageResource(R.drawable.new_online) // = "Online"
        } else {
            holder.img_paystatus.visibility = View.INVISIBLE
        }


        holder.tv_name.text = schedulelist?.get(position)?.customer_name.toString()
        holder.tv_address.text = schedulelist?.get(position)?.customerAddress


        val _24HourTimeshiftStart = schedulelist!!.get(position)?.shift_start
        val _24HourSDFS = SimpleDateFormat("HH:mm")
        val _12HourSDFS = SimpleDateFormat("hh:mm a")
        val shiftStart = _24HourSDFS.parse(_24HourTimeshiftStart)

        val _24HourTimeshiftEnd = schedulelist!!.get(position)?.shift_end
        val _24HourSDF = SimpleDateFormat("HH:mm")
        val _12HourSDF = SimpleDateFormat("hh:mm a")
        val shiftEnd = _24HourSDF.parse(_24HourTimeshiftEnd)

        holder.tv_timeing.text =
            _12HourSDFS.format(shiftStart) + " - " + _12HourSDF.format(shiftEnd)

        holder.tv_location.text =
            schedulelist.get(position)?.area
        holder.tv_zone.text = schedulelist.get(position)?.zone


        //cleaning material
        if (schedulelist.get(position)?.cleaning_material.toString() == "no") {
            holder.material_chemical.visibility = View.INVISIBLE
            //holder.tv_key.text = "No"
        } else if (schedulelist?.get(position)?.cleaning_material.toString() == "yes") {
            holder.material_chemical.visibility = View.VISIBLE
            //holder.tv_key.text = "Yes"
        }
        //vaccum material : vaccum_cleaner : 0 means disabled,  1 means enabled
        if (schedulelist.get(position)?.vaccum_cleaner.toString() == "0") {
            holder.material_vacuum.visibility = View.INVISIBLE
            // = "No"
        } else if (schedulelist?.get(position)?.vaccum_cleaner.toString() == "1") {
            holder.material_vacuum.visibility = View.VISIBLE
            //= "Yes"
        }
        //key
        if (schedulelist?.get(position)?.keyStatus.toString() == "0") {
            // = "No"
            holder.key.visibility = View.INVISIBLE
        } else if (schedulelist?.get(position)?.keyStatus.toString() == "1") {
            // = "Yes"
            holder.key.visibility = View.VISIBLE
        }


        //---TRANSFER STATUS
        when (schedulelist?.get(position)?.istransfer) {
            0 -> holder.img_transfer.visibility = View.INVISIBLE//= "Not "
            1 -> holder.img_transfer.visibility = View.VISIBLE// = "yes"
            else -> holder.img_transfer.visibility = View.INVISIBLE
        }

          // booking notes
        if (!schedulelist?.get(position)?.bookingNote.isNullOrEmpty()) {
            Log.d("NOTES", "onBindViewHolder: "+schedulelist?.get(position)?.customernotes)
            holder.img_notes.visibility = View.VISIBLE
        }else{
            holder.img_notes.visibility = View.GONE
        }

        holder.cv_booking.setOnClickListener {
            onItemClicked.invoke(schedulelist?.get(position), position, schedulelist)
        }

        if (schedulelist?.get(position)?.maidlist != null) {
            maidListAdapter = MaidListAdapter(
                context,
                schedulelist.get(position)?.maidlist
            )
            val lm = LinearLayoutManager(context)
            lm.orientation = LinearLayoutManager.VERTICAL
            holder.rv_maidlist.setLayoutManager(lm)




            holder.rv_maidlist.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener {
                override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                    val action: Int = e.action
                    when (action) {
                        MotionEvent.ACTION_MOVE -> rv.getParent()
                            .requestDisallowInterceptTouchEvent(true)
                    }
                    return false
                }

                override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
                }

                override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
                }

            })


            holder.rv_maidlist.setAdapter(maidListAdapter)
            holder.fastScroller.attachRecyclerView(holder.rv_maidlist)

        }


    }

    override fun getItemCount(): Int {
        return schedulelist?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout_schedule = itemView.findViewById<ConstraintLayout>(R.id.layout_schedule)
        var l_maid_list = itemView.findViewById<RelativeLayout>(R.id.l_maid_list)  //
        var rv_maidlist = itemView.findViewById<RecyclerView>(R.id.rv_maidlist)
        var fastScroller = itemView.findViewById<RecyclerFastScroller>(R.id.fasttrcv)

        var cv_booking = itemView.findViewById<CardView>(R.id.cv_booking)
        var tv_name = itemView.findViewById<TextView>(R.id.tv_name)
        var tv_time = itemView.findViewById<TextView>(R.id.tv_time)
        var tv_timeing = itemView.findViewById<TextView>(R.id.tv_timeing)
        var tv_location = itemView.findViewById<TextView>(R.id.tv_location)
        var tv_zone = itemView.findViewById<TextView>(R.id.tv_zone)
        var txt_status = itemView.findViewById<TextView>(R.id.txt_status)
        var tv_address = itemView.findViewById<TextView>(R.id.tv_address)

        var txt_maid1 = itemView.findViewById<TextView>(R.id.txt_maid1)
        var txt_maid2 = itemView.findViewById<TextView>(R.id.txt_maid2)
        var txt_maid3 = itemView.findViewById<TextView>(R.id.txt_maid3)
        var img_maid1 = itemView.findViewById<ImageView>(R.id.img_maid1)
        var img_maid2 = itemView.findViewById<ImageView>(R.id.img_maid2)
        var img_maid3 = itemView.findViewById<ImageView>(R.id.img_maid3)
        var img_paystatus = itemView.findViewById<ImageView>(R.id.img_paystatus)
        var img_transfer = itemView.findViewById<ImageView>(R.id.img_transfer)
        var img_notes = itemView.findViewById<ImageView>(R.id.img_notes)

       // var view1 = itemView.findViewById<View>(R.id.view1)
       // var view2 = itemView.findViewById<View>(R.id.view2)

        var material_chemical = itemView.findViewById<ImageView>(R.id.material_chemical)
        var material_vacuum = itemView.findViewById<ImageView>(R.id.material_vacuum)
        var key = itemView.findViewById<ImageView>(R.id.key)

    }


}