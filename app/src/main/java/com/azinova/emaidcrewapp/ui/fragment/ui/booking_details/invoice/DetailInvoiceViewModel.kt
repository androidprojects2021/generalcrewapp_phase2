package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.model.response.Invoice.ResponseInvoice
import com.azinova.emaidcrewapp.model.response.PdfUrl.ResponsePdfUrl
import com.azinova.emaidcrewapp.model.response.send_mail.ResponseSendMail
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref

class DetailInvoiceViewModel: ViewModel() {

    private val prefs = SharedPref

    private val _detailInvoice = MutableLiveData<ResponseInvoice?>()
    val detailInvoice: LiveData<ResponseInvoice?>
        get() = _detailInvoice

    private val _pdfUrl = MutableLiveData<ResponsePdfUrl?>()
    val pdfUrl: LiveData<ResponsePdfUrl?>
        get() = _pdfUrl

    private val _sendMail = MutableLiveData<ResponseSendMail>()
    val sendMail: LiveData<ResponseSendMail?>
    get() = _sendMail



    suspend fun getDetailInvoice(
        user_id: String,
        user_type: String,
        token: String?,
        booking_id: String,
        date: String
    ) {

            try {
                val response = ApiClient.getRetrofit().getInvoice(user_id, token!!, date, user_type, booking_id)

                if (response.status.equals("success")) {
                    _detailInvoice.value = response
                } else {
                    _detailInvoice.value = response
                }
            } catch (e: Exception) {
                _detailInvoice.value = ResponseInvoice(message = "Something Went Wrong", status = "Error")
            }
        }

    suspend fun needPdfUrl(user_id: String, token: String?, invoiceId: String?, user_type: String) {

        try {
            val response = ApiClient.getRetrofit().getPdfUrl(user_id,token!!,invoiceId!!,user_type)

            if (response.status.equals("success")){
                _pdfUrl.value = response
            }else{
                _pdfUrl.value = response
            }
        }catch (e:Exception){
            _pdfUrl.value = ResponsePdfUrl(status = "Error")
        }

    }

    suspend fun sendInvoiceMail(
        userId: String,
        token: String?,
        user_type: String,
        invoiceId: String?
    ) {
        try {
            val response = ApiClient.getRetrofit().sendMail(userId,token!!,user_type,invoiceId!!)

            if (response.status.equals("success")){
                _sendMail.value = response
            }else{
                _sendMail.value = response
            }
        }catch (e: Exception){
            _sendMail.value = ResponseSendMail(message = "Something Went Wrong", status = "Error")
        }
    }
}