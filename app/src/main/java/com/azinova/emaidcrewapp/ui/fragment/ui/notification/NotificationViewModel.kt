package com.azinova.emaidcrewapp.ui.fragment.ui.notification

import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaidcrewapp.model.response.notification.ResponseNotification
import com.azinova.emaidcrewapp.model.response.update_notification.ResponseUpdateNotification
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.azinova.emaidcrewapp.utils.notification.NotificationDioalogue
import kotlinx.coroutines.launch

class NotificationViewModel:ViewModel() {

    val prefs = SharedPref

    private val _notificationlistStatus = MutableLiveData<ResponseNotification>()
    val notificationlistStatus: LiveData<ResponseNotification>
        get() = _notificationlistStatus

    private val _updateNotification = MutableLiveData<ResponseUpdateNotification>()
    val updateNotification: LiveData<ResponseUpdateNotification>
    get() = _updateNotification


    fun getNotificationList(userId: String, currentdate: String, userType: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getnotificationList(userId,userType,currentdate,token!!)
                if (response.status.equals("success")) {
                    _notificationlistStatus.value = response

                } else {
                    _notificationlistStatus.value = response
                }
            } catch (e: Exception) {
                _notificationlistStatus.value = ResponseNotification(message = "Something went wrong", status = "Error")

            }
        }
    }

    fun updateNotification(
        user_id: String,
        token: String,
        user_tyoe: String,
        read_status: String,
        push_id: String
    ) {
        viewModelScope.launch {
            try {
                val response = ApiClient.getRetrofit().updateNotifications(user_id,token,user_tyoe,read_status,push_id)

                    if (response.status.equals("success")){
                        _updateNotification.value = response
                }else{
                    _updateNotification.value = response
                }
            }catch (e: Exception){
                _updateNotification.value = ResponseUpdateNotification(status = "Error",message = "Something Went Wrong")
                Log.e("throw", "fetDatFromServer: Notification")
            }
        }
    }


}