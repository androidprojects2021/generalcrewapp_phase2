package com.azinova.emaidcrewapp.ui.fragment.ui.more

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.model.response.logout.ResponseLogout
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class MoreFragmentViewModel : ViewModel() {

    private val pref = SharedPref

    private val _logoutStatus = MutableLiveData<ResponseLogout>()
    val logoutStatus: LiveData<ResponseLogout>
        get() = _logoutStatus

    fun logOut(userId: String, userType: String?, token: String?) {

        GlobalScope.launch(Dispatchers.Main){
            try {
                val response = ApiClient.getRetrofit().hitLogout(userId,userType!!,token!!)
                if (response.status.equals("success")){
                    _logoutStatus.value = response
                }else{
                    _logoutStatus.value = response
                }
            }catch (e: Exception){
                _logoutStatus.value = ResponseLogout(status = "Error", message = "Something Went Wrong")
            }
        }
    }

}