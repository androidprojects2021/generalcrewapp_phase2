package com.azinova.emaidcrewapp.ui.fragment.ui.notification

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.notification.NotificationListItem
import com.azinova.emaidcrewapp.utils.SharedPref

class NotificationDetailsAdapter(val context: Context, val notificationList: List<NotificationListItem?>?,val onitemClick:(NotificationListItem?,Int,List<NotificationListItem?>?) -> Unit) : RecyclerView.Adapter<NotificationDetailsAdapter.ViewHolder>() {

    private val prefs = SharedPref
    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): NotificationDetailsAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_notificationview, parent, false))

    }

    override fun onBindViewHolder(holder: NotificationDetailsAdapter.ViewHolder, position: Int) {

        when(notificationList?.get(position)?.type){
            "1" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingadded_icon)

            }
            "2" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingcancel_icon)

            }
            "3" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_bookingschange_icon)

            }
            "4" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_general_icon)

            }
            "5" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_tranfer_maid_icon)

            }
            "6" -> {
                holder.img_notificationicon.setImageResource(R.drawable.ic_alert_icon)

            }
        }

        when(prefs.user_type){
            "0" -> {
                when(notificationList?.get(position)?.maid_read_status){
                    "0" -> {
                        holder.txt_notification_title.setTextColor(Color.BLACK)
                        holder.txt_notification.setTextColor(Color.BLACK)
                        holder.txt_notificationcustomer.setTextColor(Color.BLACK)
                        holder.txt_notificationshift.setTextColor(Color.BLACK)
                        holder.txt_timeing.setTextColor(Color.BLACK)

                        holder.notification.setOnClickListener {
                            onitemClick.invoke(notificationList.get(position),position,notificationList)
                        }
                    }
                    "1" -> {
                        holder.txt_notification_title.setTextColor(Color.LTGRAY)
                        holder.txt_notification.setTextColor(Color.LTGRAY)
                        holder.txt_notificationcustomer.setTextColor(Color.LTGRAY)
                        holder.txt_notificationshift.setTextColor(Color.LTGRAY)
                        holder.txt_timeing.setTextColor(Color.LTGRAY)
                    }
                }
            }

            "1" -> {
                when(notificationList?.get(position)?.read_status){
                    "0" -> {
                        holder.txt_notification_title.setTextColor(Color.BLACK)
                        holder.txt_notification.setTextColor(Color.BLACK)
                        holder.txt_notificationcustomer.setTextColor(Color.BLACK)
                        holder.txt_notificationshift.setTextColor(Color.BLACK)
                        holder.txt_timeing.setTextColor(Color.BLACK)

                        holder.notification.setOnClickListener {
                            onitemClick.invoke(notificationList.get(position),position,notificationList)
                        }
                    }
                    "1" -> {
                        holder.txt_notification_title.setTextColor(Color.LTGRAY)
                        holder.txt_notification.setTextColor(Color.LTGRAY)
                        holder.txt_notificationcustomer.setTextColor(Color.LTGRAY)
                        holder.txt_notificationshift.setTextColor(Color.LTGRAY)
                        holder.txt_timeing.setTextColor(Color.LTGRAY)
                    }
                }
            }
        }




        holder.txt_notification_title.text = notificationList?.get(position)?.title
        holder.txt_notification.text = notificationList?.get(position)?.maid
        holder.txt_notificationcustomer.text = notificationList?.get(position)?.customer
        holder.txt_notificationshift.text = notificationList?.get(position)?.booking_time


        holder.txt_timeing.text = notificationList?.get(position)?.time

        if ((position+1).equals(notificationList?.size)) // array.size
        { holder.view.visibility = View.GONE}
    }

    override fun getItemCount(): Int {
        return notificationList?.size!!
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_notification = itemView.findViewById<TextView>(R.id.txt_notification)
        var txt_notification_title = itemView.findViewById<TextView>(R.id.txt_notification_title)
        var txt_notificationcustomer = itemView.findViewById<TextView>(R.id.txt_notificationcustomer)
        var txt_notificationshift = itemView.findViewById<TextView>(R.id.txt_notificationshift)
        var txt_timeing = itemView.findViewById<TextView>(R.id.txt_timeing)
        var view = itemView.findViewById<View>(R.id.view)
        var img_notificationicon = itemView.findViewById<ImageView>(R.id.img_notificationicon)
        var notification = itemView.findViewById<ConstraintLayout>(R.id.cl_notification)

    }
}