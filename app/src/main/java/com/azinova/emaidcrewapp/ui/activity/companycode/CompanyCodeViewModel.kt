package com.azinova.emaidcrewapp.ui.activity.companycode

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.azinova.emaidcrewapp.model.response.company_code.ResponseCompanyCode
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class CompanyCodeViewModel(application: Application) : AndroidViewModel(application) {

    private val prefs = SharedPref
    private val _companyCodeStatus = MutableLiveData<ResponseCompanyCode>()
    val companyCodeStatus: LiveData<ResponseCompanyCode>
        get() = _companyCodeStatus


    init {
        application.let { prefs.with(it) }
    }

    fun hitCompany(companyCode: String) {
        GlobalScope.launch(Dispatchers.Main){
            try {
                val response = ApiClient.getRetrofit().companyCode(companyCode)
                if (response.status.equals("success")){
                    _companyCodeStatus.value = response
                }else{
                    _companyCodeStatus.value = response
                }

            }catch (e: Exception){
                _companyCodeStatus.value = ResponseCompanyCode(message = "Something went wrong",status = "error")
            }
        }
    }
}