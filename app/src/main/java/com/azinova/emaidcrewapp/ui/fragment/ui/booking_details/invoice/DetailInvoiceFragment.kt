package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.model.response.Invoice.DataDetails
import com.azinova.emaidcrewapp.ui.activity.pdfviewer.PdfViewerActivity
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.adapter.CrewDetailsAdapter
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.adapter.DetailsAdapter
import com.azinova.emaidcrewapp.utils.SharedPref
import kotlinx.android.synthetic.main.activity_booking_details.*
import kotlinx.android.synthetic.main.fragment_detail_invoice.*
import kotlinx.android.synthetic.main.fragment_detail_invoice.downloadPdf
import kotlinx.android.synthetic.main.fragment_detail_invoice.sharePdf
import kotlinx.android.synthetic.main.fragment_detail_invoice.showPdf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import android.app.Activity

import androidx.core.app.ActivityCompat

import android.content.pm.PackageManager
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentManager
import java.net.URI
import java.util.jar.Manifest
import android.content.pm.ResolveInfo


class DetailInvoiceFragment : Fragment() {
    private val prefs = SharedPref
    lateinit var detailInvoiceViewModel: DetailInvoiceViewModel
    var booking_id: String = ""
    lateinit var booking_date: String

    lateinit var rv_crew_details: RecyclerView
    lateinit var rv_details: RecyclerView

    lateinit var crewDetailsAdapter: CrewDetailsAdapter
    lateinit var detailsAdapter: DetailsAdapter

    lateinit var details: DataDetails


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_detail_invoice, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailInvoiceViewModel = ViewModelProvider(this)[DetailInvoiceViewModel::class.java]

        details = requireArguments().getSerializable("details") as DataDetails





        if (prefs.is_hide.equals("yes")) {
            btn_submit.visibility = View.VISIBLE
        } else {
            btn_submit.visibility = View.GONE
        }

        Log.e("PDFURL", "PDF : $pdfUrl")
        Log.e("PDFURL", "PDF : ${prefs.pdfUrl}")


        if (pdfUrl.isEmpty()) {
            GlobalScope.launch(Dispatchers.Main) {
                Loader.showLoader(requireContext())
                detailInvoiceViewModel.needPdfUrl(
                    prefs.user_id.toString(),
                    prefs.token,
                    details.invoice_id,
                    prefs.user_type.toString()
                )
            }
        }


        rv_crew_details = view.findViewById(R.id.rv_crew_details)
        rv_details = view.findViewById(R.id.rv_details)


        onClick()
        onObserve()
        initView()


    }

    private fun initView() {

        if (prefs.invoice_id.equals(details.invoice_id) && prefs.customer_sign.equals(true)) {
            prefs.customer_sign = true
        } else {
            prefs.invoice_id = details.invoice_id.toString()
            prefs.customer_sign = false
        }

        if (prefs.customer_sign.equals(true) || details.signature_status.equals("1")) {
            btn_customer_sign.visibility = View.GONE

        } else {
            btn_customer_sign.visibility = View.VISIBLE
        }

        txt_invoice_id.text = "Invoice :  ${details.inv_ref}"
        txt_name.text = details.customer_name
        txt_date.text = details.issued_date
        txt_addresstext.text = details.customer_address
        txt_sub_amount.text = "AED ${details.total_amount}"
        txt_vat_amount.text = "AED ${details.vat_amount}"
        txt_total_amount.text = "AED ${details.total_net_amount}"
        txt_vat.text = "VAT(${details.vat_percent}%)"


        if (details.line_items?.size != 0) {

        } else {
        }

        crewDetailsAdapter = CrewDetailsAdapter(requireContext(), details.line_items!!)
        rv_crew_details.adapter = crewDetailsAdapter
        detailsAdapter = DetailsAdapter(requireContext(), details.line_items!!)
        rv_details.adapter = detailsAdapter


    }

    private fun onObserve() {


        detailInvoiceViewModel.pdfUrl.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()
            if (it?.status.equals("success")) {
                if (it?.pdfurl?.isNotEmpty()!!) {
                    pdfUrl = it.pdfurl
                    lastWord = pdfUrl.substringAfterLast("/")
                    Log.e("PDFURLLAST", "url: $lastWord")
                    Log.e("PDFURL", "url: ${pdfUrl}")
                    val file = File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        "EmaidCrew_${lastWord}.pdf"
                    )
                    if (!file.exists()) {
                        downloadPdf(pdfUrl, lastWord)
                        Toast.makeText(requireContext(), "Downloading...", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        file.delete()
                        downloadPdf(pdfUrl, lastWord)
                        Toast.makeText(requireContext(), "Downloading...", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {

                    Log.e("pdf_empty", "Invoice Not Found")
                }
            } else {

                Log.e("pdf_failed", "Invoice Not Found")
            }
        })

        detailInvoiceViewModel.sendMail.observe(viewLifecycleOwner, Observer {
            Loader.hideLoader()

            if (it?.status.equals("success")) {
                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), it?.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun onClick() {
        btn_customer_sign.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("invoiceId", details.invoice_id)
            findNavController().navigate(
                R.id.action_detailInvoiceFragment_to_signatureFragment,
                bundle
            )
        }

        requireActivity().showPdf.setOnClickListener {
            if (!pdfUrl.equals("")) {

                PdfViewerActivity.url = pdfUrl

                val intent = Intent(requireContext(), PdfViewerActivity::class.java)
                startActivity(intent)
            }

        }

        requireActivity().layout_downloadPdf.setOnClickListener {
            if (!pdfUrl.equals("")) {
                GlobalScope.launch(Dispatchers.Main) {
                    Loader.showLoader(requireContext())
                    detailInvoiceViewModel.sendInvoiceMail(
                        prefs.user_id.toString(),
                        prefs.token,
                        prefs.user_type.toString(),
                        details.invoice_id
                    )
                }
            }
        }

        requireActivity().layout_sharePdf.setOnClickListener {
            if (!pdfUrl.equals("")) {

                sharePdf(pdfUrl, lastWord)
            }
        }

        btn_submit.setOnClickListener {
            if (!pdfUrl.equals("")) {
                GlobalScope.launch(Dispatchers.Main) {
                    Loader.showLoader(requireContext())
                    detailInvoiceViewModel.sendInvoiceMail(
                        prefs.user_id.toString(),
                        prefs.token,
                        prefs.user_type.toString(),
                        details.invoice_id
                    )
                }
            } else {
                Toast.makeText(requireContext(), "PDF Not Found", Toast.LENGTH_SHORT).show()
            }
        }


    }



    private fun shareInvoice(pdfUrl: String, lastWord: String) {

        try {
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "EmaidCrew_${lastWord}.pdf"
            )
            if (file.exists()) {
                try {


                    val uri =
                        if (Build.VERSION.SDK_INT < 24) Uri.fromFile(file) else Uri.parse(file.path)
                    val intent = Intent(Intent.ACTION_SEND)
                    intent.type = "application/pdf"
                    intent.putExtra(Intent.EXTRA_SUBJECT, "EmaidCrew")
                    intent.putExtra(Intent.EXTRA_STREAM, uri)
                    intent.putExtra(Intent.EXTRA_EMAIL, "")
//                startActivity(Intent.createChooser(intent,"Choose One"))
//                intent.data = Uri.parse("mailto:")
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                    requireActivity().finish()
                } catch (e: Exception) {
                }
            } else {
                Toast.makeText(requireContext(), "Invoice Not Downloaded...", Toast.LENGTH_SHORT)
                    .show()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun sharePdf(pdfUrl: String, lastWord: String) {
        try {
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "EmaidCrew_${lastWord}.pdf"
            )
            if (file.exists()) {
                try {


                    var uri: Uri? = null
                    uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        FileProvider.getUriForFile(
                            requireContext(),
                            requireActivity().packageName + ".provider",
                            file
                        )
                    } else {
                        if (Build.VERSION.SDK_INT < 24) Uri.fromFile(file) else Uri.parse(file.path)
                    }


                    val intent = Intent(Intent.ACTION_SEND)

                    intent.type = "application/pdf"
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Emaid crew")
                    intent.putExtra(Intent.EXTRA_STREAM, uri)

                    // intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("email@example.com"))
                    /*intent.putExtra(Intent.EXTRA_TEXT, "Dear ztestcustomer,\n" +
                            "Thank you for your recent business with us. Please find attached a detailed copy of the invoice INV-2022-2574 for House Cleaning Service done on 14/02/2022.\n" +
                            "\n" +
                            "If you have any questions or concerns please contact us on info@crystalblu.ae or 8002258. ")
*/



                    val resInfoList = requireActivity().packageManager.queryIntentActivities(
                        intent,
                        PackageManager.MATCH_DEFAULT_ONLY
                    )
                    for (resolveInfo in resInfoList) {
                        requireActivity().grantUriPermission(
                            requireActivity().packageName,
                            uri,
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
                        )
                    }



                    intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    intent.flags = Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                    startActivity(Intent.createChooser(intent, "Choose One"))

                } catch (e: Exception) {
                }
            } else {
//                val request = DownloadManager.Request(Uri.parse(pdfUrl))
//                val title = "EmaidCrew_$lastWord"
//                request.setTitle(title)
//                if (readAndWriteExternalStorage(context)) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        request.allowScanningByMediaScanner()
////                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
//                    }
//                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "$title.pdf")
//                    val downloadManager =
//                        requireActivity().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
//                    request.setMimeType("application/pdf")
//                    request.allowScanningByMediaScanner()
//                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
//                    downloadManager?.enqueue(request)
//
//                }
                GlobalScope.launch(Dispatchers.Main) {
                    Loader.showLoader(requireContext())
                    detailInvoiceViewModel.needPdfUrl(
                        prefs.user_id.toString(),
                        prefs.token,
                        details.invoice_id,
                        prefs.user_type.toString()
                    )
                }
                Toast.makeText(requireContext(), "Please Wait...", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun downloadPdf(url: String, titlepdf: String) {
        val request = DownloadManager.Request(Uri.parse(url))
        val title = "EmaidCrew_$titlepdf"
        request.setTitle(title)
        if (readAndWriteExternalStorage(context)) {

            try {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner()
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                }
                request.setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_DOWNLOADS,
                    "$title.pdf"
                )
                val downloadManager =
                    requireActivity().getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
                request.setMimeType("application/pdf")
                request.allowScanningByMediaScanner()
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE or DownloadManager.Request.NETWORK_WIFI)
                downloadManager?.enqueue(request)
            } catch (e: Exception) {
            }
        }
//        sharePdf(pdfUrl, lastWord)
    }

    fun readAndWriteExternalStorage(context: Context?): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                requireContext(),
                READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireContext(), WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                (context as Activity?)!!,
                arrayOf(
                    READ_EXTERNAL_STORAGE,
                    WRITE_EXTERNAL_STORAGE
                ),
                1
            )
            false
        } else {
            true
        }
    }

    companion object {
        var pdfUrl = ""
        var lastWord = ""
    }


}