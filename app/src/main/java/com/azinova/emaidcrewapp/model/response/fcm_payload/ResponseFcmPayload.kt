package com.azinova.emaidcrewapp.model.response.fcm_payload

import com.google.gson.annotations.SerializedName

data class ResponseFcmPayload(

    @field:SerializedName("pushid")
    val pushid: Int? = null,

    @field:SerializedName("isfeedback")
    val isfeedback: Boolean

    )
