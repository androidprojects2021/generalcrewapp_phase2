package com.azinova.emaidcrewapp.ui.activity.pdfviewer

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.webkit.*
import android.widget.Toast
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.ui.activity.detail.BookingDetailsActivity
import java.io.File
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

class PdfViewerActivity : AppCompatActivity() {

    lateinit var webView: WebView


    companion object {
        var url = " "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_viewer)

        Log.e("Test", "Url: $url")

        webView = findViewById(R.id.webView)
        webView.settings.builtInZoomControls = true
        webView.settings.allowFileAccessFromFileURLs = true
        webView.settings.allowUniversalAccessFromFileURLs = true
        webView.settings.javaScriptEnabled = true

        Loader.showLoader(this)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                Loader.hideLoader()
            }
        }
        webView.loadUrl("https://docs.google.com/gview?embedded=true&url=$url")

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


}