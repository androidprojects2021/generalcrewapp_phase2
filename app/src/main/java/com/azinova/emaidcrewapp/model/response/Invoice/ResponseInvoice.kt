package com.azinova.emaidcrewapp.model.response.Invoice

import android.telecom.Call
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseInvoice (
    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("details")
    val details: DataDetails? = null
)

data class DataDetails(
    @field:SerializedName("invoice_id")
    val invoice_id: String? = null,

    @field:SerializedName("customer_id")
    val customer_id: String? = null,

    @field:SerializedName("customer_name")
    val customer_name: String? = null,

    @field:SerializedName("customer_address")
    val customer_address: String? = null,

    @field:SerializedName("inv_ref")
    val inv_ref: String? = null,

    @field:SerializedName("issued_date")
    val issued_date: String? = null,

    @field:SerializedName("due_date")
    val due_date: String? = null,

    @field:SerializedName("cust_trn")
    val cust_trn: String? = null,

    @field:SerializedName("total_amount")
    val total_amount: String? = null,

    @field:SerializedName("vat_amount")
    val vat_amount: String? = null,

    @field:SerializedName("vat_percent")
    val vat_percent: Int? = null,

    @field:SerializedName("total_net_amount")
    val total_net_amount: String? = null,

    @field:SerializedName("signature_status")
    val signature_status: String? = null,

    @field:SerializedName("pdf_url")
    val pdf_url: String? = null,

    @field:SerializedName("line_items")
    val line_items: List<Line_Items?>? = null,
) : Serializable

data class Line_Items(
    @field:SerializedName("service_date")
    val service_date: String? = null,

    @field:SerializedName("service_name")
    val service_name: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("hrs")
    val hrs: Double? = null,

    @field:SerializedName("maid_id")
    val maid_id: String? = null,

    @field:SerializedName("maid_name")
    val maid_name: String? = null,

    @field:SerializedName("from_time")
    val from_time: String? = null,

    @field:SerializedName("to_time")
    val to_time: String? = null,

    @field:SerializedName("line_amount")
    val line_amount: String? = null
): Serializable
