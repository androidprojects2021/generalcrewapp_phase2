package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.model.response.PdfUrl.ResponsePdfUrl
import com.azinova.emaidcrewapp.model.response.Signature.ResponseSignature
import com.azinova.emaidcrewapp.network.ApiClient
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SignatureViewModel: ViewModel() {

    private val _uploadSignature = MutableLiveData<ResponseSignature?>()
    val uploadSignature: LiveData<ResponseSignature?>
        get() = _uploadSignature

    private val _pdfUrl = MutableLiveData<ResponsePdfUrl?>()
    val pdfUrl: LiveData<ResponsePdfUrl?>
        get() = _pdfUrl

    suspend fun uploadSign(multipartBodyImage: MultipartBody.Part?, user_id: RequestBody, token: RequestBody, user_type: RequestBody, invoiceId: RequestBody) {

        try {
            val response = ApiClient.getRetrofit().uploadSignature(multipartBodyImage,user_id,token,invoiceId,user_type)

            if (response.status.equals("success")){
                _uploadSignature.value = response
            }else{
                _uploadSignature.value = response
            }
        }catch (e: Exception){

            _uploadSignature.value = ResponseSignature(status = "Error", message = "Something Went Wrong")
        }
    }

    suspend fun needPdfUrl(user_id: String, token: String?, invoiceId: String?, user_type: String) {

        try {
            val response = ApiClient.getRetrofit().getPdfUrl(user_id,token!!,invoiceId!!,user_type)

            if (response.status.equals("success")){
                _pdfUrl.value = response
            }else{
                _pdfUrl.value = response
            }
        }catch (e:Exception){
            _pdfUrl.value = ResponsePdfUrl(status = "Error")
        }

    }
}