package com.azinova.emaidcrewapp.model.response.send_mail

import com.google.gson.annotations.SerializedName

data class ResponseSendMail(

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
