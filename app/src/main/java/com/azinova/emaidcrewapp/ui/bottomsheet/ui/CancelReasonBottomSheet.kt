package com.azinova.emaidcrewapp.ui.bottomsheet.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.model.response.cancel_reasons.Reasons
import com.azinova.emaidcrewapp.ui.activity.detail.BookingDetailsActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.ReasonSelectAdapter
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.layout_cancel_reasons.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


class CancelReasonBottomSheet : BottomSheetDialogFragment() {

    var bookingId: String? = ""
    var note: String? = ""

    var reason_id: String? = ""
    var cancelreason: String? = ""

    val prefs = SharedPref
    var database: EmaidDatabase? = null

    var reasonSelectAdapter: ReasonSelectAdapter? = null


    private lateinit var cancelReasonViewModel: CancelReasonViewModel

    private lateinit var parent: BookingDetailsActivity

    var booking_id_list: ArrayList<String>? = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.layout_cancel_reasons, container, false)

        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return dialog

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent = activity as BookingDetailsActivity
        cancelReasonViewModel = ViewModelProvider(this).get(CancelReasonViewModel::class.java)
        observeList()
        onClicked()

        val bundle = this.arguments
        if (bundle != null) {
            bookingId = bundle.getString("BookingId")
            note = bundle.getString("notes")
            booking_id_list =
                bundle.getSerializable("Booking_id_list") as ArrayList<String>?
        }
        Log.d("Serializable", "onViewCreated: "+booking_id_list)
        if (requireContext().isConnectedToNetwork()) {

            Loader.showLoader(requireContext())
            GlobalScope.launch(Dispatchers.Main) {
                cancelReasonViewModel.getCancelReasons(
                    prefs.user_id.toString(),
                    prefs.token,
                    prefs.user_type
                )
            }
        } else {
            Toast.makeText(requireContext(), "Please connect to internet", Toast.LENGTH_SHORT)
                .show()

        }

    }

    private fun onClicked() {
        btn_cancel.setOnClickListener {

            @SuppressLint("SimpleDateFormat")
            val sdf1 = SimpleDateFormat("yyyy/MM/dd")
            val currentDate = sdf1.format(Date())

            if (requireContext().isConnectedToNetwork()) {
                Loader.showLoader(requireContext())
                GlobalScope.launch(Dispatchers.Main) {
                    cancelReasonViewModel.cancelservice(
                        requireContext(), prefs.user_id, prefs.user_type, currentDate,
                        prefs.token, bookingId, "3", note, reason_id, cancelreason,booking_id_list
                    )
                    Log.d("CANCEL 1", "onClickedCancel: "+prefs.user_id+ prefs.user_type+ currentDate+
                        prefs.token+ bookingId+ "3"+ note+ reason_id+ cancelreason+booking_id_list)
                }
            } else {
                Toast.makeText(requireContext(), "Please connect to internet", Toast.LENGTH_SHORT)
                    .show()
            }


        }
        btn_dont_cancel.setOnClickListener {
            this.dismiss()
        }

    }

    private fun observeList() {
        cancelReasonViewModel.cancelReasonsList.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                Loader.hideLoader()
                if (it.status.equals("success")) {

                    Log.d("REASON LIST", "observeList: " + it)
                    if (it.reasons?.size!! > 0) {

                        rv_reasonlist.visibility = View.VISIBLE
                        txt_noreason.visibility = View.GONE

                        reasonSelectAdapter = ReasonSelectAdapter(
                            requireContext(),
                            it.reasons
                        ) { reason: Reasons?, i: Int ->
                            Log.d(
                                "Selected Reason",
                                "observeList: " + reason?.reason_id + ", " + reason?.reason_name
                            )
                            reason_id = reason?.reason_id
                            cancelreason = reason?.reason_name
                        }
                        val lm = LinearLayoutManager(requireContext())
                        lm.orientation = LinearLayoutManager.VERTICAL
                        rv_reasonlist.setLayoutManager(lm)
                        rv_reasonlist.setAdapter(reasonSelectAdapter)

                    } else {
                        rv_reasonlist.visibility = View.GONE
                        txt_noreason.visibility = View.VISIBLE
                    }

                } else if (it.status.equals("token_error")) {
                    Loader.hideLoader()
                    if (!it?.message.isNullOrEmpty()) {
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                            .show()
                    }

                    clearLogin()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()
                    if (!it?.message.isNullOrEmpty()) {
                        Toast.makeText(
                            requireContext(),
                            it.message,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                }


            })



        cancelReasonViewModel.cancelServiceStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                Loader.hideLoader()
                Log.d("Cancel", "observe Cancelservice: ")

                if (it.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    requireActivity().finish()
                } else if (it.status.equals("token_error")) {
                    Loader.hideLoader()

                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                        .show()




                    clearLogin()
                     parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()

                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()


                }

            })

    }

    private fun clearLogin() {
        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}