package com.azinova.emaidcrewapp.ui.fragment.ui.notification

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.model.response.notification.NotificationDetailsItem
import com.azinova.emaidcrewapp.model.response.notification.NotificationListItem
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_notification.*
import java.text.SimpleDateFormat
import java.util.*


class NotificationFragment : Fragment() {
    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    private lateinit var parent: MainActivity

    private lateinit var notificationViewModel: NotificationViewModel
    private lateinit var rv_notificationlist: RecyclerView
    lateinit var notificationAdapter: NotificationAdapter

    var currentdate: String = " "

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        notificationViewModel = ViewModelProvider(this).get(NotificationViewModel::class.java)

        parent = activity as MainActivity

        rv_notificationlist = view.findViewById(R.id.rv_notificationlist)


        setUpNotificationlist()
        observedIn()

    }

    private fun observedIn() {

        notificationViewModel.notificationlistStatus.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { notificationlistStatus ->

                if (notificationlistStatus.status.equals("success")) {
                    Loader.hideLoader()


                    if (notificationlistStatus.notificationDetails?.size != 0) {


                        for (datas in notificationlistStatus.notificationDetails!! as List<NotificationDetailsItem>){
                            for (data1 in datas.notificationList as List<NotificationListItem>){
                               setNotificationView(data1)
                            }
                        }


                        Log.d("notificationlistStatus", "observedIn: "+notificationlistStatus.notificationDetails)
                        rv_notificationlist.visibility = View.VISIBLE
                        l_nonotificationList.visibility = View.GONE

                        notificationAdapter = NotificationAdapter(
                            requireContext(),
                            notificationlistStatus.notificationDetails
                        )
                        { notificationListItem: NotificationListItem?, i: Int, list: List<NotificationListItem?>? ->
                            showNotificationPopUp(notificationListItem)
                        }
                        val lmList = LinearLayoutManager(context)
                        lmList.orientation = LinearLayoutManager.VERTICAL
                        rv_notificationlist.setLayoutManager(lmList)
                        rv_notificationlist.setAdapter(notificationAdapter)
                        // }

                    } else {
                        rv_notificationlist.visibility = View.GONE
                        l_nonotificationList.visibility = View.VISIBLE
                    }


                } else if (notificationlistStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        notificationlistStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()


                    clearLogin()
                    parent.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(requireContext(), LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    activity?.finish()

                } else {
                    Loader.hideLoader()
                    Toast.makeText(
                        requireContext(),
                        notificationlistStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                }


            })

        notificationViewModel.updateNotification.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it.status.equals("success")){
               setUpNotificationlist()
                Log.d("Success", "onNotification: " + "UpdateNotification")
            }else if (it.status.equals("token_error")){
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                    .show()


                clearLogin()
                 parent.stopServiceTracker()
                database?.clearAllTables()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()

                Log.d("Fail", "onNotification: " + "Notification token ")
            }else{
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                    .show()
                Log.d("Fail", "onNotification: " + "Notification Fail ")
            }
        })

    }

    private fun setNotificationView(data1: NotificationListItem) {

        when(prefs.user_type){
            "0" -> {
                if (data1.maid_read_status.equals("0")){

                    requireActivity().img_notification.setImageResource(R.drawable.unread_not)
                }else{
                    requireActivity().img_notification.setImageResource(R.drawable.not_new)
                }
            }
            "1" -> {
                if (data1.read_status.equals("0")){

                    requireActivity().img_notification.setImageResource(R.drawable.unread_not)
                }else{
                    requireActivity().img_notification.setImageResource(R.drawable.not_new)
                }
            }
        }

    }

    private fun showNotificationPopUp(notificationListItem: NotificationListItem?) {
        val builder = AlertDialog.Builder(requireContext())

        val _view: View =
            LayoutInflater.from(requireContext())
                .inflate(R.layout.customerdialog_notiy, null)
        builder.setView(_view)

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(true)

        val textViewHeading: TextView = _view.findViewById(R.id.customeerror_heading)
        val textViewContent: TextView = _view.findViewById(R.id.customeerror_message)
        val shiftTextView: TextView = _view.findViewById(R.id.shift)
        val areaTextView: TextView = _view.findViewById(R.id.area)



        textViewHeading.text = notificationListItem?.title
        textViewContent.text = notificationListItem?.maid
        shiftTextView.text = notificationListItem?.customer
        areaTextView.text = notificationListItem?.booking_time




        val customeerror_btn: TextView = _view.findViewById(R.id.customeerror_btn)

        customeerror_btn.setOnClickListener {
            if (requireContext().isConnectedToNetwork()){


                notificationViewModel.updateNotification(
                    prefs.user_id.toString(),
                    prefs.token!!,
                    prefs.user_type.toString(),
                    "1",
                    notificationListItem?.pushid.toString())

            }
            alertDialog.dismiss()

        }
        alertDialog.show()
    }

    private fun setUpNotificationlist() {


        val sdf = SimpleDateFormat("yyyy/MM/dd")
        currentdate = sdf.format(Date())

        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {
            getNotifications(
                    prefs.user_id.toString(),
                    currentdate,
                    prefs.user_type.toString(),
                    prefs.token
            )
        } else {
            Loader.hideLoader()
            Snackbar.make(outernotifications, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }


    }

    private fun getNotifications(user_id: String, currentdate: String, user_type: String, token: String?) {
        notificationViewModel.getNotificationList(user_id, currentdate, user_type, token)

    }

    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }
}