package com.azinova.emaidcrewapp.network

import androidx.annotation.NonNull
import com.azinova.emaidcrewapp.model.response.Invoice.ResponseInvoice
import com.azinova.emaidcrewapp.model.response.PdfUrl.ResponsePdfUrl
import com.azinova.emaidcrewapp.model.response.Signature.ResponseSignature
import com.azinova.emaidcrewapp.model.response.bookings.ResponseBookings
import com.azinova.emaidcrewapp.model.response.cancel.ResponseCancelService
import com.azinova.emaidcrewapp.model.response.cancel_reasons.ResponseCancelReasons
import com.azinova.emaidcrewapp.model.response.company_code.ResponseCompanyCode
import com.azinova.emaidcrewapp.model.response.edit_profile.ResponseEditProfile
import com.azinova.emaidcrewapp.model.response.finish.ResponseFinishService
import com.azinova.emaidcrewapp.model.response.key.ResponseAddRemoveKey
import com.azinova.emaidcrewapp.model.response.location.ResponseLocationUpdate
import com.azinova.emaidcrewapp.model.response.login.ResponseLogin
import com.azinova.emaidcrewapp.model.response.logout.ResponseLogout
import com.azinova.emaidcrewapp.model.response.maidattendance.ResponseMaidAttendance
import com.azinova.emaidcrewapp.model.response.maiddetails.ResponseMaidBookingDetailsList
import com.azinova.emaidcrewapp.model.response.maiddetails.ResponseMaidDetailsList
import com.azinova.emaidcrewapp.model.response.maidstatus.ResponseMaidStatusChange
import com.azinova.emaidcrewapp.model.response.notification.ResponseNotification
import com.azinova.emaidcrewapp.model.response.payment.ResponsePayment
import com.azinova.emaidcrewapp.model.response.report.ResponseScheduleReport
import com.azinova.emaidcrewapp.model.response.send_mail.ResponseSendMail
import com.azinova.emaidcrewapp.model.response.start.ResponseStartService
import com.azinova.emaidcrewapp.model.response.transfer.ResponseBookingTransfer
import com.azinova.emaidcrewapp.model.response.transfer.ResponseTransferlist
import com.azinova.emaidcrewapp.model.response.update_notification.ResponseUpdateNotification
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import java.util.ArrayList


interface ApiInterface {

    @FormUrlEncoded
    @POST("appLogin")
    suspend fun Login(
        @Field("username") @NonNull username: String,
        @Field("userpassword") @NonNull userpassword: String,
        @Field("maid_driver_status") @NonNull maid_driver_status: String,
        @Field("date") @NonNull date: String,
        @Field("device_regid") @NonNull device_regid: String
    ): ResponseLogin

    @FormUrlEncoded
    @POST("getbookings_schedule")
    suspend fun getBookings(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseBookings

    @FormUrlEncoded
    @POST("payment_list")
    suspend fun getPaymentlist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_status") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponsePayment

    @FormUrlEncoded
    @POST("maid_details_list")
    suspend fun getMaidDetailslist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_status") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseMaidDetailsList

    @FormUrlEncoded
    @POST("get_transfer_list")
    suspend fun getTransferlist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseTransferlist

    @FormUrlEncoded
    @POST("transfer_maid")
    suspend fun getBookingsTransfer(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id[]") @NonNull booking_id: ArrayList<String>,
        @Field("zone_id") @NonNull zone_id: String,
        @Field("transfer_all") @NonNull transfer_type: String,
        @Field("is_driver_based") @NonNull is_driver_based: String,
        @Field("maid_id") @NonNull maidId: String?
    ): ResponseBookingTransfer

    // START SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getStartService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id[]") @NonNull booking_id: ArrayList<String>,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String
    ): ResponseStartService

    // FINISH SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getFinishService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id[]") @NonNull booking_id: ArrayList<String>,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String
    ): ResponseFinishService

    // CANCEL SERVICE
    @FormUrlEncoded
    @POST("service_start_stop")
    suspend fun getCancelService(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id[]") @NonNull booking_id: ArrayList<String>,
        @Field("status") @NonNull status: String,
        @Field("payment") @NonNull payment: String,
        @Field("amount") @NonNull amount: String,
        @Field("receipt_no") @NonNull receipt_no: String,
        @Field("method") @NonNull method: String,
        @Field("outstanding_amount") @NonNull outstanding_amount: String,
        @Field("notes") @NonNull notes: String,
        @Field("reason_id") @NonNull reason_id: String,
        @Field("reason_name") @NonNull reason_name: String
    ): ResponseCancelService


    @FormUrlEncoded
    @POST("maid_in_out")
    suspend fun getMaidStatusChange(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("status") @NonNull status: String,
        @Field("maid_id") @NonNull maid_id: String,
        @Field("sl_no") @NonNull slNo: String
    ): ResponseMaidStatusChange

    @FormUrlEncoded
    @POST("maid_details")
    suspend fun getMaidbookingsDetailslist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("maid_id") @NonNull maid_id: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseMaidBookingDetailsList


    @FormUrlEncoded
    @POST("maid_attendance_list")
    suspend fun getMaidAttendancelist(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseMaidAttendance

    @FormUrlEncoded
    @POST("schedule_reports")
    suspend fun getScheduleReport(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String
    ): ResponseScheduleReport

    @FormUrlEncoded
    @POST("profile_edit")
    suspend fun getEditProfile(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("name") @NonNull name: String,
        @Field("phone") @NonNull phone: String,
        @Field("email") @NonNull email: String
    ): ResponseEditProfile

    @FormUrlEncoded
    @POST("update_location")
   suspend fun getLocationUpdate(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("latitude") @NonNull latitude: String,
        @Field("longitude") @NonNull longitude: String
    ): ResponseLocationUpdate

    @FormUrlEncoded
    @POST("get_notifications")
    suspend fun getnotificationList(
            @Field("id") @NonNull id: String,
            @Field("maid_driver_stat") @NonNull type: String,
            @Field("date") @NonNull date: String,
            @Field("token") @NonNull token: String
    ): ResponseNotification

    @FormUrlEncoded
    @POST("logout")
    suspend fun hitLogout(
            @Field("id") @NonNull id: String,
            @Field("maid_driver_stat") @NonNull type: String,
            @Field("token") @NonNull token: String
    ): ResponseLogout

    @FormUrlEncoded
    @POST("get_driver_list")
    suspend fun getDriverList(
        @Field("id") @NonNull id: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("date") @NonNull date: String,
        @Field("token") @NonNull token: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseTransferlist

    @FormUrlEncoded
     //  @POST("company.php") //DEMO
     @POST("company_live_new.php") // New LIVE updated on 05-mar-2022 by Reema
       /* eg :https://booking.emaid.info/crewapp/company_live_new.php*/
    suspend fun companyCode(
        @Field("company_code") @NonNull companyCode: String): ResponseCompanyCode

    @FormUrlEncoded
    @POST("create_invoice")
    suspend fun getInvoice(
        @Field("id") @NonNull id: String,
        @Field("token") @NonNull token: String,
        @Field("service_date") @NonNull service_date: String,
        @Field("maid_driver_stat") @NonNull type: String,
        @Field("booking_id") @NonNull booking_id: String
    ): ResponseInvoice

    @FormUrlEncoded
    @POST("invoice_pdf_generate")
    suspend fun getPdfUrl(
            @Field("id") @NonNull id: String,
            @Field("token") @NonNull token: String,
            @Field("invoice_id") @NonNull invoice_id: String,
            @Field("maid_driver_stat") @NonNull type: String
    ): ResponsePdfUrl

    @Multipart
    @POST("invoice_signature_update")
    suspend fun uploadSignature(
            @Part signature: MultipartBody.Part?,
            @Part ("id") @NonNull id: RequestBody,
            @Part ("token") @NonNull token: RequestBody,
            @Part ("invoice_id") @NonNull invoice_id: RequestBody,
            @Part ("maid_driver_stat") @NonNull type: RequestBody
    ): ResponseSignature

    @FormUrlEncoded
    @POST("list_cancel_reasons")
    suspend fun cancelReasons(
        @Field("id") @NonNull id: String,
        @Field("token") @NonNull token: String,
        @Field ("maid_driver_stat") @NonNull type: String
    ): ResponseCancelReasons

    @FormUrlEncoded
    @POST("update_notification_status")
    suspend fun updateNotifications(
        @Field("id") @NonNull id: String,
        @Field("token") @NonNull token: String,
        @Field ("maid_driver_stat") @NonNull type: String,
        @Field("read_status") @NonNull read_status:String,
//        @Field("maid_read_status") @NonNull maid_read_status:String,
        @Field("pushid") @NonNull pushid:String
    ): ResponseUpdateNotification

    @FormUrlEncoded
    @POST("sent_invoice_email")
    suspend fun sendMail(
        @Field("id") @NonNull id: String,
        @Field("token") @NonNull token: String,
        @Field ("maid_driver_stat") @NonNull type: String,
        @Field("invoice_id") @NonNull invoice_id: String
    ): ResponseSendMail

// Key add/remove
    @FormUrlEncoded
    @POST("add_remove_key")
    suspend fun getAddRemoveKey(
        @Field("id") @NonNull id: String,
        @Field ("maid_driver_stat") @NonNull type: String,
        @Field("token") @NonNull token: String,
        @Field("add_remove") @NonNull add_remove: String,
        @Field("customer_id") @NonNull customer_id: String
    ): ResponseAddRemoveKey
}