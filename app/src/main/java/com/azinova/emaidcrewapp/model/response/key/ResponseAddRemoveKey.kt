package com.azinova.emaidcrewapp.model.response.key

import com.google.gson.annotations.SerializedName

data class ResponseAddRemoveKey(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	var key_statuss: String? = null
)
