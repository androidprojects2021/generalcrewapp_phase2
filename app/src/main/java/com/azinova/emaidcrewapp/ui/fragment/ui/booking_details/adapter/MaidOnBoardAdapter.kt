package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.adapter

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.net.toUri
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.database.tables.MaidTable
import com.bumptech.glide.Glide
import java.io.File

class MaidOnBoardAdapter(val context: Context, val maidlistStatus: List<MaidTable?>?, val onItemClicked: (MaidTable?, Int, List<MaidTable?>?) -> Unit) : RecyclerView.Adapter<MaidOnBoardAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.card_maiddetails,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_name.text = maidlistStatus!![position]?.maid_name


        if (maidlistStatus[position]?.maid_image!!.isNotEmpty()){



            val savedUri: Uri? =
                maidlistStatus?.get(position)?.maid_image?.toUri()
            Glide.with(context)
                .load( File(savedUri?.getPath()))
                .placeholder(R.drawable.nomaidimage)
                .error(R.drawable.nomaidimage)
                .into(holder.img_maid)
        }


        if (maidlistStatus.get(position)?.maid_attandence == "0" || maidlistStatus.get(position)?.maid_attandence == "2" ){
            holder.img_maid_status.setImageResource(R.drawable.ic_circlered)
        }else  if (maidlistStatus.get(position)?.maid_attandence == "1"){
            holder.img_maid_status.setImageResource(R.drawable.ic_circle_green)
        }


        holder.cv_maidonboard.setOnClickListener {
            onItemClicked.invoke(maidlistStatus.get(position),position,maidlistStatus)

        }
    }

    override fun getItemCount(): Int {
        return maidlistStatus!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cv_maidonboard = itemView.findViewById<ConstraintLayout>(R.id.cv_maidonboard)
        var tv_name = itemView.findViewById<TextView>(R.id.tv_name)
        var img_maid_status = itemView.findViewById<ImageView>(R.id.img_maid_status)
        var img_maid = itemView.findViewById<ImageView>(R.id.circleImageView)


    }


}