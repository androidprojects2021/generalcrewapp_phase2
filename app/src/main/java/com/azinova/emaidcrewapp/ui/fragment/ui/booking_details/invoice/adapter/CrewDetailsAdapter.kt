package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.Invoice.Line_Items
import kotlinx.android.synthetic.main.layout_crew_details.view.*
import kotlin.math.log

class CrewDetailsAdapter(requireContext: Context, val lineItems: List<Line_Items?>) : RecyclerView.Adapter<CrewDetailsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.layout_crew_details,parent,false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.e("text", "$position" )
        holder.itemView.txt_sl_no.text = position.plus(1).toString()
        holder.itemView.txt_crew_name.text = lineItems[position]?.maid_name
        holder.itemView.txt_time.text = lineItems[position]?.from_time+ " - "+lineItems[position]?.to_time
    }

    override fun getItemCount(): Int = lineItems.size
}