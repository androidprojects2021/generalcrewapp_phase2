package com.azinova.emaidcrewapp.model.response.cancel_reasons

import com.google.gson.annotations.SerializedName

data class ResponseCancelReasons(

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("reasons")
    val reasons: List<Reasons?>? = null

)

data class Reasons(

    @field:SerializedName("reason_id")
    val reason_id: String? = null,

    @field:SerializedName("reason_name")
    val reason_name: String? = null

)
