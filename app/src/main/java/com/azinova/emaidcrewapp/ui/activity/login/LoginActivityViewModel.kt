package com.azinova.emaidcrewapp.ui.activity.login

import android.app.Application
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.model.response.bookings.ResponseBookings
import com.azinova.emaidcrewapp.model.response.bookings.ScheduleItem
import com.azinova.emaidcrewapp.model.response.login.ResponseLogin
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.gson.Gson
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.net.URL
import java.util.*

class LoginActivityViewModel(application: Application) : AndroidViewModel(application) {


    private val prefs = SharedPref
    private val _loginStatus = MutableLiveData<String>()
    val loginStatus: LiveData<String>
        get() = _loginStatus


    init {
        application.let { prefs.with(it) }
    }


    fun doLogin(
            context: Context,
            username: String,
            password: String,
            type: String,
            currentDate: String,
            firebasetoken: String?
    ) {

        GlobalScope.launch(Dispatchers.Main) {
            try {

                val response = ApiClient.getRetrofit().Login(username, password, type, currentDate, firebasetoken.toString())

                if (response.status.equals("success")) {

                    prefs.user_details = Gson().toJson(response.userdetails)
                    prefs.user_id = response.userdetails?.id!!
                    prefs.user_name = response.userdetails.name
                    prefs.user_type = type
                    prefs.token = response.userdetails.token
                    prefs.phone = response.userdetails.phone
                    prefs.email = response.userdetails.email
                    prefs.user_zone = response.userdetails.zone_name.toString()
                    Log.d("Zone", "doLogin: "+response.userdetails.zone_name.toString())
                    Log.d("login ", "doLogin: "+response)

                    if (response.userdetails.image!!.isNotEmpty()) {
                        val urlImage: URL = URL(response.userdetails.image)

                        val result: Deferred<Bitmap?> = GlobalScope.async {
                            urlImage.toBitmap()
                        }

                        val bitmap: Bitmap? = result.await()

                        bitmap?.apply {
                            val savedUri: Uri? = saveToInternalStorage(context)
                            prefs.image = savedUri.toString()
                        }
                    } else {
                        prefs.image = ""
                    }


                    fetDatFromServer(context, currentDate)


                } else {
                    _loginStatus.value = response.message!!
                }


            } catch (e: Exception) {
                ResponseLogin(message = "Something went wrong", status = "error")
                _loginStatus.value = "Something went wrong"

            }
        }

    }

    suspend fun fetDatFromServer(context: Context, currentDate: String) {

        try {

            val response = ApiClient.getRetrofit().getBookings(prefs.user_id.toString(), prefs.user_type.toString(), currentDate, prefs.token.toString())

            Log.d("---------------", "getBookings: " + response)

            if (response.status.equals("success")) {

                Log.d("CHECK #1", "fetDatFromServer: "+response.schedule)

                insertToDb(context, response.schedule)
                _loginStatus.value = response.status!!
            }  else {
                ResponseBookings(message = "Bookings not updated, please synchronise", status = "error")
                _loginStatus.value = "Something went wrong"

            }
        } catch (e: Exception) {
            Log.d("CHECK #2", "fetDatFromServer: "+e.message)


            ResponseBookings(message = "Something went wrong", status = "error")
            _loginStatus.value = "Something went wrong"
        }

    }

    private suspend fun insertToDb(context: Context, schedule: List<ScheduleItem?>?) {
        BookingsRepository.insertData(context, schedule)
    }

    // --------------------------Image Download -----------------
    fun URL.toBitmap(): Bitmap? {
        return try {
            BitmapFactory.decodeStream(openStream())
        } catch (e: IOException) {
            null
        }
    }

    fun Bitmap.saveToInternalStorage(context: Context): Uri? {
        val wrapper = ContextWrapper(context)

        var file = wrapper.getDir("images", Context.MODE_PRIVATE)

        file = File(file, "${UUID.randomUUID()}.jpg")

        return try {
            val stream: OutputStream = FileOutputStream(file)

            compress(Bitmap.CompressFormat.JPEG, 100, stream)

            stream.flush()

            stream.close()

            Uri.parse(file.absolutePath)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }
}