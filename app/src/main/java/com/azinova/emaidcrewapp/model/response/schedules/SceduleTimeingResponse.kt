package com.azinova.emaidcrewapp.model.response.schedules



data class SceduleTimeingResponse(

        var scheduleDate: String? = null,
        var scheduleTimeing: String? = null,
        var noOfMaids: Int? = null,
        var schedulebookings: List<ScheduleResponse?>? = null
)



