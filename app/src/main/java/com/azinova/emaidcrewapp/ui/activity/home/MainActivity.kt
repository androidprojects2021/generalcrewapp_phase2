package com.azinova.emaidcrewapp.ui.activity.home

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.ExitAppDialog
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.model.response.notification.NotificationDetailsItem
import com.azinova.emaidcrewapp.model.response.notification.NotificationListItem
import com.azinova.emaidcrewapp.model.response.notification.ResponseNotification
import com.azinova.emaidcrewapp.service.LocationService
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.PermissionUtils
import com.azinova.emaidcrewapp.utils.SharedPref
import com.azinova.emaidcrewapp.utils.notification.BaseActivity
import com.azinova.emaidcrewapp.utils.notification.Myservices
import com.azinova.emaidcrewapp.utils.notification.NotificationDioalogue
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.toolbar_layout
import kotlinx.android.synthetic.main.activity_main.txt_zone
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_more.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.widget.RelativeLayout
import com.azinova.emaidcrewapp.ui.activity.companycode.CompanyCodeActivity


class MainActivity : BaseActivity(), ExitAppDialog.ExitClick {
    private lateinit var navController: NavController
    private lateinit var bottomNavigationView: BottomNavigationView

    private lateinit var mainActivityViewModel: MainActivityViewModel

    companion object {
        private var REQUEST_LOCATION_CODE = 101
    }

    private val prefs = SharedPref
    var database: EmaidDatabase? = null


    private lateinit var txt_title: TextView
    private lateinit var img_notification: ImageView
    private lateinit var img_calender: ImageView
    private lateinit var img_sync: ImageView


    var newdate: String = " "
    var currentdate: String = " "


    lateinit var remoteConfig : FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))


        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        Log.d("Emaid crew test", "generateNotification: " + intent.extras)
        Log.e("SHOW_OTHER_DATES", "SHOW_OTHER_DATES: ${prefs.show_other_dates.toString()}")

        if (intent.extras != null) {
            if (intent.extras!!.getString("message") != null) {

                val myIntent = Intent(this, Myservices::class.java)
                myIntent.putExtra("message", intent.extras!!.getString("message"))
                myIntent.putExtra("title", intent.extras!!.getString("title"))
                myIntent.putExtra("maid", intent.extras!!.getString("maid"))
                myIntent.putExtra("bookingTime", intent.extras!!.getString("bookingTime"))
                myIntent.putExtra("customer", intent.extras!!.getString("customer"))
                myIntent.putExtra("heading", "Notification")
                startService(myIntent)

            }
        }


        onLocation()

        try {
            if (intent.extras!!.getString("page")
                    .equals("notification")
            ) {
                NotificationDioalogue.synchrinoouserror(
                    this,
                    "Update Error", "Please Update Again"
                )
            }
        } catch (e: Exception) {

        }

        //Getting the Navigation Controller
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

        bottomNavigationView = findViewById(R.id.bottomNavigationView)

        txt_title = findViewById(R.id.txt_title)
        img_notification = findViewById(R.id.img_notification)
        img_calender = findViewById(R.id.img_calender)
        img_sync = findViewById(R.id.img_sync)


        bottomNavigationView.setupWithNavController(navController)





        onDestination()
        onclick()
        onObserve()
    }

    private fun setUpNotificationList() {
        Log.e("NOTIFICATION", "setUpNotificationList: Working" )
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        currentdate = sdf.format(Date())

        if (this.isConnectedToNetwork()) {
            getNotifications(
                prefs.user_id.toString(),
                currentdate,
                prefs.user_type.toString(),
                prefs.token
            )
        } else {
            Snackbar.make(outer_main, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun getNotifications(userId: String, currentdate: String, userType: String, token: String?) {
        mainActivityViewModel.getNotificationList(userId,currentdate,userType,token)
    }


    private fun onLocation() {
        when {
            PermissionUtils.isAccessFineLocationGranted(this) -> {
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        serviceGps()
                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    this,
                    REQUEST_LOCATION_CODE
                )


            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                        }
                        else -> {

                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Location Permission not granted",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            setUpNotificationList()
            firebaseRemoteConfig()
            onLocation()
        }catch (e:Exception){}
    }

    private fun onObserve() {
        mainActivityViewModel.bookingsDetailsSyncStatus.observe(
            this,
            androidx.lifecycle.Observer { bookingsDetailsSyncStatus ->
                if (bookingsDetailsSyncStatus.status.equals("success")) {
                    Loader.hideLoader()
                    Toast.makeText(
                        this,
                        "Updated",
                        Toast.LENGTH_SHORT
                    ).show()

                } else if (bookingsDetailsSyncStatus.status.equals("token_error")) {
                    Loader.hideLoader()
                    Toast.makeText(this, bookingsDetailsSyncStatus.message, Toast.LENGTH_SHORT)
                        .show()


                    clearLogin()
                    this.stopServiceTracker()
                    database?.clearAllTables()

                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    this.finish()

                } else {
                    Loader.hideLoader()
                    Toast.makeText(
                        this,
                        bookingsDetailsSyncStatus.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })

        mainActivityViewModel.notificationlistStatus.observe(
            this,
            androidx.lifecycle.Observer { notificationlistStatus: ResponseNotification ->
                var unread_noti:Boolean = false

                if (notificationlistStatus.status.equals("success")) {
                   // Loader.hideLoader()
                    Log.e("NOTSUccess", "onObserve: maidStauts" )

                    if (notificationlistStatus.notificationDetails?.size != 0) {
                        Log.d("Notificationlist  1", "setNotificationView: "+notificationlistStatus.notificationDetails)

                      for (datas in notificationlistStatus.notificationDetails!! ){
                          for (data1 in datas?.notificationList!!){
                              Log.d("Notificationlist  2", "setNotificationView: "+data1?.read_status)
                                if (data1?.read_status == "0" )
                                {unread_noti = true}
                              //setNotificationView(data1)
                          }
                      }

                        if (unread_noti){
                            img_notification.setImageResource(R.drawable.unread_not)
                        }else{
                            img_notification.setImageResource(R.drawable.not_new)
                        }
                       }

                }
                else if (notificationlistStatus.status.equals("token_error")) {
                   // Loader.hideLoader()
                    Toast.makeText(
                        this,
                        notificationlistStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                    database?.clearAllTables()

                    val intent = Intent(this, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    this.finish()

                } else {
                   // Loader.hideLoader()
                    Toast.makeText(
                        this,
                        notificationlistStatus.message,
                        Toast.LENGTH_SHORT
                    )
                        .show()

                }


            })
    }

    private fun setNotificationView(data1: NotificationListItem) {

        Log.d("Notificationlist  1", "setNotificationView: "+data1)




    }

    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }

    private fun serviceGps() {
        ContextCompat.startForegroundService(this, Intent(this, LocationService::class.java))
        Log.e("serv ", "start")
    }

    private fun onclick() {
        img_notification.setOnClickListener {
            findNavController(R.id.nav_host_fragment).navigate(R.id.navigation_notification)
        }
        img_back.setOnClickListener {
            onBackPressed()
        }
        img_search.setOnClickListener {
            search_view.showSearch()
        }

        @SuppressLint("SimpleDateFormat")
        val sdf = SimpleDateFormat("yyyy/MM/dd")
        val currentDate = sdf.format(Date())

        img_sync?.setOnClickListener {
            Loader.showLoader(this)
            if (this.isConnectedToNetwork()) {
                GlobalScope.launch(Dispatchers.Main) {
                    mainActivityViewModel.fetDatFromServer(applicationContext, currentDate)
                }
            } else {
                Loader.hideLoader()
                Snackbar.make(outermore, "Please connect to internet", Snackbar.LENGTH_LONG).show()
            }

        }

    }

    private fun onDestination() {

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        newdate = sdf.format(Date())

        findNavController(R.id.nav_host_fragment).addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_jobs -> {

                    txt_title.text = "Bookings"
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_filter.visibility = View.VISIBLE
                    img_notification.visibility = View.VISIBLE
                    img_calender.visibility = View.GONE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                    txt_zone.visibility = View.VISIBLE


                }
                R.id.navigation_payment -> {

                    txt_title.text = newdate
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    if (prefs.is_payment_hide.equals("yes")){
                        img_calender.visibility = View.GONE
                    }else {
                        img_calender.visibility = View.VISIBLE
                    }
                    img_notification.visibility = View.VISIBLE
                    img_filter.visibility = View.GONE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                    txt_zone.visibility = View.GONE


                }
                R.id.navigation_report -> {

                    txt_title.text = "Booking Report"
                    toolbar_layout.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    layout_menu.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.VISIBLE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                    txt_zone.visibility = View.GONE


                }
                R.id.navigation_more -> {

                    txt_title.text = ""
                    toolbar_layout.visibility = View.VISIBLE
                    layout_menu.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.INVISIBLE
                    img_sync.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                    txt_zone.visibility = View.GONE

                }
                R.id.navigation_editprofile -> {
                    txt_moretitle.text = "Edit Profile"
                    toolbar_layout.visibility = View.GONE
                    img_search.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE
                    txt_zone.visibility = View.GONE


                }
                R.id.navigation_notification -> {
                    txt_moretitle.text = "Notifications"
                    img_search.visibility = View.GONE
                    toolbar_layout.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE
                    txt_zone.visibility = View.GONE


                }
                R.id.navigation_maidattendence -> {
                    txt_moretitle.text = "Maid Attendance"

                    img_search.visibility = View.VISIBLE
                    toolbar_layout.visibility = View.GONE
                    layout_search.visibility = View.VISIBLE
                    cv_bottomNavigationView.visibility = View.GONE
                    txt_zone.visibility = View.GONE

                }
                else -> {

                    txt_title.text = ""
                    toolbar_layout.visibility = View.VISIBLE
                    layout_search.visibility = View.GONE
                    img_calender.visibility = View.GONE
                    img_filter.visibility = View.GONE
                    img_notification.visibility = View.INVISIBLE
                    img_sync.visibility = View.GONE
                    cv_bottomNavigationView.visibility = View.VISIBLE
                    txt_zone.visibility = View.GONE

                }

            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (search_view.isSearchOpen) {
            search_view.closeSearch()
        } else if (findNavController(R.id.nav_host_fragment).currentDestination?.id == R.id.navigation_jobs) {
            ExitAppDialog.showDialog(this, this)
        } else {
            super.onBackPressed()
        }
    }

    override fun proceedExit() {
        finish()
    }

    fun stopServiceTracker(){
        stopService(Intent(this, LocationService::class.java))
    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
            this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

    private fun firebaseRemoteConfig() {

        remoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 0
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        val map = mutableMapOf<String, Any?>()
        map["state_android"] = "normal"
        map["version_android"] = 0
        remoteConfig.setDefaultsAsync(map)
        fetchConfig()

    }

    private fun fetchConfig() {
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->

                if (task.isSuccessful) {
                    val updated = task.result

                    Log.e("result version", remoteConfig.getString("version_android"))
                    Log.e("result state", remoteConfig.getString("state_android"))

                    try {
                        if(!remoteConfig.getString("version_android").equals("")){

                            if(applicationContext.packageManager.getPackageInfo(
                                    applicationContext.packageName,
                                    0
                                ).versionCode <
                                (remoteConfig.getString("version_android").toInt())){

                                if(remoteConfig.getString("state_android").equals("critical"))

                                { onUpdateNeeded(true) } else {
                                    onUpdateNeeded(false)
                                }

                            }

                        }
                    } catch (e: Exception) {
                    }

                } else {

                }

            }
    }

    private fun onUpdateNeeded(isMandatoryUpdate: Boolean) {
        val dialogBuilder = androidx.appcompat.app.AlertDialog.Builder(this)
            .setTitle("New Update")
            .setCancelable(false)
            .setMessage(if (isMandatoryUpdate) "Please update Emaid Crew application to continue.." else "A new version is found on Play store, please update for better usage.")
            .setPositiveButton("update")
            { dialog, which ->

                openAppOnPlayStore(this, applicationContext.packageName)
            }

        if (!isMandatoryUpdate) {
            dialogBuilder.setNegativeButton("later") { dialog, which ->
                dialog?.dismiss()
            }.create()
        }
        val dialog: androidx.appcompat.app.AlertDialog = dialogBuilder.create()
        dialog.show()
    }

    private fun openAppOnPlayStore(ctx: MainActivity, package_name: String?) {
        var package_name = package_name

        if (package_name == null) {
            package_name = ctx.packageName
        }
        Log.e("test", "package name = " + package_name)

        val uri = Uri.parse("market://details?id=$package_name")
        openURI(ctx, uri, "Play Store not found in your device")
    }

    private fun openURI(ctx: MainActivity, uri: Uri?, error_msg: String) {
        val i = Intent(Intent.ACTION_VIEW, uri)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        if (ctx.packageManager.queryIntentActivities(i, 0).size > 0) {
            ctx.startActivity(i)
        } else if (error_msg != null) {
            Toast.makeText(this, error_msg, Toast.LENGTH_SHORT).show()
        }
    }




}