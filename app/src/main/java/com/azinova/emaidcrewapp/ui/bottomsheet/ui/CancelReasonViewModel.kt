package com.azinova.emaidcrewapp.ui.bottomsheet.ui

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.azinova.emaidcrewapp.database.repository.BookingsRepository
import com.azinova.emaidcrewapp.model.response.cancel.ResponseCancelService
import com.azinova.emaidcrewapp.model.response.cancel_reasons.ResponseCancelReasons
import com.azinova.emaidcrewapp.network.ApiClient
import com.azinova.emaidcrewapp.network.ApiClient.prefs
import java.util.ArrayList


class CancelReasonViewModel: ViewModel() {


    private val _cancelServiceStatus = MutableLiveData<ResponseCancelService>()
    val cancelServiceStatus: LiveData<ResponseCancelService>
        get() = _cancelServiceStatus


    private val _cancelReasonsList = MutableLiveData<ResponseCancelReasons>()
    val cancelReasonsList: LiveData<ResponseCancelReasons>
        get() = _cancelReasonsList



    suspend fun getCancelReasons(user_id: String, token: String?, userType: String?) {
        try {
            val response = ApiClient.getRetrofit().cancelReasons(id = user_id,token = token!!,type = userType!!)
            if (response.status.equals("success")){
                _cancelReasonsList.value = response
            }else{
                _cancelReasonsList.value = response
            }
        }catch (e: Exception){
            _cancelReasonsList.value = ResponseCancelReasons(message = "Something Went Wrong", status = "Error")
        }

    }

    suspend fun cancelservice(
        requireContext: Context,
        userId: Int,
        userType: String?,
        currentDate: String,
        token: String?,
        bookingId: String?,
        status: String,
        note: String?,
        reasonId: String?,
        cancelreason: String?,
        booking_id_list: ArrayList<String>?
    ) {
        try {
            val response = ApiClient.getRetrofit().getCancelService(userId.toString(),userType.toString(),currentDate,token.toString(),
                booking_id_list!!,
                status, "0","", "", "","",note.toString(),reasonId.toString(),cancelreason.toString()
            )
            Log.d("CANCEL 2", "onClickedCancel: "+response)
            if (response.status.equals("success")) {
                Log.d("Cancel service 1", "getCancelService: "+response)

                _cancelServiceStatus.value = response
                updateCancelServiceStatus(
                    requireContext, bookingId.toString(),
                    response.serviceStatus.toString())
            } else {
                _cancelServiceStatus.value = response
            }
        }catch (e:Exception){
            _cancelServiceStatus.value = ResponseCancelService(message = "Something went wrong", status = "error")
        }

    }



    suspend fun updateCancelServiceStatus(context: Context, bookingId: String, serviceStatus: String) {
        BookingsRepository.updatecancelbookingStatus(context, bookingId, serviceStatus)
    }


}