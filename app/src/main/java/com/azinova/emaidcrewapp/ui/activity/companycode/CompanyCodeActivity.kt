package com.azinova.emaidcrewapp.ui.activity.companycode

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_company_code.*

class CompanyCodeActivity : AppCompatActivity() {
    private var prefs = SharedPref
    lateinit var companyCodeViewModel: CompanyCodeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_code)
        prefs.baseurl = "https://booking.emaid.info/crewapp/"

        companyCodeViewModel = ViewModelProvider(this)[CompanyCodeViewModel::class.java]

        initView()
        clickListener()
        onObserve()
    }

    private fun onObserve() {
        companyCodeViewModel.companyCodeStatus.observe(this, Observer {
            Loader.hideLoader()
            if (it.status.equals("success")) {
                prefs.baseurl = ""
                prefs.baseurl = it.url!!
                prefs.is_driver_based = it.is_driver_based.toString()
                prefs.is_hide = it.is_hide.toString()
                prefs.show_other_dates = it.show_other_dates.toString()
                prefs.is_maid_hide = it.is_maid_hide.toString()
                prefs.is_payment_hide = it.is_payment_hide.toString()
                prefs.company_name = it.company_name

                val intent = Intent(applicationContext, LoginActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(applicationContext, it.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun clickListener() {
        submitButton.setOnClickListener {
            if (isValidate()){
                Loader.showLoader(this)

                 if (this.isConnectedToNetwork()){
                     companyCodeViewModel.hitCompany(edt_companycode.text.toString())
                 }else {
                     Loader.hideLoader()
                     Toast.makeText(this, "Please connect to internet", Toast.LENGTH_LONG).show()
                 }
            }

        }
    }

    private fun initView() {
    }

    private fun isValidate(): Boolean{
        if (TextUtils.isEmpty(edt_companycode.text.toString())){
            Snackbar.make(outerCode, "Please enter company code", Snackbar.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }





}