package com.azinova.emaidcrewapp.ui.fragment.ui.report

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.azinova.emaidcrewapp.model.response.report.ResponseScheduleReport
import com.azinova.emaidcrewapp.network.ApiClient
import kotlinx.coroutines.launch

class ReportViewModel : ViewModel() {

    private val _reportStatus = MutableLiveData<ResponseScheduleReport>()
    val reportStatus: LiveData<ResponseScheduleReport>
        get() = _reportStatus

    fun getReportDetails(userId: String, date: String, userType: String, token: String?) {
        viewModelScope.launch {
            try {

                val response = ApiClient.getRetrofit().getScheduleReport(userId, userType, date, token!!)
                if (response.status.equals("success")) {
                    _reportStatus.value = response


                } else {
                    _reportStatus.value = response
                }
            } catch (e: Exception) {
                _reportStatus.value = ResponseScheduleReport(message = "Something went wrong", status = "Error")

            }
        }
    }

}