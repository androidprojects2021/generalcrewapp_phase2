package com.azinova.emaidcrewapp.model.response.transfer

import com.google.gson.annotations.SerializedName

data class ResponseBookingTransfer(


	@field:SerializedName("booking_id")
	val bookingId: List<String?>? = null,

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("transfer_all")
	val transferAll: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null
)

data class TransferedBookingsItem(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("no_of_maids")
	val noOfMaids: Int? = null,

	@field:SerializedName("time_from")
	val timeFrom: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null
)

data class DataItem(

	@field:SerializedName("transfered_bookings")
	val transferedBookings: List<TransferedBookingsItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("transfer_all")
	val transferAll: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null
)



	/*@field:SerializedName("transfered_bookings")
	val transferDetails: List<TransferDetailsItem>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("transfer_all")
	val transferAll: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class TransferDetailsItem(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("customer_id")
	val customerId: String? = null,

	@field:SerializedName("no_of_maids")
	val noOfMaids: Int? = null,

	@field:SerializedName("time_from")
	val timeing: String? = null
)
*/