package com.azinova.emaidcrewapp.model.response.start

import com.google.gson.annotations.SerializedName

data class ResponseStartService(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("service_status")
	val serviceStatus: Int? = null,

	@field:SerializedName("starttime")
	val starttime: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null
)

data class DataItem(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("receipt_no")
	val receiptNo: String? = null,

	@field:SerializedName("method")
	val method: String? = null,

	@field:SerializedName("service_status")
	val serviceStatus: Int? = null,

	@field:SerializedName("endtime")
	val endtime: String? = null,

	@field:SerializedName("payment")
	val payment: String? = null,

	@field:SerializedName("outstanding_amount")
	val outstandingAmount: Double? = null,

	@field:SerializedName("starttime")
	val starttime: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("balance_amt")
	val balanceAmt: Double? = null,

	@field:SerializedName("error_code")
	val error_code: String? = null
)

/*
@field:SerializedName("booking_id")
val bookingId: String? = null,

@field:SerializedName("amount")
val amount: String? = null,

@field:SerializedName("receipt_no")
val receiptNo: String? = null,

@field:SerializedName("method")
val method: String? = null,

@field:SerializedName("service_status")
val serviceStatus: Int? = null,

@field:SerializedName("payment")
val payment: String? = null,

@field:SerializedName("outstanding_amount")
val outstandingAmount: String? = null,

@field:SerializedName("message")
val message: String? = null,

@field:SerializedName("status")
val status: String? = null,

@field:SerializedName("starttime")
val starttime: String? = null,

@field:SerializedName("endtime")
val endtime: String? = null
*/
