package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.invoice.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.Invoice.Line_Items
import kotlinx.android.synthetic.main.layout_invoice_details.view.*

class DetailsAdapter(requireContext: Context, val lineItems: List<Line_Items?>) : RecyclerView.Adapter<DetailsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.layout_invoice_details,parent,false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txt_slno.text = position.plus(1).toString()
        holder.itemView.txt_cleaning.text = lineItems[position]?.service_name
        holder.itemView.txt_description.text = lineItems[position]?.description
        holder.itemView.txt_amount.text = lineItems[position]?.line_amount

    }

    override fun getItemCount(): Int = lineItems.size
}