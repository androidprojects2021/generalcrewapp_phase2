package com.azinova.emaidcrewapp.ui.fragment.ui.booking_details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.model.response.maiddetails.NextSchedulesItemList

class TimeScheduleAdapter(val nextSchedules: List<NextSchedulesItemList?>?) : RecyclerView.Adapter<TimeScheduleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.card_timeschedule,
                        parent,
                        false
                )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txt_customername.text = nextSchedules?.get(position)?.customerName
        holder.txt_servicetime.text = nextSchedules?.get(position)?.shiftStart +" - "+ nextSchedules?.get(position)?.shiftEnd
        holder.txt_customeraddress.text = nextSchedules?.get(position)?.customerAddress +" - "+ nextSchedules?.get(position)?.zone

        if (nextSchedules?.get(position)?.service_status.toString() == "2"){
            holder.img_zoneselect.setImageResource(R.drawable.ic_zone_select)
        }else{
            holder.img_zoneselect.setImageResource(R.drawable.ic_zone_unselect)
        }
    }

    override fun getItemCount(): Int {
        return nextSchedules?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txt_customername = itemView.findViewById<TextView>(R.id.txt_customername)
        var txt_servicetime = itemView.findViewById<TextView>(R.id.txt_servicetime)
        var txt_customeraddress = itemView.findViewById<TextView>(R.id.txt_customeraddress)
        var img_zoneselect = itemView.findViewById<ImageView>(R.id.img_zoneselect)

    }




}