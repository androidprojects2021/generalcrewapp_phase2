package com.azinova.emaidcrewapp.model.response.login

import com.google.gson.annotations.SerializedName

data class ResponseLogin(

	@field:SerializedName("userdetails")
	val userdetails: Userdetails? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Userdetails(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("driver_status")
	val driverStatus: Int? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("zone_name")
	val zone_name: String? = " "
)
