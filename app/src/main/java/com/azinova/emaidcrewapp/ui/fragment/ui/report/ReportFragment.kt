package com.azinova.emaidcrewapp.ui.fragment.ui.report

import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.azinova.emaidcrewapp.R
import com.azinova.emaidcrewapp.commons.Loader
import com.azinova.emaidcrewapp.database.EmaidDatabase
import com.azinova.emaidcrewapp.ui.activity.home.MainActivity
import com.azinova.emaidcrewapp.ui.activity.login.LoginActivity
import com.azinova.emaidcrewapp.utils.SharedPref
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_report.*
import java.text.SimpleDateFormat
import java.util.*


class ReportFragment : Fragment() {
    private lateinit var reportViewModel: ReportViewModel

    private val prefs = SharedPref
    var database: EmaidDatabase? = null

    var newdate: String = " "

    private lateinit var parent: MainActivity


    lateinit var datePickerDialog: DatePickerDialog
    var year = 0
    var month = 0
    var dayOfMonth = 0
    lateinit var calendar: Calendar

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_report, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        reportViewModel = ViewModelProvider(this).get(ReportViewModel::class.java)

        parent = activity as MainActivity

        val sdf = SimpleDateFormat("dd/MM/yyyy")
        newdate = sdf.format(Date())

        if (prefs.is_payment_hide.equals("yes")) {
            ll_paymentsection.visibility = View.GONE
        }else{
            ll_paymentsection.visibility = View.VISIBLE
        }

        setUpReport(newdate)
        observedIn()
        onClick()
    }

    private fun setUpReport(newdate: String) {

        txt_selecteddate .text = newdate

        var spf = SimpleDateFormat("dd/MM/yyyy")
        val newDatee = spf.parse(this.newdate)
//        spf = SimpleDateFormat("yyyy/MM/dd")
        spf = SimpleDateFormat("yyyy-MM-dd")
        val date:String = spf.format(newDatee)


        Loader.showLoader(requireContext())
        if (requireContext().isConnectedToNetwork()) {
            getReportDetails(prefs.user_id.toString(), date, prefs.user_type.toString(), prefs.token)
        } else {
            Loader.hideLoader()
            Snackbar.make(outerreport, "Please connect to internet", Snackbar.LENGTH_LONG).show()
        }

    }

    private fun getReportDetails(user_id: String, date: String, user_type: String, token: String?) {
        reportViewModel.getReportDetails(user_id,date,user_type,token)
    }

    private fun onClick() {
        img_datereport.setOnClickListener {

            calendar = Calendar.getInstance()
            year = calendar.get(Calendar.YEAR)
            month = calendar.get(Calendar.MONTH)
            dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)

            datePickerDialog = DatePickerDialog(
                    requireContext(),
                    { datePicker, year, month, day ->

                        var formatmonth: String = (month+1).toString()
                        var formatDayOfMonth: String = "" + day

                        if ((month+1) < 10) {

                            formatmonth = "0" + (month+1);
                        }
                        if (day < 10) {

                            formatDayOfMonth = "0" + day;
                        }


                        newdate = formatDayOfMonth + "/" + formatmonth + "/" + year
                        Log.e("Date", "Calender: ${newdate}")
                        txt_selecteddate.text = newdate
                        setUpReport(newdate)

                    }, year, month, dayOfMonth
            )

            datePickerDialog.setButton(
                    DialogInterface.BUTTON_NEGATIVE,
                    "Cancel",
                    DialogInterface.OnClickListener { dialog, which ->
                        if (which == DialogInterface.BUTTON_NEGATIVE) {

                            datePickerDialog.dismiss()

                        }
                    })
            datePickerDialog.show()
        }
    }

    private fun observedIn() {
        reportViewModel.reportStatus.observe(viewLifecycleOwner, androidx.lifecycle.Observer { reportStatus ->


            if (reportStatus.status.equals("success")) {
                Loader.hideLoader()

                if (prefs.is_payment_hide.equals("yes")) {
                    ll_paymentsection.visibility = View.GONE
                }else{
                    ll_paymentsection.visibility = View.VISIBLE
                }
                txt_no_of_bookings.text = reportStatus.reportDetails?.bookingCount.toString()
                txt_no_of_hours.text = reportStatus.reportDetails?.bookingHours.toString()
                txt_billingamount_value.text ="AED "+ reportStatus.reportDetails?.totalBilled.toString()
                txt_totalcollected_value.text ="AED "+ reportStatus.reportDetails?.totalCollected.toString()
                txt_cashinhand_value.text = "AED "+reportStatus.reportDetails?.cashinhand.toString()

            } else if (reportStatus.status.equals("token_error")) {
                Loader.hideLoader()
                Toast.makeText(requireContext(), reportStatus.message, Toast.LENGTH_SHORT)
                        .show()


                clearLogin()
                parent.stopServiceTracker()
                database?.clearAllTables()

                val intent = Intent(requireContext(), LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                activity?.finish()

            } else {
                Loader.hideLoader()
                Toast.makeText(requireContext(), reportStatus.message, Toast.LENGTH_SHORT)
                        .show()

            }



        })
    }
    private fun clearLogin() {

        prefs.user_details = ""
        prefs.user_id = 0
        prefs.user_name = ""
        prefs.user_type = ""
        prefs.token = ""
        prefs.image = ""
        prefs.phone = ""
        prefs.email = ""
        prefs.Latitude = ""
        prefs.Longitude = ""
        prefs.user_zone = ""
        prefs.is_maid_hide = ""
        prefs.is_hide = ""
        prefs.is_payment_hide = ""

    }

    override fun onPause() {
        super.onPause()
        try {
            Loader.hideLoader()
        } catch (e: Exception) {
        }
    }

    fun Context.isConnectedToNetwork(): Boolean {
        val connectivityManager =
                this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return connectivityManager?.activeNetworkInfo?.isConnectedOrConnecting() ?: false
    }

}