package com.azinova.emaidcrewapp.model.response.schedules

import androidx.room.ColumnInfo

data class ScheduleResponse(
        var booking_id: Int? = null,

        var schedule_date: String? = null,

        var timeing: String? = null,

        var no_of_maids: Int? = null,

        var service_status: Int? = null,

        var shift_start: String? = null,

        var shift_end: String? = null,

        var area: String? = null,

        var cleaning_material: String? = null,

        var customer_id: Int? = null,

        var customer_name: String? = null,

        var customerAddress: String? = null,

        var customerMobile: String? = null,

        var customerType: String? = null,

        var customerCode: String? = null,

        var keyStatus: String? = null,

        var bookingNote: String? = null,

        var customernotes: String? = null,

        var serviceFee: String? = null,

        var zone: String? = null,

        var mop: String? = null,

        var extraService: String? = null,

        var tools: String? = null,

        var total: String? = null,

        var balance: Double? = null,

        var outstandingBalance: String? = null,

        var servicetype: String? = null,

        var paymentStatus: Int? = null,

        var paidAmount: Double? = null,

        var customerLatitude: String? = null,

        var customerLongitude: String? = null,

        var starttime: String? = null,

        var endtime: String? = null,

        var istransfer: Int? = null,

        var vaccum_cleaner: Int? = null,

        var booking_type: String? = null,

        var username:String? = null,

        var maidlist: List<MaidDetails>? = null
)
