package com.azinova.emaidcrewapp.utils

import android.content.Context
import android.content.SharedPreferences

object SharedPref {

    private lateinit var sharedPreferences: SharedPreferences

    private const val PREF_NAME = "prefName"
    private const val USER_DETAILS = "userDetails"
    private const val USER_ID = "userid"
    private const val USER_NAME = "username"
    private const val USER_IMAGE = "userimage"
    private const val USER_PHONE = "userphone"
    private const val USER_EMAIL = "useremail"
    private const val USER_TYPE = "usertype"
    private const val TOKEN = "token"
    private const val FIREBASE_TOKEN = "firebasetoken"
    private const val LATITUDE = "latitude"
    private const val LONGITUDE = "longitude"
    private const val VIEW_STATUS = "viewstatus"
    private const val BASE_URL = "baseurl"
    private const val DRIVER_BASED = "driverBased"
    private const val CUSTOMER_SIGN = "customer_sign"
    private const val INVOICE_ID = "invoice_id"
    private const val IS_HIDE = "is_hide"
    private const val PUSHID = "pushId"
    private const val SHOW_OTHER_DATE = "show_other_dates"
    private const val IS_MAID_HIDE = "is_maid_hide"
    private const val PDF_URL = "pdf_url"
    private const val USER_ZONE = "userzone"
    private const val SCREEN_ORIENTATION = "screen_orientation"
    private const val IS_PAYMENT_HIDE = "is_payment_hide"
    private const val COMPANY_NAME = "company_name"

    fun clearSession(context: Context) {
        val editor =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.apply()
    }

    fun with(context: Context) {
        sharedPreferences =
            context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }


    var user_details: String?
        get() = sharedPreferences.getString(USER_DETAILS, "")
        set(id) = sharedPreferences.edit().putString(USER_DETAILS, id).apply()


    var user_id: Int
        get() = sharedPreferences.getInt(USER_ID, 0)
        set(id) = sharedPreferences.edit().putInt(USER_ID, id).apply()


    var user_name: String?
        get() = sharedPreferences.getString(USER_NAME, "")
        set(value) = sharedPreferences.edit().putString(USER_NAME, value).apply()

    var user_type: String?
        get() = sharedPreferences.getString(USER_TYPE, "")
        set(value) = sharedPreferences.edit().putString(USER_TYPE, value).apply()

    var token: String?
        get() = sharedPreferences.getString(TOKEN, "")
        set(value) = sharedPreferences.edit().putString(TOKEN, value).apply()

    var image: String?
        get() = sharedPreferences.getString(USER_IMAGE, "")
        set(value) = sharedPreferences.edit().putString(USER_IMAGE, value).apply()

    var phone: String?
        get() = sharedPreferences.getString(USER_PHONE, "")
        set(value) = sharedPreferences.edit().putString(USER_PHONE, value).apply()

    var email: String?
        get() = sharedPreferences.getString(USER_EMAIL, "")
        set(value) = sharedPreferences.edit().putString(USER_EMAIL, value).apply()

    var Latitude: String?
        get() = sharedPreferences.getString(LATITUDE, "")
        set(value) = sharedPreferences.edit().putString(LATITUDE, value).apply()

    var Longitude: String?
        get() = sharedPreferences.getString(LONGITUDE, "")
        set(value) = sharedPreferences.edit().putString(LONGITUDE, value).apply()

    var firebasetoken: String?
        get() = sharedPreferences.getString(FIREBASE_TOKEN, "")
        set(value) = sharedPreferences.edit().putString(FIREBASE_TOKEN, value).apply()

    var baseurl: String
        get() = sharedPreferences.getString(BASE_URL, "").toString()
        set(value) = sharedPreferences.edit().putString(BASE_URL, value).apply()

    var is_driver_based: String
        get() = sharedPreferences.getString(DRIVER_BASED, "").toString()
        set(value) = sharedPreferences.edit().putString(DRIVER_BASED, value).apply()

    var customer_sign: Boolean
        get() = sharedPreferences.getBoolean(CUSTOMER_SIGN, false)
        set(value) = sharedPreferences.edit().putBoolean(CUSTOMER_SIGN, value).apply()

    var invoice_id: String
        get() = sharedPreferences.getString(INVOICE_ID, "").toString()
        set(value) = sharedPreferences.edit().putString(INVOICE_ID, value).apply()

    var is_hide: String
        get() = sharedPreferences.getString(IS_HIDE, "").toString()
        set(value) = sharedPreferences.edit().putString(IS_HIDE, value).apply()

    var push_id: Int
        get() = sharedPreferences.getInt(PUSHID, 0)
        set(id) = sharedPreferences.edit().putInt(PUSHID, id).apply()

    var show_other_dates: String
        get() = sharedPreferences.getString(SHOW_OTHER_DATE, "").toString()
        set(value) = sharedPreferences.edit().putString(SHOW_OTHER_DATE, value).apply()

    var is_maid_hide: String
        get() = sharedPreferences.getString(IS_MAID_HIDE, "").toString()
        set(value) = sharedPreferences.edit().putString(IS_MAID_HIDE, value).apply()

    var is_payment_hide: String
        get() = sharedPreferences.getString(IS_PAYMENT_HIDE, "").toString()
        set(value) = sharedPreferences.edit().putString(IS_PAYMENT_HIDE, value).apply()


    var pdfUrl: String
        get() = sharedPreferences.getString(PDF_URL, "").toString()
        set(value) = sharedPreferences.edit().putString(PDF_URL, value).apply()

    var user_zone: String
        get() = sharedPreferences.getString(USER_ZONE, "").toString()
        set(value) = sharedPreferences.edit().putString(USER_ZONE, value).apply()

    var screen_orientation: String
        get() = sharedPreferences.getString(SCREEN_ORIENTATION, "").toString()
        set(value) = sharedPreferences.edit().putString(SCREEN_ORIENTATION, value).apply()

    var company_name: String?
        get() = sharedPreferences.getString(COMPANY_NAME, "")
        set(value) = sharedPreferences.edit().putString(COMPANY_NAME, value).apply()

}